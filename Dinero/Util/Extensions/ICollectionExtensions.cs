﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Util.Extensions
{
    public static class ICollectionExtensions
    {
        /// <summary>
        /// Get the group leader from a group of table guests.
        /// </summary>
        /// <param name="tableGroup"></param>
        /// <returns></returns>
        public static Models.Database.TableGuest getGroupLeader(this ICollection<Models.Database.TableGuest> tableGroup)
        {
            return tableGroup.Single(member => member.IsGroupLeader);
        }


        public static void setState(this ICollection<Models.Database.OrderItem> orderItems, Models.Database.OrderItem.Status status)
        {
            foreach( var orderItem in orderItems )
            {
                orderItem.State = (byte)status;
            }
        }
    }
}
