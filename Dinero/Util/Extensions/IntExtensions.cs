﻿namespace Dinero.Util.Extensions
{
    public static class IntExtensions
    {
        public static Currency? ToCurrency(this int currencyInt)
        {
            switch (currencyInt)
            {
                case 0: return Currency.CAD;
                default: return null;
            }
        }
    }
}