﻿namespace Dinero.Util.Extensions
{
    public static class ShortExtensions
    {
        public static Currency? ToCurrency(this short currencyInt)
        {
            switch (currencyInt)
            {
                case 0: return Currency.CAD;
                default: return null;
            }
        }
    }
}
