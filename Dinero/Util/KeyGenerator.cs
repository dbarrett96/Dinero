﻿using System.Security.Cryptography;
using System.Text;

namespace Dinero.Util
{
    public class KeyGenerator
    {
        // Public Static Methods.
        public static string Numeric(int length)
        {
            return GetUniqueKey(length, "0123456789");
        }

        public static string AlphaNumeric(int length)
        {
            return GetUniqueKey(length, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }

        // Private Static Methods.
        private static string GetUniqueKey(int maxSize, string charset)
        {
            char[] chars = charset.ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
    }
}
