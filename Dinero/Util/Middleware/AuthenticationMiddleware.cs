﻿using Dinero.Controllers.Core;
using Dinero.Services.Core;
using Dinero.Services.User;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dinero.Util.Middleware
{
    public class AuthenticationMiddleware
    {
        // Instnace Variables.
        private readonly RequestDelegate next;

        // Constructor
        public AuthenticationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        // Pubic Methods.
        public async Task Invoke(HttpContext httpContext, IUserService userService)
        {
            var noAuthEndpoints = new string[] { "/api/signIn", "/api/signUp", "/api/token", "/api/token/refresh" };

            // If the request is to an endpoint where authorization is required.
            if ( !noAuthEndpoints.Contains(httpContext.Request.Path.ToString()) )
            {
                // Request requires an authorization header.
                if (httpContext.Request.Headers.Keys.Contains("Authorization"))
                {
                    // Get access token (from Authorization header).
                    var accessToken = httpContext.Request.Headers["Authorization"];

                    // Set user id header.
                    try
                    {
                        httpContext.Request.Headers["X-User-Id"] = userService.find.byAccessToken(accessToken).Id.ToString();   
                    }
                    catch (DnException e)
                    {
                        await setUnauthorized(httpContext);
                        return;
                    }
                }
                else
                {
                    await setUnauthorized(httpContext);
                    return;
                }
            }

            await next.Invoke(httpContext);
        }

        // Private Methods.
        private async Task setUnauthorized(HttpContext httpContext)
        {
            var statusCode = (int)HttpStatusCode.Unauthorized;
            var response = new DnController.ErrorResponse(null, statusCode);
            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(response));
            httpContext.Response.StatusCode = statusCode;
            return;
        }
    }
}
