﻿using Dinero.Models.Database;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.CustomerSession;
using Dinero.Services.MenuItem;
using Dinero.Services.MenuItemOptions;
using Dinero.Services.Order;
using Dinero.Services.OrderItem;
using Dinero.Services.OrderItemOptions;
using Dinero.Services.Payment;
using Dinero.Services.Restaurant;
using Dinero.Services.SignIn;
using Dinero.Services.SignUp;
using Dinero.Services.Table;
using Dinero.Services.TableAlias;
using Dinero.Services.TableGuest;
using Dinero.Services.TableGuestRequest;
using Dinero.Services.TableSession;
using Dinero.Services.Token;
using Dinero.Services.User;
using Dinero.Util.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Dinero
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DineroContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DineroDatabase"))
            );

            services.AddScoped<IIdCoder, IdCoder>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserFetcher, UserFetcher>();
            services.AddScoped<IUserPersistor, UserPersistor>();
            services.AddScoped<ISignInService, SignInService>();
            services.AddScoped<ISignInSecretFetcher, SignInSecretFetcher>();
            services.AddScoped<ISignInSecretPersistor, SignInSecretPersistor>();
            services.AddScoped<ISignUpService, SignUpService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<ITokenFetcher, TokenFetcher>();
            services.AddScoped<ITokenPersistor, TokenPersistor>();
            services.AddScoped<ITableService, TableService>();
            services.AddScoped<ITableFetcher, TableFetcher>();
            services.AddScoped<ITableGuestRequestService, TableGuestRequestService>();
            services.AddScoped<ITableGuestRequestFetcher, TableGuestRequestFetcher>();
            services.AddScoped<ITableGuestRequestPersistor, TableGuestRequestPersistor>();
            services.AddScoped<ITableGuestService, TableGuestService>();
            services.AddScoped<ITableGuestFetcher, TableGuestFetcher>();
            services.AddScoped<ITableGuestPersistor, TableGuestPersistor>();
            services.AddScoped<ITableGuestMutator, TableGuestMutator>();
            services.AddScoped<ITableAliasService, TableAliasService>();
            services.AddScoped<ITableAliasFetcher, TableAliasFetcher>();
            services.AddScoped<IMenuItemService, MenuItemService>();
            services.AddScoped<IMenuItemFetcher, MenuItemFetcher>();
            services.AddScoped<IMenuItemOptionsService, MenuItemOptionsService>();
            services.AddScoped<IMenuItemOptionsFetcher, MenuItemOptionsFetcher>();
            services.AddScoped<ICurrencyService, CurrencyService>();
            services.AddScoped<IRestaurantService, RestaurantService>();
            services.AddScoped<IRestaurantFetcher, RestaurantFetcher>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderFetcher, OrderFetcher>();
            services.AddScoped<ITableSessionService, TableSessionService>();
            services.AddScoped<ITableSessionFetcher, TableSessionFetcher>();
            services.AddScoped<ITableSessionPersistor, TableSessionPersistor>();
            services.AddScoped<IOrderItemService, OrderItemService>();
            services.AddScoped<IOrderItemFetcher, OrderItemFetcher>();
            services.AddScoped<IOrderItemPersistor, OrderItemPersistor>();
            services.AddScoped<IOrderItemMutator, OrderItemMutator>();
            services.AddScoped<IOrderItemOptionsService, OrderItemOptionsService>();
            services.AddScoped<IOrderItemOptionsFetcher, OrderItemOptionsFetcher>();
            services.AddScoped<IOrderItemOptionsMutator, OrderItemOptionsMutator>();
            services.AddScoped<IOrderItemOptionsPersistor, OrderItemOptionsPersistor>();
            services.AddScoped<IBillGroupService, BillGroupService>();
            services.AddScoped<IBillGroupFetcher, BillGroupFetcher>();
            services.AddScoped<IBillGroupPersistor, BillGroupPersistor>();
            services.AddScoped<IBillGroupMutator, BillGroupMutator>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IPaymentProvider, TestPaymentProvider>();
            services.AddScoped<IPaymentPersistor, PaymentPersistor>();
            services.AddScoped<IPaymentFetcher, PaymentFetcher>();
            services.AddScoped<ICustomerSessionService, CustomerSessionService>();
            services.AddScoped<ICustomerSessionPersistor, CustomerSessionPersistor>();
            services.AddScoped<ICustomerSessionFetcher, CustomerSessionFetcher>();

            services.AddMvc();

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Dinero Customer API",
                    Description = "",
                    TermsOfService = "None",
                    Contact = new Contact()
                    {
                        Name = "Damon Barrett"
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<AuthenticationMiddleware>();
            app.UseMvc();

            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dinero Customer API - v1");
            });
        }
    }
}
