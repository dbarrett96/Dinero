﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.MenuItem;
using Dinero.Services.Restaurant;
using Dinero.Services.Table;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Dinero.Controllers.Menu
{
    [Route("api/menu")]
    public class MenuController: UserController
    {
        // Instance Variables.
        private readonly ITableService tableService;
        private readonly IMenuItemService menuItemService;
        private readonly ICurrencyService currencyService;
        private readonly IRestaurantService restaurantService;

        // Constructor.
        public MenuController(
            IUserService userService, 
            ITableService tableService, 
            IMenuItemService menuItemService,
            ICurrencyService currencyService,
            IRestaurantService restaurantService) : base(userService)
        {
            this.tableService = tableService;
            this.menuItemService = menuItemService;
            this.currencyService = currencyService;
            this.restaurantService = restaurantService;
        }

        [HttpGet]
        public IResponse get()
        {
            try
            {
                // Get the table the user is sitting at.
                var table = tableService.find.byUser(user);

                // Get menu items for restaurant id from table.
                var menuItems = menuItemService.find.manyByRestaurantId(table.ResturantId);

                // Map items to response type.
                var responseItems = menuItems.Select(item =>
                    new MenuResponse.Item(menuItemService.id.encode(item.Id),
                                          item.Name,
                                          item.Category,
                                          item.Description,
                                          currencyService.buildPrice(item.Price, item.CurrencyType ?? Util.Currency.CAD)));

                var externalRestaurantId = restaurantService.id.encode(table.ResturantId);

                return new MenuResponse(externalRestaurantId, responseItems);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
