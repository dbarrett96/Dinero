﻿using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.MenuItem;
using Dinero.Services.MenuItemOptions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using static Dinero.Controllers.Core.DnController;

namespace Dinero.Controllers.Menu
{
    [Route("api/menu/{menuItemId}")]
    public class MenuItemDetailsController
    {
        // Instance Variables.
        private readonly IMenuItemService menuItemService;
        private readonly IMenuItemOptionsService menuItemOptionsService;
        private readonly ICurrencyService currencyService;

        // Constructor.
        public MenuItemDetailsController(IMenuItemService menuItemService, IMenuItemOptionsService menuItemOptionsService, ICurrencyService currencyService)
        {
            this.menuItemService = menuItemService;
            this.menuItemOptionsService = menuItemOptionsService;
            this.currencyService = currencyService;
        }

        [HttpGet] 
        public IResponse get(string menuItemId)
        {
            try
            {
                var decodedItemId = menuItemService.id.decode(menuItemId);
                var item = menuItemService.find.byId(decodedItemId);
                var options = menuItemOptionsService.find.byMenuItem(item);

                return new MenuItemResponse(
                    menuItemService.id.encode(item.Id),
                    item.Name, item.Category,
                    item.Description,
                    currencyService.buildPrice(item.Price, item.CurrencyType ?? Util.Currency.CAD),
                    options.Select(opt =>
                        new MenuItemResponse.Option(
                            menuItemOptionsService.id.encode(opt.Id),
                            opt.Description,
                            opt.Category,
                            currencyService.buildPrice(opt.Surcharge ?? 0, opt.CurrencyType ?? Util.Currency.CAD)
                        )
                    )
                );
            }
            catch (DnException e)
            {
                return new ErrorResponse(e.Message, 400);
            }
        }
    }
}
