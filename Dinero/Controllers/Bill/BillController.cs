﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.Order;
using Dinero.Services.OrderItem;
using Dinero.Services.OrderItemOptions;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Controllers.Bill
{
    [Route("api/bill")]
    public class BillController: UserController
    {
        // Instance Variables.
        private readonly ITableGuestService tableGuestService;
        private readonly IBillGroupService billGroupService;
        private readonly IOrderService orderService;
        private readonly ICurrencyService currencyService;
        private readonly IOrderItemService orderItemService;
        private readonly IOrderItemOptionsService orderItemOptionsService;

        // Constructor.
        public BillController(
            IUserService userService,
            ITableGuestService tableGuestService,
            IBillGroupService billGroupService,
            IOrderService orderService,
            ICurrencyService currencyService,
            IOrderItemService orderItemService,
            IOrderItemOptionsService orderItemOptionsService) : base(userService)
        {
            this.tableGuestService = tableGuestService;
            this.billGroupService = billGroupService;
            this.orderService = orderService;
            this.currencyService = currencyService;
            this.orderItemService = orderItemService;
            this.orderItemOptionsService = orderItemOptionsService;
        }

        // Methods.
        [HttpGet]
        public IResponse get()
        {
            try
            {
                var userAsTableGuest = tableGuestService.find.byUser(user);
                var billGroup = billGroupService.find.byPayer(userAsTableGuest);
                var billGroupGuests = billGroup.Select(member => tableGuestService.find.byId(member.Guest));
                var completeOrderItems = orderService.find.forTableGuests(billGroupGuests);
                return summarizeOrderItems(completeOrderItems);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }

        private BillResponse summarizeOrderItems(IEnumerable<Models.Database.OrderItem> completeOrderItems)
        {
            int totalCost = 0;
            var fragmentItems = new List<OrderResponse.Fragment.Item>();

            foreach (var item in completeOrderItems)
            {
                var surcharges = (item.MenuItem.MenuItemOption.Sum(o => o.Surcharge) ?? 0);

                totalCost += item.MenuItem.Price + surcharges;

                var fragmentOptions = item.OrderItemOption.Zip(item.MenuItem.MenuItemOption, (orderOption, menuOption) => new { orderOption, menuOption })
                    .Select(o =>
                       new OrderResponse.Fragment.Item.Option(
                           orderItemOptionsService.id.encode(o.orderOption.Id),
                           o.menuOption.Description,
                           currencyService.buildPrice(o.menuOption.Surcharge ?? 0, Util.Currency.CAD)
                       )
                    );

                var fragmentItem = new OrderResponse.Fragment.Item(
                    orderItemService.id.encode(item.Id),
                    item.MenuItem.Name,
                    currencyService.buildPrice(
                        item.MenuItem.Price,
                        Util.Currency.CAD),
                    fragmentOptions.ToList());

                fragmentItems.Add(fragmentItem);
            }

            return new BillResponse(
                currencyService.buildPrice(totalCost, Util.Currency.CAD),
                new Dictionary<string, string>(),
                fragmentItems);
        }
    }
}
