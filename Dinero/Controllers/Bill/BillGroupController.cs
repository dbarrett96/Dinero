﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Dinero.Controllers.Bill
{
    [Route("api/bill/group")]
    public class BillGroupController: UserController
    {
        // Instance Variables.
        private readonly ITableGuestService tableGuestService;
        private readonly IBillGroupService billGroupService;

        // Constructor.
        public BillGroupController(IUserService userService, ITableGuestService tableGuestService, IBillGroupService billGroupService) : base(userService)
        {
            this.tableGuestService = tableGuestService;
            this.billGroupService = billGroupService;
        }
        
        // Methods.
        [HttpGet]
        public IResponse get()
        {
            try
            {
                var userAsTableGuest = tableGuestService.find.byUser(user);
                var billGroup = billGroupService.find.byPayer(userAsTableGuest);
                var billGroupGuests = billGroup.Select(member => tableGuestService.find.byId(member.Guest));
                var billGroupUsers = billGroupGuests.Select(guest => userService.find.byId(guest.UserId));

                var billGroupResponseMembers = billGroupUsers.Select(user => new BillGroupResponse.Member(userService.id.encode(user.Id), user.FirstName, user.LastName));

                return new BillGroupResponse(billGroupResponseMembers.ToList());
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
