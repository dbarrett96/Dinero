﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.Order;
using Dinero.Services.Payment;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.Bill
{
    [Route("api/bill/pay")]
    public class PayBillController : UserController
    {
        // Instance Variables.
        private readonly ITableGuestService tableGuestService;
        private readonly IBillGroupService billGroupService;
        private readonly IOrderService orderService;
        private readonly IPaymentService paymentService;
        private readonly ITableSessionService tableSessionService;

        // Constructor.
        public PayBillController(
            IUserService userService,
            ITableGuestService tableGuestService,
            IBillGroupService billGroupService,
            IOrderService orderService,
            IPaymentService paymentService,
            ITableSessionService tableSessionService) : base(userService)
        {
            this.tableGuestService = tableGuestService;
            this.billGroupService = billGroupService;
            this.orderService = orderService;
            this.paymentService = paymentService;
            this.tableSessionService = tableSessionService;
        }

        // Methods.
        [HttpGet]
        public async Task<IResponse> get()
        {
            try
            {
                if (paymentService.userHasPendingPayment(user))
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "The user already has an outstanding pending payment.");
                }

                var userAsTableGuest = tableGuestService.find.byUser(user);
                var billGroup = billGroupService.find.byPayer(userAsTableGuest);
                var billGroupGuests = billGroup.Select(member => tableGuestService.find.byId(member.Guest));
                var completeOrderItems = orderService.find.forTableGuests(billGroupGuests);

                var billTotal = getBillTotal(completeOrderItems);

                if (billTotal == 0)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "No bill to pay.");
                }

                var session = tableSessionService.find.byUser(user);

                var payment = await paymentService.createPayment(user, session, billTotal, Util.Currency.CAD);

                return new PayBillResponse(paymentService.id.encode(payment.Id), billTotal, payment.PaymentId);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }

        private long getBillTotal(IEnumerable<Models.Database.OrderItem> completeOrderItems)
        {
            long totalCost = 0;

            foreach (var item in completeOrderItems)
            {
                var surcharges = (item.MenuItem.MenuItemOption.Sum(o => o.Surcharge) ?? 0);
                totalCost += item.MenuItem.Price + surcharges;
            }

            return totalCost;
        }
    }
}
