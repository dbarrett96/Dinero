﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Table;
using Dinero.Services.TableGuestRequest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.SeatRequest
{
    [Route("api/seatRequest/{requestId}")]
    public class SeatRequestController : UserController
    {
        // instance Variables.
        private readonly ITableService tableService;
        private readonly ITableGuestRequestService tableGuestRequestService;

        // Constructor.
        public SeatRequestController(IUserService userService, ITableService tableService, ITableGuestRequestService tableGuestRequestService) : base(userService)
        {
            this.tableService = tableService;
            this.tableGuestRequestService = tableGuestRequestService;
        }

        [HttpGet]
        /// <summary>
        /// Get the status of a seat request.
        /// </summary>
        /// <param name="requestId">The unique id for the request.</param>
        /// <returns>The request state, or an error.</returns>
        public IResponse get(string requestId)
        {
            long internalRequestId = tableGuestRequestService.id.decode(requestId);

            try
            {
                var request = tableGuestRequestService.find.byId(internalRequestId);
                var requestOriginator = userService.find.byId(request.UserId);

                if (requestOriginator.Id == user.Id)
                {
                    return new SeatRequestResponse(request.requestState.ToString());
                }
                else
                {
                    return error(System.Net.HttpStatusCode.Unauthorized, "User is not permitted to the resource.");
                }
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
