﻿using Dinero.Controllers.Core;
using Dinero.Models.Database;
using Dinero.Models.Request;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Table;
using Dinero.Services.Table.Exceptions;
using Dinero.Services.TableGuestRequest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Dinero.Controllers.SeatRequest
{
    [Route("api/seatRequest")]
    public class RequestSeatController: UserController
    {
        // Instance Variables.
        public ITableService tableService;
        private readonly ITableGuestRequestService tableGuestRequestService;

        // Constructor.
        public RequestSeatController(IUserService userSerivce, ITableService tableService, ITableGuestRequestService tableGuestRequestService) : base(userSerivce)
        {
            this.tableService = tableService;
            this.tableGuestRequestService = tableGuestRequestService;
        }

        [HttpPost]
        /// <summary>
        /// Request to join a table.
        /// </summary>
        /// <param name="body">The request body.</param>
        /// <returns>The status of the request, or an error.</returns>
        public async Task<IResponse> post([FromBody]RequestSeatRequest body)
        {
            // Make sure user is not sitting at a table already, return error if already sitting.
            try
            {
                var currentTable = tableService.find.byUser(user);
                return error(HttpStatusCode.BadRequest, "User is already seated at a table.");
            }
            // User is not seated, continue...
            catch (NotSeatedException e) 
            {
                try
                {
                    var requestedTable = tableService.find.byAlias(body.table);

                    // If they have already request to join other tables, cancel those requests firsts.
                    var pendingRequests = tableGuestRequestService.find.byUser(user, TableGuestRequest.State.PENDING);
                    foreach (var request in pendingRequests)
                    {
                        await tableGuestRequestService.cancel(request);
                    }

                    // Create a new request to join the specified table.
                    var newRequest = await tableGuestRequestService.join(user, requestedTable);
                    await tableGuestRequestService.accept(newRequest);

                    // Return request id, state.
                    return new RequestSeatResponse(
                        tableGuestRequestService.id.encode(newRequest.Id),
                        newRequest.requestState.ToString()
                    );
                }
                catch (DnException err)
                {
                    return error(HttpStatusCode.InternalServerError, err.Message);
                }
            }
        }
    }
}
