﻿using Dinero.Controllers.Core;
using Dinero.Models.Database;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Table;
using Dinero.Services.TableGuest;
using Dinero.Services.TableGuestRequest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Dinero.Util.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.SeatRequest
{
    [Route("api/seatRequest/{requestId}/cancel")]
    public class CancelSeatRequestController: UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestRequestService tableGuestRequestService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public CancelSeatRequestController(
            IUserService userService,
            ITableSessionService tableSessionService,
            ITableGuestRequestService tableGuestRequestService,
            ITableGuestService tableGuestService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestRequestService = tableGuestRequestService;
            this.tableGuestService = tableGuestService;
        }

        // Constructor.
        public CancelSeatRequestController(IUserService userService, ITableSessionService tableSessionService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
        }

        [HttpPut]
        public async Task<IResponse> put(string requestId)
        {
            var internalRequestId = tableGuestRequestService.id.decode(requestId);

            try
            {
                var request = tableGuestRequestService.find.byId(internalRequestId);
                var requestOriginator = userService.find.byId(request.UserId);

                if (requestOriginator.Id != user.Id)
                {
                    var tableSession = tableSessionService.find.activeAtTableId(request.TableId);
                    var tableGroupLeaderId = tableGuestService.find.manyByTableSession(tableSession).getGroupLeader().UserId;

                    if (tableGroupLeaderId != user.Id)
                    {
                        return error(System.Net.HttpStatusCode.Unauthorized, "User is not permited to cancel the request.");
                    }
                }

                return await cancelRequest(request);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.InternalServerError, e.Message);
            }
        }

        private async Task<IResponse> cancelRequest(TableGuestRequest request)
        {
            await tableGuestRequestService.cancel(request);
            return null;
        }
    }
}
