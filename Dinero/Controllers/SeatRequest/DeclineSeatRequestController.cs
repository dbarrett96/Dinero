﻿using Dinero.Controllers.Core;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using Dinero.Services.TableGuestRequest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Dinero.Util.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Dinero.Controllers.SeatRequest
{
    [Route("api/seatRequest/{requestId}/decline")]
    public class DeclineSeatRequestController : UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestRequestService tableGuestRequestService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public DeclineSeatRequestController(
            IUserService userService,
            ITableSessionService tableSessionService,
            ITableGuestRequestService tableGuestRequestService,
            ITableGuestService tableGuestService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestRequestService = tableGuestRequestService;
            this.tableGuestService = tableGuestService;
        }

        [HttpPut]
        public async Task<IResponse> put(string requestId)
        {
            var internalRequestId = tableGuestRequestService.id.decode(requestId);

            try
            {
                var request = tableGuestRequestService.find.byId(internalRequestId);
                var tableSession = tableSessionService.find.activeAtTableId(request.TableId);
                var tableGroupLeader = tableGuestService.find.manyByTableSession(tableSession).getGroupLeader();

                // If user who made request is the group leader...
                if (tableGroupLeader != null && tableGroupLeader.UserId == user.Id)
                {
                    await tableGuestRequestService.decline(request);
                    return null;
                }
                else
                {
                    return error(System.Net.HttpStatusCode.Unauthorized, "User is not permited to decline the request.");
                }
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
