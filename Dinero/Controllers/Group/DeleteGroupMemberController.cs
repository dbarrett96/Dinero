﻿using Dinero.Controllers.Core;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Table;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Dinero.Util.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.Group
{
    [Route("api/group/{userId}")]
    public class DeleteGroupMemberController : UserController
    {
        // Instance Variables.
        private readonly ITableGuestService tableGuestService;
        private readonly ITableService tableService;
        private readonly ITableSessionService tableSessionService;

        // Constructor.
        public DeleteGroupMemberController(IUserService userService, ITableService tableService, ITableGuestService tableGuestService, ITableSessionService tableSessionService) : base(userService)
        {
            this.tableService = tableService;
            this.tableGuestService = tableGuestService;
            this.tableSessionService = tableSessionService;
        }

        [HttpDelete]
        public async Task<IResponse> delete(string userId)
        {
            try
            {
                var decodedRemovalUserId = userService.id.decode(userId);

                // Get group. 
                var session = tableSessionService.find.byUser(user);
                var tableGroup = tableGuestService.find.manyByTableSession(session);

                // Get group leader.
                var groupLeader = tableGroup.getGroupLeader();

                // Make sure current user is group leader.
                if (groupLeader.UserId != user.Id)
                {
                    return error(System.Net.HttpStatusCode.Unauthorized, "User is not authorized to remove group members.");
                }

                // Make sure user is not trying to remove themself.
                if (decodedRemovalUserId == user.Id)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "User cannot remove themself from a group.");
                }

                // Make sure user is part of group.
                tableGroup.Single(member => member.UserId == decodedRemovalUserId);

                // Get user to remove.
                var userToRemove = userService.find.byId(decodedRemovalUserId);

                // Remove table group member.
                await tableSessionService.remove(userToRemove);

                return null;
            } catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.InternalServerError, e.Message);
            }
        }


    }
}
