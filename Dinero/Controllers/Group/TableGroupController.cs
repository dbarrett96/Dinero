﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Table;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Dinero.Controllers.Group
{
    [Route("api/group")]
    public class TableGroupController: UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public TableGroupController(IUserService userService, ITableSessionService tableSessionService, ITableGuestService tableGuestService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestService = tableGuestService;
        }

        [HttpGet]
        /// <summary>
        /// Get the list of people seated at the table the current user is sitting at.
        /// </summary>
        /// <returns>A list of people seated at the table the current user is sitting at.</returns>
        public IResponse get()
        {
            try
            {
                var session = tableSessionService.find.byUser(user);
                var tableGuests = tableGuestService.find.manyByTableSession(session);

                var tableGroupMembers = tableGuests
                    .Select(guest => new { User = userService.find.byId(guest.UserId), IsGroupLeader = guest.IsGroupLeader })
                    .Select(o => new TableGroupResponse.Member(userService.id.encode(o.User.Id), o.User.FirstName, o.User.LastName, o.IsGroupLeader));

                return new TableGroupResponse(tableGroupMembers);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
