﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Dinero.Util.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.Group
{
    [Route("api/group/leader")]
    public class GroupLeaderController: UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public GroupLeaderController(IUserService userService, ITableGuestService tableGuestService, ITableSessionService tableSessionService): base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestService = tableGuestService;
        }

        [HttpGet]
        public IResponse get()
        {
            try
            {
                var session = tableSessionService.find.byUser(user);
                var tableGroup = tableGuestService.find.manyByTableSession(session);
                var groupLeader = tableGroup.getGroupLeader();
                var groupLeaderUser = userService.find.byId(groupLeader.UserId);

                return new GroupLeaderResponse(groupLeaderUser.FirstName, groupLeaderUser.LastName);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
            
        }
    }
}
