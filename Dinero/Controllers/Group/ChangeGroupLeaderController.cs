﻿using Dinero.Controllers.Core;
using Dinero.Models.Request;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Dinero.Util.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.Group
{
    [Route("api/group/leader")]
    public class ChangeGroupLeaderController: UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public ChangeGroupLeaderController(IUserService userService, ITableSessionService tableSessionService, ITableGuestService tableGuestService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestService = tableGuestService;
        }

        [HttpPut]
        public async Task<IResponse> put([FromBody]ChangeGroupLeaderRequest body)
        {
            try
            {
                var session = tableSessionService.find.byUser(user);
                var tableGroup = tableGuestService.find.manyByTableSession(session);
                var groupLeader = tableGroup.getGroupLeader();

                if (groupLeader.UserId != user.Id)
                {
                    return error(System.Net.HttpStatusCode.Unauthorized, "Only the group leader can change the leader of the group.");
                }

                var newGroupLeaderId = userService.id.decode(body.id);
                var newGroupLeader = tableGroup.Single(member => member.UserId == newGroupLeaderId);

                await tableSessionService.changeLeader(groupLeader, newGroupLeader);

                return null;
            }
            catch (InvalidOperationException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
