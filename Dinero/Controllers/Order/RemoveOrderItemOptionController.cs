﻿using Dinero.Controllers.Core;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.OrderItem;
using Dinero.Services.OrderItemOptions;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Dinero.Controllers.Order
{
    [Route("api/order/{orderItemId}/options/{orderItemOptionId}")]
    public class RemoveOrderItemOptionController : UserController
    {
        // Instance Variables.
        private readonly IOrderItemService orderItemService;
        private readonly IOrderItemOptionsService orderItemOptionsService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public RemoveOrderItemOptionController(IUserService userService, IOrderItemOptionsService orderItemOptionsService, IOrderItemService orderItemService, ITableGuestService tableGuestService) : base(userService)
        {
            this.orderItemOptionsService = orderItemOptionsService;
            this.orderItemService = orderItemService;
            this.tableGuestService = tableGuestService;
        }

        [HttpDelete]
        public async Task<IResponse> delete(string orderItemId, string orderItemOptionId)
        {
            try
            {
                var internalOrderItemId = orderItemService.id.decode(orderItemId);
                var internalOrderItemOptionId = orderItemOptionsService.id.decode(orderItemOptionId);

                // Get orderItemOption.
                var orderItemOption = orderItemOptionsService.find.byId(internalOrderItemOptionId);

                // Make sure it is from order item id.
                if (orderItemOption.OrderItemId != internalOrderItemId)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Invalid path.");
                }

                // Make sure user has access to orderItemId.
                var userAsTableGuest = tableGuestService.find.byUser(user);
                var orderItem = orderItemService.find.byId(internalOrderItemId);

                if (userAsTableGuest.Id != orderItem.TableGuestId)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "User is not allowed to alter the item.");
                }

                if (orderItem.status >= Models.Database.OrderItem.Status.ORDERED)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Cannot alter an ordered item.");
                }

                // Remove option.
                orderItemOptionsService.mutate.delete(orderItemOption);
                await orderItemOptionsService.persist.Save();

                return null;
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
