﻿using Dinero.Controllers.Core;
using Dinero.Models.Database;
using Dinero.Models.Request;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.MenuItem;
using Dinero.Services.MenuItemOptions;
using Dinero.Services.Order;
using Dinero.Services.Table;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dinero.Controllers.Order
{
    [Route("api/order")]
    public class AddOrderItemController: UserController
    {
        // Instance Variables.
        private readonly ITableService tableService;
        private readonly IMenuItemService menuItemService;
        private readonly IMenuItemOptionsService menuItemOptionsService;
        private readonly ITableGuestService tableGuestService;
        private readonly IOrderService orderService;

        // Constructor.
        public AddOrderItemController(
            IUserService userService, 
            ITableService tableService, 
            IMenuItemService menuItemService,
            IMenuItemOptionsService menuItemOptionsService,
            ITableGuestService tableGuestService,
            IOrderService orderService) : base(userService)
        {
            this.tableService = tableService;
            this.menuItemService = menuItemService;
            this.menuItemOptionsService = menuItemOptionsService;
            this.tableGuestService = tableGuestService;
            this.orderService = orderService;
        }

        [HttpPut]
        public async Task<IResponse> put([FromBody] AddOrderItemRequest body)
        {
            try
            {
                // Check user is sitting at a table.
                var userTable = tableService.find.byUser(user);
                var userAsTableGuest = tableGuestService.find.byUser(user);

                // Get menu items.
                var menuItemIds = body.items.Keys.Select(menuItemId => menuItemService.id.decode(menuItemId));
                var menuItems = menuItemService.find.manyById(menuItemIds);

                // Check that order items are for the restaurant the table is at.
                foreach (var item in menuItems)
                {
                    if (item.RestaurantId != userTable.ResturantId)
                    {
                        return error(HttpStatusCode.BadRequest, "User is not authorized to order all requested items.");
                    } 
                }

                // Get menu item option ids from body.
                var menuItemOptionIds = body.items.Values.Select(optionIdList => optionIdList.Select(optionId => menuItemOptionsService.id.decode(optionId)));
                // MenuItem zipped with IEnumerable<long>
                var itemsAndOptionIds = menuItems.Zip(menuItemOptionIds, (menuItem, optionIdList) => new { menuItem, optionIdList });

                var itemsToBeOrdered = new List<MenuItem>();

                // Check that options belong to the menu item.
                foreach (var orderItem in itemsAndOptionIds)
                {
                    var menuItem = orderItem.menuItem.Copy();
                    var menuItemOptions = menuItemOptionsService.find.byMenuItem(menuItem);
                    var selectedMenuItemOptions = menuItemOptions.Where(option => orderItem.optionIdList.Contains(option.Id));

                    if (selectedMenuItemOptions.Count() != orderItem.optionIdList.Count())
                    {
                        return error(HttpStatusCode.BadRequest, "Cannot add option to the provided menu item.");
                    }

                    menuItem.MenuItemOption = selectedMenuItemOptions.ToList();

                    itemsToBeOrdered.Add(menuItem);
                }

                // Add pending order items and options.
                await orderService.addItems(itemsToBeOrdered, userAsTableGuest);
               
                return null;
            }
            catch (DnException e)
            {
                return error(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
