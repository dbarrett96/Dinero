﻿using Dinero.Controllers.Core;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Order;
using Dinero.Services.OrderItem;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.Order
{
    [Route("api/order/{orderItemId}")]
    public class RemoveOrderItemController : UserController
    {
        // Instance Variables.
        private readonly IOrderService orderService;
        private readonly IOrderItemService orderItemService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public RemoveOrderItemController(IUserService userService, IOrderItemService orderItemService, ITableGuestService tableGuestService) : base(userService)
        {
            this.orderItemService = orderItemService;
            this.tableGuestService = tableGuestService;
        }

        [HttpDelete]
        public IResponse delete(string orderItemId)
        {
            try
            {
                // Get order item.
                var internalOrderItemId = orderItemService.id.decode(orderItemId);
                var orderItem = orderItemService.find.byId(internalOrderItemId);

                if (orderItem.status >= Models.Database.OrderItem.Status.ORDERED)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Cannot remove an ordered item.");
                }

                // Make sure it is the users.
                var userAsTableGuest = tableGuestService.find.byUser(user);

                if (userAsTableGuest.Id == orderItem.TableGuestId)
                {
                    orderItemService.mutate.delete(orderItem);
                    orderItemService.persist.Save();

                    return null;
                }
                else
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Cannot remove that order item.");
                }
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }      
        }
    }
}
