﻿using Dinero.Controllers.Core;
using Dinero.Models.Database;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Order;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Controllers.Order
{
    [Route("api/order")]
    public class OrderController : UserController
    {
        private readonly ITableGuestService tableGuestService;
        private readonly IOrderService orderService;
        private readonly ICurrencyService currencyService;
        private readonly IIdCoder id;

        // Constructor.
        public OrderController(IUserService userService, ITableGuestService tableGuestService, IOrderService orderService, ICurrencyService currencyService, IIdCoder id) : base(userService)
        {
            this.tableGuestService = tableGuestService;
            this.orderService = orderService;
            this.currencyService = currencyService;
            this.id = id;
        }

        [HttpGet]
        public IResponse get()
        {
            try
            {
                var userAsTableGuest = tableGuestService.find.byUser(user);
                var order = orderService.find.byTableGuest(userAsTableGuest);

                var responseFragments = new Dictionary<string, OrderResponse.Fragment>();

                foreach (var enumCase in Enum.GetValues(typeof(OrderItem.Status)).Cast<OrderItem.Status>())
                {
                    responseFragments[enumCase.ToString()] = summarizeOrderItems(order.Where(item => item.status == enumCase));
                }

                return new OrderResponse(responseFragments);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }

        private OrderResponse.Fragment summarizeOrderItems(IEnumerable<Models.Database.OrderItem> completeOrderItems)
        {
            int totalCost = 0;
            var fragmentItems = new List<OrderResponse.Fragment.Item>();

            foreach (var item in completeOrderItems)
            {
                var surcharges = (item.MenuItem.MenuItemOption.Sum(o => o.Surcharge) ?? 0);

                totalCost += item.MenuItem.Price + surcharges;

                var fragmentOptions = item.OrderItemOption.Zip( item.MenuItem.MenuItemOption,  (orderOption, menuOption) => new { orderOption, menuOption } )
                    .Select( o => 
                        new OrderResponse.Fragment.Item.Option(
                            id.encode(o.orderOption.Id), 
                            o.menuOption.Description, 
                            currencyService.buildPrice(o.menuOption.Surcharge ?? 0, Util.Currency.CAD)
                        )
                    );

                var fragmentItem = new OrderResponse.Fragment.Item(
                    id.encode(item.Id), 
                    item.MenuItem.Name, 
                    currencyService.buildPrice(
                        item.MenuItem.Price,
                        Util.Currency.CAD),
                    fragmentOptions.ToList());

                fragmentItems.Add(fragmentItem);
            }

            return new OrderResponse.Fragment(currencyService.buildPrice(totalCost, Util.Currency.CAD), new Dictionary<string,string>(), fragmentItems);
        }
    }
}
