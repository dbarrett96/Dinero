﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Order;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Dinero.Controllers.Order
{
    [Route("api/order")]
    public class PlaceOrderController: UserController
    {
        // Instance Variables.
        private readonly ITableGuestService tableGuestService;
        private readonly IOrderService orderService;

        // Constructor.
        public PlaceOrderController(IUserService userService, IOrderService orderService, ITableGuestService tableGuestService) : base(userService)
        {
            this.orderService = orderService;
            this.tableGuestService = tableGuestService;
        }

        [HttpPost]
        public async Task<IResponse> post()
        {
            try
            {
                var userAsTableGuest = tableGuestService.find.byUser(user);
                var orderStatus = await orderService.placeOrderFor(userAsTableGuest);

                return new PlaceOrderResponse(orderStatus.ToString());
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
