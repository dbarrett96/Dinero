﻿using Dinero.Services.User;
using System;

namespace Dinero.Controllers.Core
{
    public class UserController: DnController
    {
        // Instance Variables
        protected readonly IUserService userService;

        // Contructor.
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// The user who made the request.
        /// </summary>
        public Models.Database.User user => userService.find.byId(Int32.Parse(Request.Headers["X-User-Id"]));
    }
}
