﻿using Dinero.Models.Response.Core;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Dinero.Controllers.Core
{
    public abstract class DnController: Controller
    {
        // Public Methods.
        public ErrorResponse error(HttpStatusCode statusCode, string message, int code)
        {
            Response.StatusCode = (int)statusCode;
            return new ErrorResponse(message, code);
        }

        public ErrorResponse error(HttpStatusCode statusCode, string message)
        {
            Response.StatusCode = (int)statusCode;
            return new ErrorResponse(message, -1);
        }

        public ErrorResponse error(HttpStatusCode statusCode)
        {
            Response.StatusCode = (int)statusCode;
            return new ErrorResponse(null, -1);
        }
    
        // Internal Type.
        public class ErrorResponse: IResponse
        {
            // Instance Variables.
            public int errorCode { get; set; }
            public string errorMessage { get; set; }

            // Constructors.
            public ErrorResponse(string errorMessage, int errorCode)
            {
                this.errorMessage = errorMessage;
                this.errorCode = errorCode;
            }
        }
    }
}
