﻿using Dinero.Controllers.Core;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Payment;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Dinero.Controllers.Payment
{
    [Route("api/payment/{paymentId}/cancel")]
    public class CancelPaymentController : UserController
    {
        // Instance Variables.
        private readonly IPaymentService paymentService;

        // Constructor.
        public CancelPaymentController(IUserService userService, IPaymentService paymentService) : base(userService)
        {
            this.paymentService = paymentService;
        }

        // Methods.
        [HttpPost]
        public async Task<IResponse> post(string paymentId)
        {
            try
            {
                var internalPaymentId = paymentService.id.decode(paymentId);
                var payment = paymentService.find.byId(internalPaymentId);

                // Payment must be pending to be canceled.
                if (!payment.isPending)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "A payment can only be canceled when it is pending.");
                }

                // User must be originator of payment to cancel it.
                if (payment.UserId != user.Id)
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Access denied.");
                }

                // Cancel payment with paymentService.
                await paymentService.cancelPayment(payment);

                return null;
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
