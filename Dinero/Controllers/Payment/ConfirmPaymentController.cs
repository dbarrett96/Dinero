﻿using Dinero.Controllers.Core;
using Dinero.Models.Database;
using Dinero.Models.Request;
using Dinero.Models.Response.Core;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.CustomerSession;
using Dinero.Services.Payment;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dinero.Controllers.Payment
{
    [Route("api/payment/{paymentId}/confirm")]
    public class ConfirmPaymentController : UserController
    {
        // Instance Variables.
        private readonly IPaymentService paymentService;
        private readonly IBillGroupService billGroupService;
        private readonly ITableGuestService tableGuestService;
        private readonly ICustomerSessionService customerSessionService;
        private readonly ITableSessionService tableSessionService;

        // Constructor.
        public ConfirmPaymentController(
            IUserService userService, 
            IPaymentService paymentService,
            IBillGroupService billGroupService,
            ITableGuestService tableGuestService,
            ICustomerSessionService customerSessionService,
            ITableSessionService tableSessionService) : base(userService)
        {
            this.paymentService = paymentService;
            this.billGroupService = billGroupService;
            this.tableGuestService = tableGuestService;
            this.customerSessionService = customerSessionService;
            this.tableSessionService = tableSessionService;
        }

        // Methods.
        [HttpPost]
        public async Task<IResponse> post(string paymentId, [FromBody] ConfirmPaymentRequest body)
        {
            try
            {
                var internalPaymentId = paymentService.id.decode(paymentId);
                var payment = paymentService.find.byId(internalPaymentId);

                // Make sure payment is pending.
                if (!payment.isPending)
                {
                    return error(HttpStatusCode.BadRequest, "Payment must be pending to be confirmed.");
                }

                // Make sure user is payment originator.
                if (payment.UserId != user.Id)
                {
                    return error(HttpStatusCode.BadRequest, "Access Denied.");
                }

                // Confirm the payment.
                await paymentService.confirmPayment(payment, body.confirmationId);

                // Get the id of everyone in the user's bill group.
                var userAsTableGuest = tableGuestService.find.byUser(user);
                var billGroupTableGuestIds = billGroupService.find.byPayer(userAsTableGuest).Select(member => member.Guest);

                var tableGuestsToRemove = tableGuestService.find.whereIdIn(billGroupTableGuestIds);

                // Create the customer sessions.
                var tableSessionId = tableGuestsToRemove.First().TableSessionId;
                var customerSessions = tableGuestsToRemove.Select(guest => new CustomerSession(guest.UserId, tableSessionId, payment.Id));
                await customerSessionService.persist.Add(customerSessions);

                // Remove the table guests. Also removes order items, bill group, etc... via cascade.
                await tableGuestService.removeGuests(tableGuestsToRemove);

                // Check if everyone in session has paid...if true, end session.
                if (tableGuestService.find.manyByTableSessionId(tableSessionId).Count == 0)
                {
                    var tableSession = tableSessionService.find.byId(tableSessionId);
                    await tableSessionService.endSession(tableSession);
                }

                return null;
            }
            catch (DnException e)
            {
                return error(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
