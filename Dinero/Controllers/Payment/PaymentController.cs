﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Payment;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Dinero.Util.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.Payment
{
    [Route("api/payment/{paymentId}")]
    public class PaymentController : UserController
    {
        // Instance Variables.
        private readonly IPaymentService paymentService;
        private readonly ICurrencyService currencyService;
        private readonly ITableSessionService tableSessionService;

        // Constructor.
        public PaymentController(IUserService userService, IPaymentService paymentService, ICurrencyService currencyService, ITableSessionService tableSessionService) : base(userService)
        {
            this.paymentService = paymentService;
            this.currencyService = currencyService;
            this.tableSessionService = tableSessionService;
        }

        [HttpGet]
        public IResponse get(string paymentId)
        {
            try
            {
                // Convert id.
                var internalPaymentId = paymentService.id.decode(paymentId);

                // Fetch payment.
                var payment = paymentService.find.byId(internalPaymentId);

                // Make sure it belongs to user.
                if (payment.UserId != user.Id)
                {
                    return error(System.Net.HttpStatusCode.Unauthorized, "User is not allowed to see the payment.");
                }

                // return payment details.
                return new PaymentResponse(
                    paymentService.id.encode(payment.Id),
                    userService.id.encode(user.Id),
                    tableSessionService.id.encode(payment.TableSessionId),
                    payment.PaymentId,
                    payment.PaymentConfirmation,
                    payment.provider.ToString(),
                    currencyService.buildPrice(payment.Amount, payment.Currency.ToCurrency() ?? Util.Currency.CAD),
                    payment.state.ToString());
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
