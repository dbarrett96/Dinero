﻿using Dinero.Controllers.Core;
using Dinero.Models.Request;
using Dinero.Models.Response.Core;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.Order;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.Checkout
{
    [Route("api/checkout")]
    public class CheckoutController : UserController
    {
        // Instance Variables
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestService tableGuestService;
        private readonly IOrderService orderService;
        private readonly IBillGroupService billGroupService;

        // Constructor.
        public CheckoutController(
            IUserService userService,
            ITableSessionService tableSessionService,
            ITableGuestService tableGuestService,
            IOrderService orderService, 
            IBillGroupService billGroupService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestService = tableGuestService;
            this.orderService = orderService;
            this.billGroupService = billGroupService;
        }

        [HttpPost]
        public async Task<IResponse> post([FromBody]CheckoutRequest body)
        {
            try
            {
                // Convert Ids.
                var internalUserIds = body.users.Select(id => userService.id.decode(id));

                // Get the table session the user is part of.
                var session = tableSessionService.find.byUser(user);

                // Get table guests for each id.
                var billGroupMembers = tableGuestService.find.whereUserIdIn(internalUserIds);

                // Check that they are all in the current user's session.
                foreach (var tableGuest in billGroupMembers)
                {
                    if (tableGuest.TableSessionId != session.Id)
                    {
                        return error(System.Net.HttpStatusCode.BadRequest, "User cannot pay for someone sitting at another table.");
                    }
                }

                // Check that someone is not already paying for them.
                foreach (var tableGuest in billGroupMembers)
                {
                    if (billGroupService.isGuestBeingPaidFor(tableGuest))
                    {
                        var guestAsUser = userService.find.byId(tableGuest.UserId);
                        return error(System.Net.HttpStatusCode.BadRequest, $"Somone else is already paying for {guestAsUser.FirstName} {guestAsUser.LastName}.");
                    }
                }

                // Create bill group.
                var userAsTableGuest = tableGuestService.find.byUser(user);
                await billGroupService.create(userAsTableGuest, billGroupMembers);

                return null;
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
            
        }
    }
}
