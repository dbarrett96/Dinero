﻿using Dinero.Controllers.Core;
using Dinero.Models.Response.Core;
using Dinero.Services.BillGroup;
using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Dinero.Controllers.Checkout
{
    [Route("api/checkout")]
    public class CancelCheckoutController : UserController
    {
        // Instance Variables.
        private readonly IBillGroupService billGroupService;
        private readonly ITableGuestService tableGuestService;

        // Constructor.
        public CancelCheckoutController(IUserService userService, IBillGroupService billGroupService, ITableGuestService tableGuestService) : base(userService)
        {
            this.billGroupService = billGroupService;
            this.tableGuestService = tableGuestService;
        }

        [HttpDelete]
        public async Task<IResponse> delete()
        {
            try
            {
                var userAsTableGuest = tableGuestService.find.byUser(user);
                await billGroupService.deleteGroupLeadBy(userAsTableGuest);

                return null;
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
