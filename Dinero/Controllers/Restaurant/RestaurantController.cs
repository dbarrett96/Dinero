﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Restaurant;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.Restaurant
{
    [Route("api/restaurant/{restaurantId}")]
    public class RestaurantController: UserController
    {
        // Instance Variables.
        private readonly IRestaurantService restaurantService;

        // Constructor.
        public RestaurantController(IUserService userService, IRestaurantService restaurantService): base(userService)
        {
            this.restaurantService = restaurantService;
        }

        /// <summary>
        /// Get details about a specific restaurant.
        /// </summary>
        /// <param name="restaurantId">The unique id for the restaurant.</param>
        /// <returns>Details about the restaurant.</returns>
        [HttpGet]
        public IResponse get(string restaurantId)
        {
            try
            {
                var internalRestaurantId = restaurantService.id.decode(restaurantId);
                var restaurant = restaurantService.find.byId(internalRestaurantId);

                return new RestaurantResponse(
                    restaurantId,
                    restaurant.Name,
                    restaurant.Address,
                    restaurant.City, 
                    restaurant.State, 
                    restaurant.Country);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
