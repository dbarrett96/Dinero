﻿using Dinero.Controllers.Core;
using Dinero.Models.Request;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Dinero.Controllers.Account
{
    [Route("api/signUp")]
    public class SignUpController : DnController
    {
        // Private Instance Variables.
        private readonly IUserService userService;

        // Constructor
        public SignUpController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// Sign up a user to the platform.
        /// </summary>
        /// <param name="body">The user parameters.</param
        [HttpPost]
        public async Task<IResponse> Post([FromBody]SignUpRequest body)
        {
            try
            {
                var user = userService.find.byPhoneNumber(body.phoneNumber);
                return error(HttpStatusCode.Conflict, "User already exists.");
            }
            catch
            {
                var user = await userService.signUp.byPhoneNumber(body.phoneNumber, body.firstName, body.lastName);
                var userSignInSecret = await userService.signIn.byUser(user);

                return new SignInResponse(userSignInSecret.Secret);
            }
        }
    }
}
