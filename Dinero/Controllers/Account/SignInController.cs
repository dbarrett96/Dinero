﻿using Dinero.Controllers.Core;
using Dinero.Models.Request;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Dinero.Controllers.Account
{
    [Route("api/signIn")]
    public class SignInController: DnController
    {
        // Private Instance Variables.
        private readonly IUserService userService;

        // Constructor
        public SignInController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        public async Task<IResponse> Post([FromBody]SignInRequest body)
        {
            try
            {
                var user = userService.find.byPhoneNumber(body.phoneNumber);
                var userSignInSecret = await userService.signIn.byUser(user);

                return new SignInResponse(userSignInSecret.Secret);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
