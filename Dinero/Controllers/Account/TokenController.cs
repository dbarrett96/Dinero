﻿using Dinero.Controllers.Core;
using Dinero.Models.Database;
using Dinero.Models.Request;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.SignIn;
using Dinero.Services.Token;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.Account
{
    [Route("api/token")]
    public class TokenController: DnController
    {
        // Instance Variables.
        private readonly ISignInService signInService;
        private readonly IUserService userService;
        private readonly ITokenService tokenService;

        // Constructor.
        public TokenController(ISignInService signInService, IUserService userService, ITokenService tokenService)
        {
            this.signInService = signInService;
            this.userService = userService;
            this.tokenService = tokenService;
        }

        // Endpoints.
        [HttpPost]
        public IResponse Post([FromBody] TokenRequest body)
        {
            return signInUser(body.pin, body.signInSecret);
        }

        // Private Methods.
        private IResponse signInUser(string pin, string signInSecret)
        {
            try
            {
                var secret = signInService.findSecret.bySecret(signInSecret);

                if (secret.Pin == pin)
                {
                    var user = userService.find.byPhoneNumber(secret.PhoneNumber);
                    return generateTokenForUser(user);
                }
                else
                {
                    return error(System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }   
        }

        private TokenResponse generateTokenForUser(Models.Database.User user)
        {
            Token userToken;

            try
            {
                userToken = tokenService.find.byUser(user);
                userToken.refresh();
                tokenService.persist.Save();
            }
            catch
            {
                userToken = new Token(user.Id);
                tokenService.persist.Add(userToken);
            }

            return new TokenResponse(userToken.AccessToken, userToken.RefreshToken);
        }
    }
}
