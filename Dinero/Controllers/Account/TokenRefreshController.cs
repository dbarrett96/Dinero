﻿using Dinero.Controllers.Core;
using Dinero.Models.Request;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.Token;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Dinero.Controllers.Account
{
    [Route("api/token/refresh")]
    public class TokenRefreshController: DnController
    {
        // Instance Variables.
        private readonly ITokenService tokenService;

        // Constructor.
        public TokenRefreshController(ITokenService tokenService)
        {
            this.tokenService = tokenService;
        }

        // Endpoints
        [HttpPost]
        public async Task<IResponse> Post([FromBody]TokenRefreshRequest body)
        {
            try
            {
                var token = tokenService.find.byRefreshToken(body.refresh_token);

                token.refresh();
                await tokenService.persist.Save();

                return new TokenResponse(token.AccessToken, token.RefreshToken);
            }
            catch (DnException e)
            {
                return error(HttpStatusCode.BadRequest, "Token is invalid or expired.");
            }
        }
    }
}
