﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.CustomerSession;
using Dinero.Services.Restaurant;
using Dinero.Services.Table;
using Dinero.Services.TableAlias;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Controllers.Session
{
    [Route("api/session")]
    public class CurrentSessionController : UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableService tableService;
        private readonly IRestaurantService restaurantService;
        private readonly ITableGuestService tableGuestService;
        private readonly ICustomerSessionService customerSessionService;
        private readonly ITableAliasService tableAliasService;

        // Constructor.
        public CurrentSessionController(
            IUserService userService, 
            ITableSessionService tableSessionService,
            ITableService tableService, 
            IRestaurantService restaurantService, 
            ITableGuestService tableGuestService,
            ICustomerSessionService customerSessionService,
            ITableAliasService tableAliasService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableService = tableService;
            this.restaurantService = restaurantService;
            this.tableGuestService = tableGuestService;
            this.tableAliasService = tableAliasService;
            this.customerSessionService = customerSessionService;
        }

        [HttpGet]
        public IResponse get()
        {
            try
            {
                var tableSession = tableSessionService.find.byUser(user);
                var table = tableService.find.byId(tableSession.TableId);

                var sessionIsPaidFor = false;

                // Check if user has a customer session for the tableSession.
                if (customerSessionService.userHasSessionAt(tableSession, user))
                {
                    sessionIsPaidFor = true;
                }
                else if (tableGuestService.userIsTableGuestAt(tableSession, user))
                {
                    sessionIsPaidFor = false;
                }
                else
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Access denied.");
                }

                var sessionIsComplete = tableSession.EndedAt != null;
                var tableAlias = tableAliasService.find.byTableId(tableSession.TableId);

                return new SessionResponse(
                    tableSessionService.id.encode(tableSession.Id),
                    restaurantService.id.encode(table.ResturantId),
                    tableService.id.encode(table.Id),
                    sessionIsPaidFor,
                    sessionIsComplete);

            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
