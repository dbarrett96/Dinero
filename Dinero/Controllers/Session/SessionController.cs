﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.Core;
using Dinero.Services.CustomerSession;
using Dinero.Services.TableAlias;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.Session
{
    [Route("api/session/{sessionId}")]
    public class SessionController : UserController
    {
        // Instance Variables.
        private readonly ITableSessionService tableSessionService;
        private readonly ITableGuestService tableGuestService;
        private readonly ICustomerSessionService customerSessionService;
        private readonly ITableAliasService tableAliasService;

        // Constructor.
        public SessionController(
            IUserService userService,
            ITableSessionService tableSessionService,
            ITableGuestService tableGuestService,
            ICustomerSessionService customerSessionService,
            ITableAliasService tableAliasService) : base(userService)
        {
            this.tableSessionService = tableSessionService;
            this.tableGuestService = tableGuestService;
            this.customerSessionService = customerSessionService;
            this.tableAliasService = tableAliasService;
        }

        // Methods.
        [HttpGet]
        public IResponse get(string sessionId)
        {
            try
            {
                // Get session.
                var sessionIsPaidFor = false;
                var internalSessionId = tableSessionService.id.decode(sessionId);
                var tableSession = tableSessionService.find.byId(internalSessionId);

                // Check if user has a customer session for the tableSession.
                if (customerSessionService.userHasSessionAt(tableSession, user))
                {
                    sessionIsPaidFor = true;
                }
                else if (tableGuestService.userIsTableGuestAt(tableSession, user))
                {
                    sessionIsPaidFor = false;
                }
                else
                {
                    return error(System.Net.HttpStatusCode.BadRequest, "Access denied.");
                }

                var sessionIsComplete = tableSession.EndedAt != null;
                var tableAlias = tableAliasService.find.byTableId(tableSession.TableId);

                return new SessionResponse(sessionId, null, tableAlias.Alias, sessionIsPaidFor, sessionIsComplete);
            }
            catch (DnException e)
            {
                return error(System.Net.HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}
