﻿using Dinero.Controllers.Core;
using Dinero.Models.Response;
using Dinero.Models.Response.Core;
using Dinero.Services.User;
using Microsoft.AspNetCore.Mvc;

namespace Dinero.Controllers.User
{
    [Route("api/me")]
    public class UserProfileController: UserController
    {
        // Constructor.
        public UserProfileController(IUserService userService): base(userService) { }

        [HttpGet]
        /// <summary>
        /// Get the current user's profile.
        /// </summary>
        /// <returns>The current user's profile.</returns>
        public IResponse get()
        {
            return new UserProfileResponse(user.FirstName, user.LastName, user.PhoneNumber);
        }
    }
}

