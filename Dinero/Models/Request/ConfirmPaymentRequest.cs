﻿namespace Dinero.Models.Request
{
    public class ConfirmPaymentRequest
    {
        public string confirmationId { get; set; }
    }
}
