﻿using System.Collections.Generic;

namespace Dinero.Models.Request
{
    public class CheckoutRequest
    {
        /// <summary>
        /// User id's of the users who the current user is checking out for.
        /// </summary>
        public ICollection<string> users;
    }
}
