﻿namespace Dinero.Models.Request
{
    public class RequestSeatRequest
    {
        /// <summary>
        /// The identifier for the table the user wishes to join.
        /// </summary>
        public string table { get; set; }
    }
}
