﻿namespace Dinero.Models.Request
{
    public class SignUpRequest
    {
        public string phoneNumber { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}
