﻿namespace Dinero.Models.Request
{
    public class TokenRequest
    {
        public string pin { get; set; }
        public string signInSecret { get; set; }
    }
}
