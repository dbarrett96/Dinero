﻿using System.Collections.Generic;

namespace Dinero.Models.Request
{
    public class AddOrderItemRequest
    {
        public IDictionary<string, ICollection<string>> items;
    }
}
