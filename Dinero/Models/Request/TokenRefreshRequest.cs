﻿namespace Dinero.Models.Request
{
    public class TokenRefreshRequest
    {
        public string refresh_token { get; set; }
    }
}
