﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Models.Database
{
    public partial class TableSession
    {
        /// <summary>
        /// Create a new TableSession.
        /// </summary>
        /// <param name="tableId">The table id where the new session will take place.</param>
        public TableSession(long tableId)
        {
            TableId = tableId;
            StartedAt = DateTime.Now;
            EndedAt = null;
        }
    }
}
