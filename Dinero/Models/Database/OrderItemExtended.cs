﻿using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class OrderItem
    {
        // Constructor
        public OrderItem(long menuItemId, long tableGuestid, ICollection<OrderItemOption> options)
        {
            MenuItemId = menuItemId;
            TableGuestId = tableGuestid;
            State = 0;
            OrderItemOption = options;
        }

        /// <summary>
        /// State converted to an enum.
        /// </summary>
        public Status status
        {
            get
            {
                switch (State)
                {
                    case 0: return Status.WAITING;
                    case 1: return Status.WAITING_FOR_GROUP;
                    case 2: return Status.ORDERED;
                    default: return Status.WAITING;
                }
            }
        }

        public enum Status: byte
        {
            WAITING, WAITING_FOR_GROUP, ORDERED
        }
    }
}
