﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class Restaurant
    {
        public Restaurant()
        {
            MenuItem = new HashSet<MenuItem>();
            Table = new HashSet<Table>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public ICollection<MenuItem> MenuItem { get; set; }
        public ICollection<Table> Table { get; set; }
    }
}
