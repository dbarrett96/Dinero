﻿namespace Dinero.Models.Database
{
    public partial class OrderItemOption
    {
        public OrderItemOption() { }

        public OrderItemOption(MenuItemOption option)
        {
            MenuItemOption = option;
        }
    }
}
