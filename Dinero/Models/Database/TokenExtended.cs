﻿using Dinero.Util;

namespace Dinero.Models.Database
{
    public partial class Token
    {
        // Private Static Variables.
        private const int TOKEN_LENGTH = 256;

        // Constructors.
        public Token() { }
        public Token(long userId)
        {
            UserId = userId;
            refresh();
        }

        // Methods.
        public void refresh()
        {
            AccessToken = KeyGenerator.AlphaNumeric(TOKEN_LENGTH);
            RefreshToken = KeyGenerator.AlphaNumeric(TOKEN_LENGTH);
        }
    }
}
