﻿using Dinero.Util;
using Dinero.Util.Extensions;

namespace Dinero.Models.Database
{
    public partial class MenuItemOption
    {
        public Currency? CurrencyType
        {
            get
            {
                return Currency.Value.ToCurrency();
            }
        }
    }
}
