﻿using Dinero.Services.Core;

namespace Dinero.Models.Database
{
    public partial class TableGuestRequest
    {
        /// <summary>
        /// State enum type mapped from the Status byte.
        /// </summary>
        public enum State : byte
        {
            CANCELED, PENDING, DECLINED, REJECTED, EXPIRED, ACCEPTED
        };

        /// <summary>
        /// The mapped Status of the request.
        /// </summary>
        public State requestState
        {
            get
            {
                switch (Status)
                {
                    case 0: return State.CANCELED;
                    case 1: return State.PENDING;
                    case 2: return State.DECLINED;
                    case 3: return State.REJECTED;
                    case 4: return State.EXPIRED;
                    case 5: return State.ACCEPTED;
                    default: return State.EXPIRED;
                }
            }
        }

        // Constructors.
        public TableGuestRequest() { }
        public TableGuestRequest(long userId, long tableId)
        {
            UserId = userId;
            TableId = tableId;
            ExpiresAt = System.DateTime.Now.AddHours(2);
            Status = State.PENDING.toByte();
        }
    }

    public static class TableGuestRequestStateExtensions
    {
        public static byte toByte(this TableGuestRequest.State state)
        {
            return (byte)state;
        }

        public static string ToString(this TableGuestRequest.State state)
        {
            if (state == TableGuestRequest.State.CANCELED)
            {
                return "CANCELED";
            }
            else if (state == TableGuestRequest.State.PENDING)
            {
                return "PENDING";
            }
            else if (state == TableGuestRequest.State.DECLINED)
            {
                return "DECLINED";
            }
            else if (state == TableGuestRequest.State.REJECTED)
            {
                return "REJECTED";
            }
            else if (state == TableGuestRequest.State.ACCEPTED)
            {
                return "ACCEPTED";
            }
            else if (state == TableGuestRequest.State.EXPIRED)
            {
                return "EXPIRED";
            }
            else
            {
                return "??";
            }

        }

        public static TableGuestRequest.State ToTableGuestRequestState(this string state)
        {
            var states = new TableGuestRequest.State[] {
                TableGuestRequest.State.CANCELED,
                TableGuestRequest.State.PENDING,
                TableGuestRequest.State.DECLINED,
                TableGuestRequest.State.REJECTED,
                TableGuestRequest.State.ACCEPTED,
                TableGuestRequest.State.EXPIRED
            };

            foreach(var loopState in states)
            {
                if(loopState.ToString() == state)
                {
                    return loopState;
                }
            }

            throw new DnException("Could not decode TableGuestRequest.State.", null);
        }
    }
}
