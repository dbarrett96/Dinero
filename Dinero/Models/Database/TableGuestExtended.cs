﻿namespace Dinero.Models.Database
{
    public partial class TableGuest
    {
        // Constructors.
        public TableGuest(long userId, long tableSessionId, bool isGroupLeader)
        {
            UserId = userId;
            TableSessionId = tableSessionId;
            IsGroupLeader = isGroupLeader;
        }
    }
}
