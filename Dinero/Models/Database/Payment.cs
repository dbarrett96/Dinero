﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class Payment
    {
        public Payment()
        {
            CustomerSession = new HashSet<CustomerSession>();
        }

        public long Id { get; set; }
        public long UserId { get; set; }
        public long TableSessionId { get; set; }
        public string PaymentId { get; set; }
        public string PaymentConfirmation { get; set; }
        public short Provider { get; set; }
        public long Amount { get; set; }
        public short Currency { get; set; }
        public byte Status { get; set; }

        public TableSession TableSession { get; set; }
        public User User { get; set; }
        public ICollection<CustomerSession> CustomerSession { get; set; }
    }
}
