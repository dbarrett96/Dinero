﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class TableSession
    {
        public TableSession()
        {
            CustomerSession = new HashSet<CustomerSession>();
            Payment = new HashSet<Payment>();
            TableGuest = new HashSet<TableGuest>();
        }

        public long Id { get; set; }
        public long TableId { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime? EndedAt { get; set; }

        public Table Table { get; set; }
        public ICollection<CustomerSession> CustomerSession { get; set; }
        public ICollection<Payment> Payment { get; set; }
        public ICollection<TableGuest> TableGuest { get; set; }
    }
}
