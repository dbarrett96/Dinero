﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class SigninSecret
    {
        public string PhoneNumber { get; set; }
        public string Pin { get; set; }
        public string Secret { get; set; }

        public User PhoneNumberNavigation { get; set; }
    }
}
