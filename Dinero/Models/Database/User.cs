﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class User
    {
        public User()
        {
            CustomerSession = new HashSet<CustomerSession>();
            Payment = new HashSet<Payment>();
            TableGuestRequest = new HashSet<TableGuestRequest>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public SigninSecret SigninSecret { get; set; }
        public TableGuest TableGuest { get; set; }
        public Token Token { get; set; }
        public ICollection<CustomerSession> CustomerSession { get; set; }
        public ICollection<Payment> Payment { get; set; }
        public ICollection<TableGuestRequest> TableGuestRequest { get; set; }
    }
}
