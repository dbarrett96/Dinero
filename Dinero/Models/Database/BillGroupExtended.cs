﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Models.Database
{
    public partial class BillGroup
    {
        // Constructors.
        public BillGroup() { }
        public BillGroup(long payerTableGuestId, long payeeTableGuestId)
        {
            GroupLeader = payerTableGuestId;
            Guest = payeeTableGuestId;
        }
    }
}
