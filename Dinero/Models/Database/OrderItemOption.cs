﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class OrderItemOption
    {
        public long Id { get; set; }
        public long OrderItemId { get; set; }
        public long MenuItemOptionId { get; set; }

        public MenuItemOption MenuItemOption { get; set; }
        public OrderItem OrderItem { get; set; }
    }
}
