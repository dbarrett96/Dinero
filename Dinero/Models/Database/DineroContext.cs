﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Dinero.Models.Database
{
    public partial class DineroContext : DbContext
    {
        public virtual DbSet<BillGroup> BillGroup { get; set; }
        public virtual DbSet<CustomerSession> CustomerSession { get; set; }
        public virtual DbSet<Fee> Fee { get; set; }
        public virtual DbSet<MenuItem> MenuItem { get; set; }
        public virtual DbSet<MenuItemFee> MenuItemFee { get; set; }
        public virtual DbSet<MenuItemOption> MenuItemOption { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<OrderItemFee> OrderItemFee { get; set; }
        public virtual DbSet<OrderItemOption> OrderItemOption { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<Restaurant> Restaurant { get; set; }
        public virtual DbSet<SigninSecret> SigninSecret { get; set; }
        public virtual DbSet<Table> Table { get; set; }
        public virtual DbSet<TableAlias> TableAlias { get; set; }
        public virtual DbSet<TableGuest> TableGuest { get; set; }
        public virtual DbSet<TableGuestRequest> TableGuestRequest { get; set; }
        public virtual DbSet<TableSession> TableSession { get; set; }
        public virtual DbSet<Token> Token { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BillGroup>(entity =>
            {
                entity.HasKey(e => new { e.GroupLeader, e.Guest });

                entity.HasOne(d => d.GroupLeaderNavigation)
                    .WithMany(p => p.BillGroupGroupLeaderNavigation)
                    .HasForeignKey(d => d.GroupLeader)
                    .HasConstraintName("FK_BillGroup_TableGuest");

                entity.HasOne(d => d.GuestNavigation)
                    .WithMany(p => p.BillGroupGuestNavigation)
                    .HasForeignKey(d => d.Guest)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BillGroup_TableGuest1");
            });

            modelBuilder.Entity<CustomerSession>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.TableSessionId });

                entity.HasOne(d => d.Payment)
                    .WithMany(p => p.CustomerSession)
                    .HasForeignKey(d => d.PaymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSession_Payment");

                entity.HasOne(d => d.TableSession)
                    .WithMany(p => p.CustomerSession)
                    .HasForeignKey(d => d.TableSessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSession_TableSession");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.CustomerSession)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSession_User");
            });

            modelBuilder.Entity<Fee>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<MenuItem>(entity =>
            {
                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(512);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.MenuItem)
                    .HasForeignKey(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MenuItem_Restaurant");
            });

            modelBuilder.Entity<MenuItemFee>(entity =>
            {
                entity.HasKey(e => new { e.MenuItemId, e.FeeId });

                entity.HasOne(d => d.Fee)
                    .WithMany(p => p.MenuItemFee)
                    .HasForeignKey(d => d.FeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MenuItemFee_Fee");

                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.MenuItemFee)
                    .HasForeignKey(d => d.MenuItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MenuItemFee_MenuItem");
            });

            modelBuilder.Entity<MenuItemOption>(entity =>
            {
                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.MenuItemOption)
                    .HasForeignKey(d => d.MenuItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MenuItemOption_MenuItem");
            });

            modelBuilder.Entity<OrderItem>(entity =>
            {
                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.OrderItem)
                    .HasForeignKey(d => d.MenuItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderItem2_MenuItem1");

                entity.HasOne(d => d.TableGuest)
                    .WithMany(p => p.OrderItem)
                    .HasForeignKey(d => d.TableGuestId)
                    .HasConstraintName("FK_OrderItem_TableGuest");
            });

            modelBuilder.Entity<OrderItemFee>(entity =>
            {
                entity.HasKey(e => new { e.OrderItemId, e.FeeId });

                entity.HasOne(d => d.Fee)
                    .WithMany(p => p.OrderItemFee)
                    .HasForeignKey(d => d.FeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderItemFee_Fee");

                entity.HasOne(d => d.OrderItem)
                    .WithMany(p => p.OrderItemFee)
                    .HasForeignKey(d => d.OrderItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderItemFee_OrderItem");
            });

            modelBuilder.Entity<OrderItemOption>(entity =>
            {
                entity.HasOne(d => d.MenuItemOption)
                    .WithMany(p => p.OrderItemOption)
                    .HasForeignKey(d => d.MenuItemOptionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderItemOption2_MenuItemOption");

                entity.HasOne(d => d.OrderItem)
                    .WithMany(p => p.OrderItemOption)
                    .HasForeignKey(d => d.OrderItemId)
                    .HasConstraintName("FK_OrderItemOption2_OrderItem");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.Property(e => e.PaymentConfirmation)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentId)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.HasOne(d => d.TableSession)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.TableSessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payment_TableSession");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payment_User");
            });

            modelBuilder.Entity<Restaurant>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<SigninSecret>(entity =>
            {
                entity.HasKey(e => e.PhoneNumber);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Pin)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Secret)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.HasOne(d => d.PhoneNumberNavigation)
                    .WithOne(p => p.SigninSecret)
                    .HasPrincipalKey<User>(p => p.PhoneNumber)
                    .HasForeignKey<SigninSecret>(d => d.PhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SigninSecret_User");
            });

            modelBuilder.Entity<Table>(entity =>
            {
                entity.HasOne(d => d.Resturant)
                    .WithMany(p => p.Table)
                    .HasForeignKey(d => d.ResturantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Table_Restaurant");
            });

            modelBuilder.Entity<TableAlias>(entity =>
            {
                entity.HasKey(e => e.TableId);

                entity.Property(e => e.TableId).ValueGeneratedNever();

                entity.Property(e => e.Alias)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.HasOne(d => d.Table)
                    .WithOne(p => p.TableAlias)
                    .HasForeignKey<TableAlias>(d => d.TableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TableAlias2_Table");
            });

            modelBuilder.Entity<TableGuest>(entity =>
            {
                entity.HasIndex(e => e.UserId)
                    .HasName("IX_TableGuest")
                    .IsUnique();

                entity.HasOne(d => d.TableSession)
                    .WithMany(p => p.TableGuest)
                    .HasForeignKey(d => d.TableSessionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TableGuest2_TableSession1");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.TableGuest)
                    .HasForeignKey<TableGuest>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TableGuest2_User");
            });

            modelBuilder.Entity<TableGuestRequest>(entity =>
            {
                entity.Property(e => e.ExpiresAt).HasColumnType("datetime");

                entity.HasOne(d => d.Table)
                    .WithMany(p => p.TableGuestRequest)
                    .HasForeignKey(d => d.TableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TableGuestRequest2_Table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TableGuestRequest)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TableGuestRequest2_User");
            });

            modelBuilder.Entity<TableSession>(entity =>
            {
                entity.Property(e => e.EndedAt).HasColumnType("datetime");

                entity.Property(e => e.StartedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Table)
                    .WithMany(p => p.TableSession)
                    .HasForeignKey(d => d.TableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TableSession_Table2");
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.AccessToken)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Token)
                    .HasForeignKey<Token>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Token_User");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.PhoneNumber)
                    .HasName("IX_User")
                    .IsUnique();

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);
            });
        }
    }
}
