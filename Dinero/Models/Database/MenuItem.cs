﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class MenuItem
    {
        public MenuItem()
        {
            MenuItemFee = new HashSet<MenuItemFee>();
            MenuItemOption = new HashSet<MenuItemOption>();
            OrderItem = new HashSet<OrderItem>();
        }

        public long Id { get; set; }
        public long RestaurantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Currency { get; set; }
        public string Category { get; set; }

        public Restaurant Restaurant { get; set; }
        public ICollection<MenuItemFee> MenuItemFee { get; set; }
        public ICollection<MenuItemOption> MenuItemOption { get; set; }
        public ICollection<OrderItem> OrderItem { get; set; }
    }
}
