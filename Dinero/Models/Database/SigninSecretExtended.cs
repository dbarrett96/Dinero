﻿using Dinero.Util;

namespace Dinero.Models.Database
{
    public partial class SigninSecret
    {
        // Constructors.
        public SigninSecret() { }

        public SigninSecret(string phoneNumber)
        {
            PhoneNumber = phoneNumber;
            refresh();
        }

        // Methods.
        /// <summary>
        /// Set new automatically generated values for the Pin and Secret.
        /// </summary>
        public void refresh()
        {
            Pin = KeyGenerator.Numeric(6);
            Secret = KeyGenerator.AlphaNumeric(32);
        }
    }
}
