﻿using Dinero.Util;

namespace Dinero.Models.Database
{
    public partial class Payment
    {
        // Constructor.
        public Payment(long userId, long tableSessionId, string paymentNonce, PaymentProvider provider, long amount, Currency currency)
        {
            UserId = userId;
            TableSessionId = tableSessionId;
            PaymentId = paymentNonce;
            Provider = (short)provider;
            Amount = amount;
            Currency = (short)currency;
            Status = (byte)State.PENDING;
        }

        // Properties.
        public State state
        {
            get
            {
                switch (Status)
                {
                    case 0: return State.CANCELED;
                    case 1: return State.PENDING;
                    case 2: return State.CONFIRMED;
                    default: return State.CANCELED;
                }
            }
        }

        public PaymentProvider provider
        {
            get
            {
                switch (Provider)
                {
                    default: return PaymentProvider.TEST_PROVIDER;
                }
            }
        }

        public bool isPending
        {
            get
            {
                return state == State.PENDING;
            }
        }

        // Methods.
        public void setState(State state)
        {
            Status = (byte)state;
        }

        // Types.
        /// <summary>
        /// Enum type for mapping Status.
        /// </summary>
        public enum State: byte
        {
            CANCELED,
            PENDING,
            CONFIRMED
        }
    }
}
