﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class TableGuest
    {
        public TableGuest()
        {
            BillGroupGroupLeaderNavigation = new HashSet<BillGroup>();
            BillGroupGuestNavigation = new HashSet<BillGroup>();
            OrderItem = new HashSet<OrderItem>();
        }

        public long Id { get; set; }
        public long TableSessionId { get; set; }
        public long UserId { get; set; }
        public bool IsGroupLeader { get; set; }

        public TableSession TableSession { get; set; }
        public User User { get; set; }
        public ICollection<BillGroup> BillGroupGroupLeaderNavigation { get; set; }
        public ICollection<BillGroup> BillGroupGuestNavigation { get; set; }
        public ICollection<OrderItem> OrderItem { get; set; }
    }
}
