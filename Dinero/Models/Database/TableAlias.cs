﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class TableAlias
    {
        public long TableId { get; set; }
        public string Alias { get; set; }

        public Table Table { get; set; }
    }
}
