﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class Token
    {
        public long UserId { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }

        public User User { get; set; }
    }
}
