﻿using Dinero.Util;
using Dinero.Util.Extensions;
using System;

namespace Dinero.Models.Database
{
    public partial class MenuItem
    {
        public Currency? CurrencyType {
            get
            {
                return Currency.ToCurrency();
            }
        }

        public MenuItem Copy()
        {
            var menuItem = new MenuItem();
            menuItem.Id = Id;
            menuItem.RestaurantId = RestaurantId;
            menuItem.Name = Name;
            menuItem.Description = Description;
            menuItem.Price = Price;
            menuItem.Currency = Currency;
            menuItem.Category = Category;
            return menuItem;
        }
    }
}
