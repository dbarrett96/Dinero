﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class BillGroup
    {
        public long GroupLeader { get; set; }
        public long Guest { get; set; }

        public TableGuest GroupLeaderNavigation { get; set; }
        public TableGuest GuestNavigation { get; set; }
    }
}
