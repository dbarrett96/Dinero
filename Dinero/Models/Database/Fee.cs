﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class Fee
    {
        public Fee()
        {
            MenuItemFee = new HashSet<MenuItemFee>();
            OrderItemFee = new HashSet<OrderItemFee>();
        }

        public long Id { get; set; }
        public byte FeeType { get; set; }
        public string Description { get; set; }
        public long Amount { get; set; }
        public short? Currency { get; set; }

        public ICollection<MenuItemFee> MenuItemFee { get; set; }
        public ICollection<OrderItemFee> OrderItemFee { get; set; }
    }
}
