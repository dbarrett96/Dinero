﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class OrderItem
    {
        public OrderItem()
        {
            OrderItemFee = new HashSet<OrderItemFee>();
            OrderItemOption = new HashSet<OrderItemOption>();
        }

        public long Id { get; set; }
        public long MenuItemId { get; set; }
        public long TableGuestId { get; set; }
        public byte State { get; set; }

        public MenuItem MenuItem { get; set; }
        public TableGuest TableGuest { get; set; }
        public ICollection<OrderItemFee> OrderItemFee { get; set; }
        public ICollection<OrderItemOption> OrderItemOption { get; set; }
    }
}
