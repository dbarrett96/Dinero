﻿namespace Dinero.Models.Database
{
    public partial class CustomerSession
    {
        // Constructors.
        public CustomerSession() { }
        public CustomerSession(long userId, long tableSessionId, long paymentId)
        {
            UserId = userId;
            TableSessionId = tableSessionId;
            PaymentId = paymentId;
        }
    }
}
