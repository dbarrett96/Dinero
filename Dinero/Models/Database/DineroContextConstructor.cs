﻿using Microsoft.EntityFrameworkCore;

namespace Dinero.Models.Database
{
    public partial class DineroContext
    {
        public DineroContext(DbContextOptions<DineroContext> options) : base(options) { }
    }
}
