﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class CustomerSession
    {
        public long UserId { get; set; }
        public long TableSessionId { get; set; }
        public long PaymentId { get; set; }

        public Payment Payment { get; set; }
        public TableSession TableSession { get; set; }
        public User User { get; set; }
    }
}
