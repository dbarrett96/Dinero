﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class OrderItemFee
    {
        public long OrderItemId { get; set; }
        public long FeeId { get; set; }

        public Fee Fee { get; set; }
        public OrderItem OrderItem { get; set; }
    }
}
