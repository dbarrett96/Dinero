﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class Table
    {
        public Table()
        {
            TableGuestRequest = new HashSet<TableGuestRequest>();
            TableSession = new HashSet<TableSession>();
        }

        public long Id { get; set; }
        public long ResturantId { get; set; }
        public int Number { get; set; }
        public int Seats { get; set; }

        public Restaurant Resturant { get; set; }
        public TableAlias TableAlias { get; set; }
        public ICollection<TableGuestRequest> TableGuestRequest { get; set; }
        public ICollection<TableSession> TableSession { get; set; }
    }
}
