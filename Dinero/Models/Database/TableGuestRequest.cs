﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class TableGuestRequest
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long TableId { get; set; }
        public byte Status { get; set; }
        public DateTime ExpiresAt { get; set; }

        public Table Table { get; set; }
        public User User { get; set; }
    }
}
