﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class MenuItemFee
    {
        public long MenuItemId { get; set; }
        public long FeeId { get; set; }

        public Fee Fee { get; set; }
        public MenuItem MenuItem { get; set; }
    }
}
