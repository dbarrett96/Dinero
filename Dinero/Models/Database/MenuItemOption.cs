﻿using System;
using System.Collections.Generic;

namespace Dinero.Models.Database
{
    public partial class MenuItemOption
    {
        public MenuItemOption()
        {
            OrderItemOption = new HashSet<OrderItemOption>();
        }

        public long Id { get; set; }
        public long MenuItemId { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int? Surcharge { get; set; }
        public int? Currency { get; set; }

        public MenuItem MenuItem { get; set; }
        public ICollection<OrderItemOption> OrderItemOption { get; set; }
    }
}
