﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class SeatRequestResponse: IResponse
    {
        public string state { get; set; }

        public SeatRequestResponse(string state)
        {
            this.state = state;
        }
    }
}
