﻿using Dinero.Models.Response.Core;
using System.Collections.Generic;

namespace Dinero.Models.Response
{
    public class MenuResponse: IResponse
    {
        // Inner Type.
        public class Item
        {
            // Instance Variables.
            public string id;
            public string name;
            public string category;
            public string description;
            public string price;

            // Constructor.
            public Item(string id, string name, string category, string description, string price)
            {
                this.id = id;
                this.name = name;
                this.category = category;
                this.description = description;
                this.price = price;
            }
        }

        // Instance Variables.
        public string restaurantId;
        public IEnumerable<Item> items;

        // Constructor.
        public MenuResponse(string restaurantId, IEnumerable<Item> items)
        {
            this.restaurantId = restaurantId;
            this.items = items;
        }
    }
}
