﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class RestaurantResponse: IResponse
    {
        public string id;
        public string name;
        public string address;
        public string city;
        public string state;
        public string country;

        public RestaurantResponse(string id, string name, string address, string city, string state, string country)
        {
            this.id = id;
            this.name = name;
            this.address = address;
            this.city = city;
            this.state = state;
            this.country = country;
        }
    }
}
