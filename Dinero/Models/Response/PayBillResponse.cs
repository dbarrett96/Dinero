﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class PayBillResponse: IResponse
    {
        // Instance Variables.
        public string paymentId;
        public long amount;
        public string nonce;

        // Constructor.
        public PayBillResponse(string paymentId, long amount, string nonce)
        {
            this.paymentId = paymentId;
            this.amount = amount;
            this.nonce = nonce;
        }
    }
}
