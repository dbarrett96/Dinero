﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class SignInResponse: IResponse
    {
        // Instance Variables.
        public string secret { get; set; }

        // Constructor.
        public SignInResponse(string secret)
        {
            this.secret = secret;
        }
    }
}
