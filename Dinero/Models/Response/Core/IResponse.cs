﻿namespace Dinero.Models.Response.Core
{
    /// <summary>
    /// Interface type that all types returned from Dinero REST endpoints conform to.
    /// </summary>
    public interface IResponse {}
}
