﻿using Dinero.Models.Response.Core;
using System.Collections.Generic;

namespace Dinero.Models.Response
{
    public class OrderResponse: IResponse
    {
        // Inner Type.
        public class Fragment
        {
            // Inner Type.
            public class Item
            {
                // Inner Type.
                public class Option
                {
                    // Instance Variables.
                    public string optionId;
                    public string description;
                    public string price;

                    // Constructor.
                    public Option(string optionId, string description, string price)
                    {
                        this.optionId = optionId;
                        this.description = description;
                        this.price = price;
                    }
                }

                // Instance Variables.
                public string orderItemId;
                public string name;
                public string price;
                public ICollection<Option> options;

                // Constructor.
                public Item(string orderItemId, string name, string price, ICollection<Option> options)
                {
                    this.orderItemId = orderItemId;
                    this.name = name;
                    this.price = price;
                    this.options = options;
                }
            }

            // Instance Variables.
            public string total;
            public IDictionary<string, string> fees;
            public ICollection<Item> items;

            // Constructor.
            public Fragment(string total, IDictionary<string, string> fees, ICollection<Item> items)
            {
                this.total = total;
                this.fees = fees;
                this.items = items;
            }
        }

        public IDictionary<string, Fragment> fragments;

        // Constructor.
        public OrderResponse(IDictionary<string, Fragment> fragments)
        {
            this.fragments = fragments;
        }
    }
}
