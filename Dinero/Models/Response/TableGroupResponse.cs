﻿using Dinero.Models.Response.Core;
using System.Collections.Generic;

namespace Dinero.Models.Response
{
    public class TableGroupResponse: IResponse
    {
        // Inner Type.
        public class Member
        {
            public string id;
            public string firstName;
            public string lastName;
            public bool isGroupLeader;

            public Member(string id, string firstName, string lastName, bool isGroupLeader)
            {
                this.id = id;
                this.firstName = firstName;
                this.lastName = lastName;
                this.isGroupLeader = isGroupLeader;
            }
        }

        // Instance Variables.
        public IEnumerable<Member> members;

        // Constructor.
        public TableGroupResponse(IEnumerable<Member> members)
        {
            this.members = members;
        }
    }
}
