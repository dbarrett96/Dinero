﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class RequestSeatResponse: IResponse
    {
        public string id { get; set; }
        public string state { get; set; }

        public RequestSeatResponse(string id, string state) 
        {
            this.id = id;
            this.state = state;
        }
    }
}
