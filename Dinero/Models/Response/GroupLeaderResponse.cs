﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class GroupLeaderResponse : IResponse
    {
        // Instance Variables
        public string firstName { get; set; }
        public string lastName { get; set; }

        // Constructor.
        public GroupLeaderResponse(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}
