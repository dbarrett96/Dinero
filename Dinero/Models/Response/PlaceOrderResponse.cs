﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class PlaceOrderResponse: IResponse
    {
        // Instance Variables.
        public string status;

        // Constructor.
        public PlaceOrderResponse(string status)
        {
            this.status = status;
        }
    }
}
