﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class TokenResponse: IResponse
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }

        // Constructor.
        public TokenResponse(string accessToken, string refreshToken)
        {
            access_token = accessToken;
            refresh_token = refreshToken;
        }
    }
}
