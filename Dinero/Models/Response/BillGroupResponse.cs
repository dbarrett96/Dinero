﻿using Dinero.Models.Response.Core;
using System.Collections.Generic;

namespace Dinero.Models.Response
{
    public class BillGroupResponse : IResponse
    {
        // Inner type
        public class Member
        {
            public string id;
            public string firstName;
            public string lastName;

            public Member(string id, string firstName, string lastName)
            {
                this.id = id;
                this.firstName = firstName;
                this.lastName = lastName;
            }
        }

        // Instance Variables.
        public ICollection<Member> members;

        // Constructor.
        public BillGroupResponse(ICollection<Member> members)
        {
            this.members = members;
        }
    }
}
