﻿using Dinero.Models.Response.Core;
using System.Collections.Generic;

namespace Dinero.Models.Response
{
    public class BillResponse : OrderResponse.Fragment, IResponse
    {
        // Constructor.
        public BillResponse(string total, IDictionary<string, string> fees, ICollection<Item> items) : base(total, fees, items) { }
    }
}
