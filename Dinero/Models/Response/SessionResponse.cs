﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class SessionResponse: IResponse
    {
        // Instance Variables.
        public string sessionId;
        public string restaurantId;
        public string tableId;
        public bool isPaidFor;
        public bool isComplete;

        // Constructor.
        public SessionResponse(string sessionId, string restaurantId, string tableId, bool isPaidFor, bool isComplete)
        {
            this.sessionId = sessionId;
            this.restaurantId = restaurantId;
            this.tableId = tableId;
            this.isPaidFor = isPaidFor;
            this.isComplete = isComplete;
        }
    }
}
