﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class PaymentResponse: IResponse
    {
        // Instance Variables.
        public string id;
        public string userId;
        public string tableSessionId;
        public string nonce;
        public string paymentConfirmation;
        public string provider;
        public string amount;
        public string status;

        // Constructor.
         public PaymentResponse(
            string id,
            string userId,
            string tableSessionId,
            string nonce,
            string paymentConfirmation,
            string provider,
            string amount,
            string status)
        {
            this.id = id;
            this.userId = id;
            this.tableSessionId = tableSessionId;
            this.nonce = nonce;
            this.paymentConfirmation = paymentConfirmation;
            this.provider = provider;
            this.amount = amount;
            this.status = status;
        }
    }
}
