﻿using Dinero.Models.Response.Core;
using System.Collections.Generic;

namespace Dinero.Models.Response
{
    public class MenuItemResponse: MenuResponse.Item, IResponse
    {
        public class Option
        {
            public string id;
            public string description;
            public string category;
            public string price;

            // Constructor.
            public Option(string id, string description, string category, string price)
            {
                this.id = id;
                this.description = description;
                this.category = category;
                this.price = price;
            }
        }

        // Instance Variables.
        public IEnumerable<Option> options;

        // Constructor.
        public MenuItemResponse(string id, string name, string category, string description, string price, IEnumerable<Option> options) : base(id, name, category, description, price)
        {
            this.options = options;
        }
    }
}
