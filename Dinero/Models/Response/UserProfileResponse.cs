﻿using Dinero.Models.Response.Core;

namespace Dinero.Models.Response
{
    public class UserProfileResponse: IResponse
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phoneNumber { get; set; }

        public UserProfileResponse(string firstName, string lastName, string phoneNumber)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
        }
    }
}
