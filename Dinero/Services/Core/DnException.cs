﻿using System;

namespace Dinero.Services.Core
{
    /// <summary>
    /// Base Exception type for all Dinero domain Exceptions.
    /// </summary>
    public class DnException: Exception
    {
        // Instance Variables.
        public Exception internalSource;

        public DnException(string message) : base(message)
        {
        }

        // Constructors.
        public DnException(string message, Exception internalSource): base(message)
        {
            this.internalSource = internalSource;
        }
    }
}
