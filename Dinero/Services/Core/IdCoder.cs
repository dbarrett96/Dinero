﻿using System;

namespace Dinero.Services.Core
{
    // Interface.
    public interface IIdCoder
    {
        string encode(long internalId);
        long decode(string externalId);
    }

    // Implementation.
    public class IdCoder : IIdCoder
    {
        public string encode(long internalId)
        {
            return internalId.ToString();
        }

        public long decode(string externalId)
        {
            return Int64.Parse(externalId, System.Globalization.NumberStyles.Integer);
        }
    }
}
