﻿using Dinero.Util;

namespace Dinero.Services.Core
{
    // Interface 
    public interface ICurrencyService
    {
        string buildPrice(long price, Currency currency);
    }

    // Implmentation.
    public class CurrencyService : ICurrencyService
    {
        public string buildPrice(long price, Currency currency)
        {
            return $"${(price / 100.0).ToString("F")}";
        }
    }
}
