﻿using Dinero.Models.Database;

namespace Dinero.Services.Core
{
    // Interface.
    public interface IMutator<T>
    {
        void delete(T obj);
    }

    // Implementation.
    public abstract class Mutator<T> : ContextAdapter, IMutator<T>
    {
        // Contstructor.
        public Mutator(DineroContext database) : base(database) { }

        // Abstract Methods.
        public abstract void delete(T obj);
    }
}
