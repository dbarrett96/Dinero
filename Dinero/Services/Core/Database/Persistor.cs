﻿using Dinero.Models.Database;
using System.Threading.Tasks;

namespace Dinero.Services.Core
{
    // Interface.
    public interface IPersistor<T>
    {
        Task Add(T obj);
        Task Save();
    }

    /// <summary>
    /// Persists instances into the DineroContext database.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Persistor<T>: ContextAdapter, IPersistor<T>
    {
        // Constuctor.
        public Persistor(DineroContext database) : base(database) { }

        // Abstract Methods.
        abstract public Task Add(T obj);

        // Public Methods.
        public async Task Save()
        {
            await database.SaveChangesAsync();
        }
    }
}
