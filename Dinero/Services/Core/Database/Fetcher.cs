﻿using Dinero.Models.Database;

namespace Dinero.Services.Core
{
    public abstract class Fetcher : ContextAdapter
    {
        public Fetcher(DineroContext database) : base(database) { }
    }
}
