﻿using Dinero.Models.Database;

namespace Dinero.Services.Core
{
    public abstract class ContextAdapter
    {
        // Instance Variables
        protected DineroContext database;

        // Constructor.
        public ContextAdapter(DineroContext database)
        {
            this.database = database;
        }
    }
}
