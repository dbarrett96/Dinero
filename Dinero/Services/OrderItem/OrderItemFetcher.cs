﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.OrderItem
{
    // Interface.
    public interface IOrderItemFetcher
    {
        Models.Database.OrderItem byId(long orderItemId);
        ICollection<Models.Database.OrderItem> byTableGuest(Models.Database.TableGuest tableGuest);
        ICollection<Models.Database.OrderItem> byTableGuest(Models.Database.TableGuest tableGuest, Models.Database.OrderItem.Status status);
        ICollection<Models.Database.OrderItem> whereTableGuestIn(IEnumerable<Models.Database.TableGuest> tableGuestList);
    }

    // Implementation.
    public class OrderItemFetcher : Fetcher, IOrderItemFetcher
    {
        // Constructor.
        public OrderItemFetcher(DineroContext database) : base(database) { }



        // IOrderItemFetcher
        public Models.Database.OrderItem byId(long orderItemId)
        {
            try
            {
                return database.OrderItem.Single(orderItem => orderItem.Id == orderItemId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such order item.", e);
            }
        }

        public ICollection<Models.Database.OrderItem> byTableGuest(Models.Database.TableGuest tableGuest)
        {
            return database.OrderItem.Where(orderItem => orderItem.TableGuestId == tableGuest.Id).ToList();
        }

        public ICollection<Models.Database.OrderItem> byTableGuest(Models.Database.TableGuest tableGuest, Models.Database.OrderItem.Status status)
        {
            return database.OrderItem.Where(orderItem => orderItem.TableGuestId == tableGuest.Id && orderItem.status == status).ToList();
        }

        public ICollection<Models.Database.OrderItem> whereTableGuestIn(IEnumerable<Models.Database.TableGuest> tableGuestList)
        {
            var tableGuestIdList = tableGuestList.Select(guest => guest.Id);
            return database.OrderItem.Where(orderItem => tableGuestIdList.Contains(orderItem.TableGuestId) && orderItem.status >= Models.Database.OrderItem.Status.WAITING_FOR_GROUP).ToList();
        }
    }
}
