﻿using Dinero.Models.Database;
using Dinero.Services.Core;

namespace Dinero.Services.OrderItem
{
    // Interface.
    public interface IOrderItemMutator: IMutator<Models.Database.OrderItem> { }

    // Implementation.
    public class OrderItemMutator : Mutator<Models.Database.OrderItem>, IOrderItemMutator
    {
        // Constructor.
        public OrderItemMutator(DineroContext database) : base(database) { }

        // IOrderItemMutator
        public override void delete(Models.Database.OrderItem obj)
        {
            database.OrderItem.Remove(obj);
        }
    }
}
