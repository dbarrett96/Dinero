﻿using Dinero.Services.Core;

namespace Dinero.Services.OrderItem
{
    // Interface.
    public interface IOrderItemService
    {
        IIdCoder id { get; set; }
        IOrderItemFetcher find { get; set; }
        IOrderItemPersistor persist { get; set; }
        IOrderItemMutator mutate { get; set; }
    }

    // Implementation.
    public class OrderItemService: IOrderItemService
    {
        // Instance Variables.
        public IIdCoder id { get; set; }
        public IOrderItemFetcher find { get; set; }
        public IOrderItemPersistor persist { get; set; }
        public IOrderItemMutator mutate { get; set; }

        // Constructor.
        public OrderItemService(IIdCoder id, IOrderItemFetcher find, IOrderItemPersistor persist, IOrderItemMutator mutate )
        {
            this.id = id;
            this.find = find;
            this.persist = persist;
            this.mutate = mutate;
        }
    }
}
