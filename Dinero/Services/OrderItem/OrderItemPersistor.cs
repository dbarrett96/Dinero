﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dinero.Services.OrderItem
{
    // Interface.
    public interface IOrderItemPersistor : IPersistor<Models.Database.OrderItem>
    {
        Task Add(IEnumerable<Models.Database.OrderItem> objList);
    }

    // Implementation.
    public class OrderItemPersistor : Persistor<Models.Database.OrderItem>, IOrderItemPersistor
    {
        // Constructor.
        public OrderItemPersistor(DineroContext database) : base(database) { }

        public override async Task Add(Models.Database.OrderItem obj)
        {
            database.OrderItem.Add(obj);
            await Save();
        }

        public async Task Add(IEnumerable<Models.Database.OrderItem> objList)
        {
            foreach (var obj in objList)
            {
                await Add(obj);
            }
        }
    }
}
