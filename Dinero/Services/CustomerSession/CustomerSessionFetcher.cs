﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Linq;

namespace Dinero.Services.CustomerSession
{
    // Interface.
    public interface ICustomerSessionFetcher
    {
        Models.Database.CustomerSession byKey(Models.Database.User user, Models.Database.TableSession tableSession);
    }

    // Implementation.
    public class CustomerSessionFetcher : Fetcher, ICustomerSessionFetcher
    {
        // Constructor.
        public CustomerSessionFetcher(DineroContext database) : base(database) { }

        // Methods
        public Models.Database.CustomerSession byKey(Models.Database.User user, Models.Database.TableSession tableSession)
        {
            try
            {
                return database.CustomerSession.Single(session => session.UserId == user.Id && session.TableSessionId == tableSession.Id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such customer session.", e);
            }
        }
    }
}
