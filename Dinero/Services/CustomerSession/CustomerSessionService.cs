﻿using Dinero.Models.Database;
using Dinero.Services.Core;

namespace Dinero.Services.CustomerSession
{
    // Interface.
    public interface ICustomerSessionService
    {
        ICustomerSessionPersistor persist { get; set; }

        /// <summary>
        /// Test if the given user has a CustomerSession in the given TableSession.
        /// </summary>
        /// <param name="tableSession">The table session.</param>
        /// <param name="user">The user.</param>
        /// <returns>True f the given user has a CustomerSession in the given TableSession.</returns>
        bool userHasSessionAt(Models.Database.TableSession tableSession, Models.Database.User user);
    }

    // Implementation.
    public class CustomerSessionService: ICustomerSessionService
    {
        // Instance Variables.
        public ICustomerSessionPersistor persist { get; set; }
        public ICustomerSessionFetcher find { get; set; }

        // Constructor.
        public CustomerSessionService(ICustomerSessionPersistor persist, ICustomerSessionFetcher find)
        {
            this.persist = persist;
            this.find = find;
        }

        // Methods.
        public bool userHasSessionAt(Models.Database.TableSession tableSession, Models.Database.User user)
        {
            try
            {
                return find.byKey(user, tableSession) != null;
            }
            catch (DnException e)
            {
                return false;
            }
            
            throw new System.NotImplementedException();
        }
    }
}
