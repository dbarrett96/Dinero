﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dinero.Services.CustomerSession
{
    // Interface.
    public interface ICustomerSessionPersistor: IPersistor<Models.Database.CustomerSession>
    {
         Task Add(IEnumerable<Models.Database.CustomerSession> objList);
    }

    public class CustomerSessionPersistor : Persistor<Models.Database.CustomerSession>, ICustomerSessionPersistor
    {
        // Constructor.
        public CustomerSessionPersistor(DineroContext database) : base(database) { }

        // Methods.
        public async override Task Add(Models.Database.CustomerSession obj)
        {
            database.CustomerSession.Add(obj);
            await Save();
        }

        public async Task Add(IEnumerable<Models.Database.CustomerSession> objList)
        {
            foreach (var obj in objList)
            {
                await Add(obj);
            }
        }
    }
}
