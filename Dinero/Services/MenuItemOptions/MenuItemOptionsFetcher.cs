﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.MenuItemOptions
{
    // Interface.
    public interface IMenuItemOptionsFetcher
    {
        MenuItemOption byId(long menuItemOptionId);
        ICollection<MenuItemOption> byMenuItem(Models.Database.MenuItem menuItem);
    }
    // Implementation.
    public class MenuItemOptionsFetcher : Fetcher, IMenuItemOptionsFetcher
    {
        // Constructor.
        public MenuItemOptionsFetcher(DineroContext database) : base(database) { }

        // IMenuItemOptionsFetcher.
        public MenuItemOption byId(long menuItemOptionId)
        {
            try
            {
                return database.MenuItemOption.Single(option => option.Id == menuItemOptionId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such option.", e);
            }
        }

        public ICollection<MenuItemOption> byMenuItem(Models.Database.MenuItem menuItem)
        {
            return database.MenuItemOption.Where(option => option.MenuItemId == menuItem.Id).ToList();
        }
    }
}
