﻿using Dinero.Services.Core;

namespace Dinero.Services.MenuItemOptions
{
    // Interface.
    public interface IMenuItemOptionsService
    {
        IIdCoder id { get; set; }
        IMenuItemOptionsFetcher find { get; set; }
    }

    // Implementation.
    public class MenuItemOptionsService : IMenuItemOptionsService
    {
        // IMenuItemOptionsService.
        public IIdCoder id { get; set; }
        public IMenuItemOptionsFetcher find { get; set; }

        // Constructor.
        public MenuItemOptionsService(IIdCoder id, IMenuItemOptionsFetcher find)
        {
            this.id = id;
            this.find = find;
        }
    }
}
