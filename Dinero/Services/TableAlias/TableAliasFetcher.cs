﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Linq;

namespace Dinero.Services.TableAlias
{
    // Interface.
    public interface ITableAliasFetcher
    {
        Models.Database.TableAlias byTable(Models.Database.Table table);
        Models.Database.TableAlias byTableId(long tableId);
    }

    // Implementation.
    public class TableAliasFetcher : Fetcher, ITableAliasFetcher
    {
        // Constructor.
        public TableAliasFetcher(DineroContext database) : base(database) { }

        // ITableAliasFetcher
        public Models.Database.TableAlias byTableId(long tableId)
        {
            try
            {
                return database.TableAlias.Single(alias => alias.TableId == tableId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("Could not find alias for table.", e);
            }
        }

        public Models.Database.TableAlias byTable(Models.Database.Table table)
        {
            return byTableId(table.Id);
        }
    }
}
