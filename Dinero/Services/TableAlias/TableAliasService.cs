﻿namespace Dinero.Services.TableAlias
{
    // Interface.
    public interface ITableAliasService
    {
        ITableAliasFetcher find { get; set; }
    }

    // Implementation.
    public class TableAliasService : ITableAliasService
    {
        public ITableAliasFetcher find { get; set; }
            
        // Constructor.
        public TableAliasService(ITableAliasFetcher find)
        {
            this.find = find;
        }
    }
}
