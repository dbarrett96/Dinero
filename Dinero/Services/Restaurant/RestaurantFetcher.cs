﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Linq;

namespace Dinero.Services.Restaurant
{
    // Interface.
    public interface IRestaurantFetcher
    {
        Models.Database.Restaurant byId(long restaurantId);
    }

    // Impelmentation.
    public class RestaurantFetcher : Fetcher, IRestaurantFetcher
    {
        // Constructor.
        public RestaurantFetcher(DineroContext database) : base(database) { }

        // IRestaurantFetcher
        public Models.Database.Restaurant byId(long restaurantId)
        {
            try
            {
                return database.Restaurant.Single(resto => resto.Id == restaurantId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such restaurant.", e);
            }
        }
    }
}
