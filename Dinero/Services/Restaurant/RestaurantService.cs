﻿using Dinero.Services.Core;

namespace Dinero.Services.Restaurant
{
    // Interface.
    public interface IRestaurantService
    {
        IIdCoder id { get; set; }
        IRestaurantFetcher find { get; set; }
    }

    // Implmentation.
    public class RestaurantService : IRestaurantService
    {
        // Instance Variables.
        public IIdCoder id { get; set; }
        public IRestaurantFetcher find { get; set; }

        // Constructor.
        public RestaurantService(IIdCoder id, IRestaurantFetcher find)
        {
            this.id = id;
            this.find = find;
        }
    }
}
