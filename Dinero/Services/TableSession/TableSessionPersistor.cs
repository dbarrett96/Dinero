﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.TableSession
{
    // Interface.
    public interface ITableSessionPersistor : IPersistor<Models.Database.TableSession> { }

    // Implementation.
    public class TableSessionPersistor : Persistor<Models.Database.TableSession>, ITableSessionPersistor
    {
        // Constructor.
        public TableSessionPersistor(DineroContext database) : base(database) { }

        // ITableSessionPersistor.
        public async override Task Add(Models.Database.TableSession obj)
        {
            database.TableSession.Add(obj);
            await Save();
        }
    }
}
