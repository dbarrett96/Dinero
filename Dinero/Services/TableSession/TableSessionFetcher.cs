﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Linq;

namespace Dinero.Services.TableSession
{
    // Interface.
    public interface ITableSessionFetcher
    {
        Models.Database.TableSession byId(long tableSessionId);
        Models.Database.TableSession byUser(Models.Database.User user);
        Models.Database.TableSession activeAtTableId(long tableId);
    }

    // Implementation.
    public class TableSessionFetcher : Fetcher, ITableSessionFetcher
    {
        // Constructor.
        public TableSessionFetcher(DineroContext database) : base(database)
        {
        }

        /// <summary>
        /// Returns the active session at the given table.
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        public Models.Database.TableSession activeAtTableId(long tableId)
        {
            try
            {
                return database.TableSession.Single(session => session.TableId == tableId && session.EndedAt == null);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No current session found at the table.");
            }
        }

        // ITableSessionFetcher
        public Models.Database.TableSession byId(long tableSessionId)
        {
            try
            {
                return database.TableSession.Single(session => session.Id == tableSessionId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such session.", e);
            }
        }

        public Models.Database.TableSession byUser(Models.Database.User user)
        {
            try
            {
                var sessionId = database.TableGuest.Single(guest => guest.UserId == user.Id).TableSessionId;
                return database.TableSession.Single(session => session.Id == sessionId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("User is not part of a table.", e);
            }
        }
    }
}
