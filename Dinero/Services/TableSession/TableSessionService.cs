﻿using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using System;
using System.Threading.Tasks;

namespace Dinero.Services.TableSession
{
    // Interface.
    public interface ITableSessionService
    {
        IIdCoder id { get; set; }
        ITableSessionFetcher find { get; set; }
        ITableSessionPersistor persist { get; set; }

        /// <summary>
        /// Add a user as a guest at a table.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="table">The table.</param>
        Task add(Models.Database.User user, Models.Database.TableSession tableSession, bool isGroupLeader);

        /// <summary>
        /// Remove a user from any tables they are currently at.
        /// </summary>
        /// <param name="user">The user.</param>
        Task remove(Models.Database.User user);

        /// <summary>
        /// Change the leader of a group of TableGuests.
        /// </summary>
        /// <param name="from">The old leader of the group.</param>
        /// <param name="to">The new leader of the group.</param>
        Task changeLeader(Models.Database.TableGuest from, Models.Database.TableGuest to);

        /// <summary>
        /// End the given table session.
        /// </summary>
        /// <param name="session">The session.</param>
        Task endSession(Models.Database.TableSession session);
    }

    // Implementation.
    public class TableSessionService: ITableSessionService
    {
        // Instance Variables.
        public IIdCoder id { get; set; }
        public ITableSessionFetcher find { get; set; }
        public ITableSessionPersistor persist { get; set; }
        private ITableGuestFetcher findTableGuest { get; set; }
        private ITableGuestPersistor persistTableGuest { get; set; }
        private ITableGuestMutator mutateTableGuest { get; set; }

        // Constructor.
        public TableSessionService(
            IIdCoder id, 
            ITableSessionFetcher find, 
            ITableSessionPersistor persist, 
            ITableGuestFetcher findTableGuest, 
            ITableGuestPersistor persistTableGuest,
            ITableGuestMutator mutateTableGuest)
        {
            this.id = id;
            this.find = find;
            this.persist = persist;
            this.findTableGuest = findTableGuest;
            this.persistTableGuest = persistTableGuest;
            this.mutateTableGuest = mutateTableGuest;
        }

        // ITableSessionService.
        public async Task add(Models.Database.User user, Models.Database.TableSession tableSession, bool isGroupLeader)
        {
            var tableGuest = new Models.Database.TableGuest(user.Id, tableSession.Id, isGroupLeader);
            await persistTableGuest.Add(tableGuest);
        }

        public async Task changeLeader(Models.Database.TableGuest from, Models.Database.TableGuest to)
        {
            from.IsGroupLeader = false;
            to.IsGroupLeader = true;
            await persistTableGuest.Save();
        }

        public async Task remove(Models.Database.User user)
        {
            var userAsTableGuest = findTableGuest.byUserId(user.Id);
            mutateTableGuest.delete(userAsTableGuest);
            await persistTableGuest.Save();
        }

        public async Task endSession(Models.Database.TableSession session)
        {
            session.EndedAt = DateTime.Now;
            await persist.Save();
        }
    }
}
