﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.MenuItem
{
    // Interface.
    public interface IMenuItemFetcher
    {
        ICollection<Models.Database.MenuItem> manyByRestaurantId(long restaurantId);
        ICollection<Models.Database.MenuItem> manyById(IEnumerable<long> menuItemIds);
        Models.Database.MenuItem byId(long id);
    }

    // Implementation.
    public class MenuItemFetcher : Fetcher, IMenuItemFetcher
    {
        // Constructor.
        public MenuItemFetcher(DineroContext database) : base(database) { }

        // IMenuItemFetcher.
        public ICollection<Models.Database.MenuItem> manyByRestaurantId(long restaurantId)
        {
            return database.MenuItem.Where(item => item.RestaurantId == restaurantId).ToList();
        }

        public Models.Database.MenuItem byId(long id)
        {
            try
            {
                return database.MenuItem.Single(item => item.Id == id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such menu item.", e);
            }
        }

        public ICollection<Models.Database.MenuItem> manyById(IEnumerable<long> menuItemIds)
        {
            return database.MenuItem.Where(item => menuItemIds.Contains(item.Id)).ToList();
        }
    }
}
