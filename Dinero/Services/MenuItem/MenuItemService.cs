﻿using Dinero.Services.Core;

namespace Dinero.Services.MenuItem
{
    // Interface.
    public interface IMenuItemService
    {
        IIdCoder id { get; set; }
        IMenuItemFetcher find { get; set; }
    }

    // Implementation.
    public class MenuItemService : IMenuItemService
    {
        // IMenuItemService
        public IIdCoder id { get; set; }
        public IMenuItemFetcher find { get; set; }

        // Constructor.
        public MenuItemService(IIdCoder id, IMenuItemFetcher find)
        {
            this.id = id;
            this.find = find;
        }
    }
}
