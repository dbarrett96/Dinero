﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using Dinero.Services.TableGuest;
using Dinero.Services.TableSession;
using Dinero.Services.User;
using System.Threading.Tasks;

namespace Dinero.Services.TableGuestRequest
{
    // Interface.
    public interface ITableGuestRequestService
    {
        ITableGuestRequestFetcher find { get; set; }
        ITableGuestRequestPersistor persist { get; set; }
        IIdCoder id { get; set; }

        Task cancel(Models.Database.TableGuestRequest request);
        Task accept(Models.Database.TableGuestRequest request);
        Task decline(Models.Database.TableGuestRequest request);
        Task<Models.Database.TableGuestRequest> join(Models.Database.User user, Models.Database.Table table);
    }

    // Implementation.
    public class TableGuestRequestService : ITableGuestRequestService
    {
        // Instance variables.
        private ITableGuestService tableGuestService;
        private IUserService userService;
        private ITableSessionService tableSessionService;

        // Constructor.
        public TableGuestRequestService(
            ITableGuestService tableGuestService,
            ITableGuestRequestFetcher find,
            ITableGuestRequestPersistor persist, 
            IIdCoder id,
            IUserService userService,
            ITableSessionService tableSessionService)
        {
            this.tableGuestService = tableGuestService;
            this.find = find;
            this.persist = persist;
            this.id = id;
            this.userService = userService;
            this.tableSessionService = tableSessionService;
        }

        // ITableGuestRequestService
        public ITableGuestRequestFetcher find { get; set; }
        public ITableGuestRequestPersistor persist { get; set; }
        public IIdCoder id { get; set; }

        /// <summary>
        /// Accept a request to sit at a table. Adds the originator of the accepted request as a TableGuest at the requested table.
        /// </summary>
        /// <param name="request"></param>
        public async Task accept(Models.Database.TableGuestRequest request)
        {
            if (request.requestState == Models.Database.TableGuestRequest.State.PENDING)
            {
                request.Status = Models.Database.TableGuestRequest.State.ACCEPTED.toByte();

                Models.Database.TableSession session;
                try
                {
                    // Check if there is an existing session for the table.
                    session = tableSessionService.find.activeAtTableId(request.TableId);
                }
                catch (DnException e)
                {
                    // Create session at table if no existing session.
                    session = new Models.Database.TableSession(request.TableId);
                    await tableSessionService.persist.Add(session);
                }
              
                // Get table group and user.
                var tableGroup = tableGuestService.find.manyByTableSession(session);
                var user = userService.find.byId(request.UserId);

                // Saves all db changes as well.
                await tableSessionService.add(user, session, tableGroup.Count == 0);
            }
            else
            {
                throw new DnException("Request must be in PENDING state to be accepted.", null);
            }
        }

        /// <summary>
        /// Decline a request to sit at a table. 
        /// </summary>
        /// <param name="request">The request.</param>
        public async Task decline(Models.Database.TableGuestRequest request)
        {
            request.Status = Models.Database.TableGuestRequest.State.DECLINED.toByte();
            await persist.Save();
        }

        /// <summary>
        /// Cancel a request to sit at a table.
        /// </summary>
        /// <param name="request">The request.</param>
        public async Task cancel(Models.Database.TableGuestRequest request)
        {
            request.Status = Models.Database.TableGuestRequest.State.CANCELED.toByte();
            await persist.Save();
        }

        /// <summary>
        /// Request for a user to join a table.
        /// </summary>
        /// <param name="user">The user requesting to join the table.</param>
        /// <param name="table">The table.</param>
        /// <returns>The request object created by the user.</returns>
        public async Task<Models.Database.TableGuestRequest> join(Models.Database.User user, Models.Database.Table table)
        {
            var request = new Models.Database.TableGuestRequest(user.Id, table.Id);
            await persist.Add(request);
            return request;
        }
    }
}
