﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.TableGuestRequest
{
    // Interface.
    public interface ITableGuestRequestFetcher
    {
        ICollection<Models.Database.TableGuestRequest> byUser(Models.Database.User user);
        ICollection<Models.Database.TableGuestRequest> byUser(Models.Database.User user, Models.Database.TableGuestRequest.State state);
        Models.Database.TableGuestRequest byId(long id);
    }

    // Implementation.
    public class TableGuestRequestFetcher : Fetcher, ITableGuestRequestFetcher
    {
        // Constructor.
        public TableGuestRequestFetcher(DineroContext database) : base(database) { }

        // ITableGuestRequestFetcher
        public ICollection<Models.Database.TableGuestRequest> byUser(Models.Database.User user)
        {
            try
            {
                return database.TableGuestRequest.Where(request => request.UserId == user.Id ).ToList();
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No request to join a table could be found for user.", e);
            }
        }

        public ICollection<Models.Database.TableGuestRequest> byUser(Models.Database.User user, Models.Database.TableGuestRequest.State state)
        {
            return byUser(user).Where(request => request.requestState == state).ToList();
        }

        public Models.Database.TableGuestRequest byId(long id)
        {
            try
            {
                return database.TableGuestRequest.Single(request => request.Id == id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No request matching the id could be found.", e);
            }
        }
    }
}
