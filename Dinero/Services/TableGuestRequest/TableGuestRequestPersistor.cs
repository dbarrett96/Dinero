﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.TableGuestRequest
{
    // Interface.
    public interface ITableGuestRequestPersistor: IPersistor<Models.Database.TableGuestRequest> {}

    // Implementation.
    public class TableGuestRequestPersistor : Persistor<Models.Database.TableGuestRequest>, ITableGuestRequestPersistor
    {
        // Constructor.
        public TableGuestRequestPersistor(DineroContext database) : base(database) { }

        // ITableGuestRequestPersistor.
        public override async Task Add(Models.Database.TableGuestRequest obj)
        {
            database.TableGuestRequest.Add(obj);
            await Save();
        }
    }
}
