﻿using Dinero.Services.Core;
using System;

namespace Dinero.Services.Table.Exceptions
{
    public class NotSeatedException: DnException
    {
        public NotSeatedException(Exception internalSource) : base("User is not currently seated at a table.", internalSource) { }
    }
}
