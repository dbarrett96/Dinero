﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using Dinero.Services.Table.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.Table
{
    // Interface.
    public interface ITableFetcher
    {
        Models.Database.Table byAlias(string tableAlias);
        Models.Database.Table byUser(Models.Database.User user);
        Models.Database.Table byId(long id);
    }

    // Implementation.
    public class TableFetcher: Fetcher, ITableFetcher 
    {
        // Constuctor.
        public TableFetcher(DineroContext database) : base(database) { }

        // ITableFetcher
        public Models.Database.Table byAlias(string tableAlias)
        {
            try
            {
                return (
                    from table in database.Table
                    join alias in database.TableAlias on table.Id equals alias.TableId
                    where alias.Alias == tableAlias
                    select table
                ).Single();
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No table matching the given id could be found.", e);
            }
        }

        /// <summary>
        /// Fetch a table using its unique id.
        /// </summary>
        /// <param name="id">The unique id of the table.</param>
        /// <returns>The table matching the given id.</returns>
        public Models.Database.Table byId(long id)
        {
            try
            {
                return database.Table.Single(table => table.Id == id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No table matching the given id could be found.", e);
            }
        }

        /// <summary>
        /// Fetch the table that the specified user is sitting at.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>The table that user is sitting at.</returns>
        public Models.Database.Table byUser(Models.Database.User user)
        {
            try
            {
                return (
                    from tableGuest in database.TableGuest
                    join tableSession in database.TableSession on tableGuest.TableSessionId equals tableSession.Id 
                    join table in database.Table on tableSession.TableId equals table.Id
                    where tableGuest.UserId == user.Id
                    select table
                ).Single();
            }
            catch (InvalidOperationException e)
            {
                throw new NotSeatedException(e);
            }
        }
    }
}
