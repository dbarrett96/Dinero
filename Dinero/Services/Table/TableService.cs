﻿using Dinero.Services.Core;

namespace Dinero.Services.Table
{
    // Interface.
    public interface ITableService
    {
        IIdCoder id { get; set; }
        ITableFetcher find { get; set; }
    }

    // Implementation.
    public class TableService : ITableService
    {
        // Instance Variables.
        public IIdCoder id { get; set; }
        public ITableFetcher find { get; set; }

        // Constructor.
        public TableService(IIdCoder id, ITableFetcher find)
        {
            this.id = id;
            this.find = find;
        }
    }
}
