﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Collections.Generic;

namespace Dinero.Services.TableGuest
{
    // Interface.
    public interface ITableGuestMutator : IMutator<Models.Database.TableGuest>
    {
        void delete(IEnumerable<Models.Database.TableGuest> objList);
    }

    // Implementation.
    public class TableGuestMutator : Mutator<Models.Database.TableGuest>, ITableGuestMutator
    {
        // Constructor.
        public TableGuestMutator(DineroContext database) : base(database) { }

        public override void delete(Models.Database.TableGuest obj)
        {
            database.TableGuest.Remove(obj);
        }

        public void delete(IEnumerable<Models.Database.TableGuest> objList)
        {
            foreach (var obj in objList)
            {
                delete(obj);
            }
        }
    }
}
