﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dinero.Models.Database;
using Dinero.Services.Core;

namespace Dinero.Services.TableGuest
{
    // Interface.
    public interface ITableGuestService
    {
        ITableGuestFetcher find { get; set; }
        ITableGuestMutator mutate { get; set; }
        ITableGuestPersistor persist { get; set; }

        bool userIsTableGuestAt(Models.Database.TableSession tableSession, Models.Database.User user);
        Task removeGuests(IEnumerable<Models.Database.TableGuest> tableGuests);
    }

    // Implementation.
    public class TableGuestService: ITableGuestService
    {
        // Instance Variables
        public ITableGuestFetcher find { get; set; }
        public ITableGuestMutator mutate { get; set; }
        public ITableGuestPersistor persist { get; set; }

        // Constructor
        public TableGuestService(ITableGuestFetcher find, ITableGuestMutator mutate, ITableGuestPersistor persist)
        {
            this.find = find;
            this.mutate = mutate;
            this.persist = persist;
        }

        // Methods.
        public bool userIsTableGuestAt(Models.Database.TableSession tableSession, Models.Database.User user)
        {
            try
            {
                var userAsTableGuest = find.byUser(user);
                return userAsTableGuest != null && userAsTableGuest.TableSessionId == tableSession.Id;
            }
            catch (DnException e)
            {
                return false;
            }
        }

        public async Task removeGuests(IEnumerable<Models.Database.TableGuest> tableGuests)
        {
            mutate.delete(tableGuests);
            await persist.Save();
        }
    }
}
