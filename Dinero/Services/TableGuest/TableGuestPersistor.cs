﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.TableGuest
{
    // Interface.
    public interface ITableGuestPersistor : IPersistor<Models.Database.TableGuest> { }

    // Implementation.
    public class TableGuestPersistor : Persistor<Models.Database.TableGuest>, ITableGuestPersistor
    {
        // Constructor.
        public TableGuestPersistor(DineroContext database) : base(database) { }

        // Methods.
        public override async Task Add(Models.Database.TableGuest obj)
        {
            database.TableGuest.Add(obj);
            await Save();
        }
    }
}
