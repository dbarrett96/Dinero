﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.TableGuest
{
    // Interface.
    public interface ITableGuestFetcher
    {
        ICollection<Models.Database.TableGuest> whereUserIdIn(IEnumerable<long> tableGuestUserIdList);
        ICollection<Models.Database.TableGuest> whereIdIn(IEnumerable<long> tableGuestIdList);
        ICollection<Models.Database.TableGuest> manyByTableSession(Models.Database.TableSession tableSession);
        ICollection<Models.Database.TableGuest> manyByTableSessionId(long tableSessionId);
        Models.Database.TableGuest byUser(Models.Database.User user);
        Models.Database.TableGuest byUserId(long id);
        Models.Database.TableGuest byId(long id);
    }

    // Implementation.
    public class TableGuestFetcher : Fetcher, ITableGuestFetcher
    {
        // Constructor.
        public TableGuestFetcher(DineroContext database) : base(database) { }

        // Methods.
        public ICollection<Models.Database.TableGuest> whereUserIdIn(IEnumerable<long> tableGuestUserIdList)
        {
            return database.TableGuest.Where(guest => tableGuestUserIdList.Contains(guest.UserId)).ToList();
        }

        public ICollection<Models.Database.TableGuest> whereIdIn(IEnumerable<long> tableGuestIdList)
        {
            return database.TableGuest.Where(guest => tableGuestIdList.Contains(guest.Id)).ToList();
        }

        public ICollection<Models.Database.TableGuest> manyByTableSessionId(long tableSessionId)
        {
            try
            {
                return (
                    from tableGuest in database.TableGuest
                    where tableGuest.TableSessionId == tableSessionId
                    select tableGuest
                ).ToList();
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No table matching the given id could be found.", e);
            }
        }

        public ICollection<Models.Database.TableGuest> manyByTableSession(Models.Database.TableSession tableSession)
        {
            return manyByTableSessionId(tableSession.Id);
        }

        public Models.Database.TableGuest byUserId(long id)
        {
            try
            {
                return database.TableGuest.Single(guest => guest.UserId == id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("User is not a guest at any tables.", e);
            }
            
        }

        public Models.Database.TableGuest byUser(Models.Database.User user)
        {
            return byUserId(user.Id);
        }

        

        public Models.Database.TableGuest byId(long id)
        {
            try
            {
                return database.TableGuest.Single(tableGuest => tableGuest.Id == id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such table guest.", e);
            }
        }
    }
}
