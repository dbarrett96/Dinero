﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.OrderItemOptions
{
    // Interface.
    public interface IOrderItemOptionsFetcher
    {
        OrderItemOption byId(long orderItemOptionId);

        ICollection<Models.Database.OrderItemOption> manyByTableGuest(Models.Database.TableGuest tableGuest);
        ICollection<OrderItemOption> manyByOrderItem(Models.Database.OrderItem orderItem);
    }

    // Implementation.
    public class OrderItemOptionsFetcher : Fetcher, IOrderItemOptionsFetcher
    {
        // Constructor.
        public OrderItemOptionsFetcher(DineroContext database) : base(database) { }

        // IOrderItemOptionsFetcher.
        public OrderItemOption byId(long orderItemOptionId)
        {
            try
            {
                return database.OrderItemOption.Single(option => option.Id == orderItemOptionId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such order item option.", e);
            }
        }

        public ICollection<OrderItemOption> manyByTableGuest(Models.Database.TableGuest tableGuest)
        {
            return (from orderItemOption in database.OrderItemOption
                   join orderItem in database.OrderItem on orderItemOption.OrderItemId equals orderItem.Id
                   where orderItem.TableGuestId == tableGuest.Id
                   select orderItemOption).ToList();
        }

        public ICollection<OrderItemOption> manyByOrderItem(Models.Database.OrderItem orderItem)
        {
            return (from orderItemOption in database.OrderItemOption
                    where orderItemOption.OrderItemId == orderItem.Id
                    select orderItemOption).ToList();
        }
    }
}
