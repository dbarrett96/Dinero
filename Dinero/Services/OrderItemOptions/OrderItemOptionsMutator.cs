﻿using Dinero.Models.Database;
using Dinero.Services.Core;

namespace Dinero.Services.OrderItemOptions
{
    // Interface.
    public interface IOrderItemOptionsMutator : IMutator<Models.Database.OrderItemOption> { }

    // Implementation.
    public class OrderItemOptionsMutator : Mutator<Models.Database.OrderItemOption>, IOrderItemOptionsMutator
    {
        // Constructor.
        public OrderItemOptionsMutator(DineroContext database) : base(database) { }

        // IOrderItemOptionsMutator.
        public override void delete(OrderItemOption obj)
        {
            database.OrderItemOption.Remove(obj);
        }
    }
}
