﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Threading.Tasks;

namespace Dinero.Services.OrderItemOptions
{
    // Interface.
    public interface IOrderItemOptionsPersistor : IPersistor<OrderItemOption> { }

    // Implementation.
    public class OrderItemOptionsPersistor : Persistor<OrderItemOption>, IOrderItemOptionsPersistor
    {
        // Constructor.
        public OrderItemOptionsPersistor(DineroContext database) : base(database) { }

        // IOrderItemOptionsPersistor
        public override Task Add(OrderItemOption obj)
        {
            throw new NotImplementedException();
        }
    }
}
