﻿using Dinero.Services.Core;

namespace Dinero.Services.OrderItemOptions
{
    // Interface.
    public interface IOrderItemOptionsService
    {
        IIdCoder id { get; set; }
        IOrderItemOptionsFetcher find { get; set; }
        IOrderItemOptionsMutator mutate { get; set; }
        IOrderItemOptionsPersistor persist { get; set; }
    }

    // Implementation.
    public class OrderItemOptionsService: IOrderItemOptionsService
    {
        // Instance Variables.
        public IIdCoder id { get; set; }
        public IOrderItemOptionsFetcher find { get; set; }
        public IOrderItemOptionsMutator mutate { get; set; }
        public IOrderItemOptionsPersistor persist { get; set; }

        // Constructor.
        public OrderItemOptionsService(IIdCoder id, IOrderItemOptionsFetcher find, IOrderItemOptionsMutator mutate, IOrderItemOptionsPersistor persist)
        {
            this.id = id;
            this.find = find;
            this.mutate = mutate;
            this.persist = persist;
        }
    }
}
