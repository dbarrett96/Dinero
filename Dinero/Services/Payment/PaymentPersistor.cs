﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.Payment
{
    // Interface.
    public interface IPaymentPersistor : IPersistor<Models.Database.Payment> { }

    // Implementation.
    public class PaymentPersistor : Persistor<Models.Database.Payment>, IPaymentPersistor
    {
        // Constructor.
        public PaymentPersistor(DineroContext database) : base(database)
        {
        }

        // Methods.
        public override async Task Add(Models.Database.Payment obj)
        {
            database.Payment.Add(obj);
            await Save();
        }
    }
}
