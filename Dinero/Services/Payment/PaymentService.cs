﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using Dinero.Util;
using System.Threading.Tasks;

namespace Dinero.Services.Payment
{
    // Interface.
    public interface IPaymentService
    {
        IIdCoder id { get; set; }
        IPaymentFetcher find { get; set; }

        Task<Models.Database.Payment> createPayment(Models.Database.User payer, Models.Database.TableSession session, long amount, Util.Currency currency);
        Task cancelPayment(Models.Database.Payment payment);
        Task confirmPayment(Models.Database.Payment payment, string confirmationId);

        bool userHasPendingPayment(Models.Database.User user);
    }

    // Implementation.
    public class PaymentService : IPaymentService
    {
        // Instance Variables.
        public IIdCoder id { get; set; }
        public IPaymentFetcher find { get; set; }

        private IPaymentPersistor persist;
        private IPaymentProvider paymentProvider;

        // Constructor.
        public PaymentService(IIdCoder id, IPaymentPersistor persist, IPaymentProvider paymentProvider, IPaymentFetcher find)
        {
            this.id = id;
            this.persist = persist;
            this.paymentProvider = paymentProvider;
            this.find = find;
        }
        
        public async Task<Models.Database.Payment> createPayment(Models.Database.User payer, Models.Database.TableSession session, long amount, Currency currency)
        {
            // Request payment nonce from provider.
            var paymentNonce = await paymentProvider.requestPaymentNonce(amount, currency);

            // Create new payment object.
            var payment = new Models.Database.Payment(payer.Id, session.Id, paymentNonce, paymentProvider.id, amount, currency);

            // Save it.
            await persist.Add(payment);

            return payment;
        }

        public bool userHasPendingPayment(Models.Database.User user)
        {
            try
            {
                return find.pendingPaymentFor(user) != null;
            }
            catch (DnException e)
            {
                return false;
            }
        }

        public async Task cancelPayment(Models.Database.Payment payment)
        {
            await paymentProvider.requestNonceInvalidation(payment.PaymentId);

            payment.setState(Models.Database.Payment.State.CANCELED);
            await persist.Save();
        }

        public async Task confirmPayment(Models.Database.Payment payment, string confirmationId)
        {
            var paymentIsConfirmed = await paymentProvider.verifyNonceConfirmationId(payment.PaymentId, confirmationId);

            if (!paymentIsConfirmed)
            {
                throw new DnException("Invalid confirmation number.");
            }

            payment.PaymentConfirmation = confirmationId;
            payment.setState(Models.Database.Payment.State.CONFIRMED);
            await persist.Save();
        }
    }
}
