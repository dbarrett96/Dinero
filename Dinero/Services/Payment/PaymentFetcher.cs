﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Linq;

namespace Dinero.Services.Payment
{
    // Interface.
    public interface IPaymentFetcher
    {
        Models.Database.Payment byId(long paymentId);
        Models.Database.Payment pendingPaymentFor(Models.Database.User user);
    }

    // Implementation.
    public class PaymentFetcher : Fetcher, IPaymentFetcher
    {
        // Constructor.
        public PaymentFetcher(DineroContext database) : base(database) { }

        // Methods.
        public Models.Database.Payment byId(long paymentId)
        {
            try
            {
                return database.Payment.Single(payment => payment.Id == paymentId);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("No such payment.", e);
            }
        }

        public Models.Database.Payment pendingPaymentFor(Models.Database.User user)
        {
            try
            {
                return database.Payment.Single(payment => payment.UserId == user.Id && payment.state == Models.Database.Payment.State.PENDING);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("Couldnt find a single pending payment for the given user.", e);
            }
        }
    }
}
