﻿using Dinero.Util;
using System.Threading.Tasks;

namespace Dinero.Services.Payment
{
    // Interface.
    public interface IPaymentProvider
    {
        PaymentProvider id { get; }

        /// <summary>
        /// Returns a nonce for the client to use when making a payment.
        /// </summary>
        /// <param name="amount">The amount of the payment.</param>
        /// <param name="currency">The currency of the payment.</param>
        /// <returns>The payment nonce.</returns>
        Task<string> requestPaymentNonce(long amount, Currency currency);

        /// <summary>
        /// Requests that the payment provider no longer accept a payment for the specified nonce.
        /// </summary>
        /// <param name="nonce">The unique payment nonce.</param>
        Task requestNonceInvalidation(string nonce);

        /// <summary>
        /// Checks that the given confirmation number is valid for the given nonce.
        /// </summary>
        /// <param name="nonce">The payment nonce.</param>
        /// <param name="confirmationId">The payment confirmation number.</param>
        /// <returns>True if the confirmationId was valid, otherwise false.</returns>
        Task<bool> verifyNonceConfirmationId(string nonce, string confirmationId);
    }

    // Implementation.
    public class TestPaymentProvider : IPaymentProvider
    {
        // Properties.
        public PaymentProvider id {
            get { return PaymentProvider.TEST_PROVIDER; }
        }

        // Methods.
        public async Task<string> requestPaymentNonce(long amount, Currency currency)
        {
            await Task.CompletedTask;
            return KeyGenerator.AlphaNumeric(64);
        }

        public async Task requestNonceInvalidation(string nonce)
        {
            await Task.CompletedTask;
            return;
        }

        public async Task<bool> verifyNonceConfirmationId(string nonce, string confirmationId)
        {
            await Task.CompletedTask;
            return true;
        }
    }
}
