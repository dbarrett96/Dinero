﻿using Dinero.Services.User;
using System.Threading.Tasks;

namespace Dinero.Services.SignUp
{
    // Interface.
    public interface ISignUpService
    {
        Task<Models.Database.User> byPhoneNumber(string phoneNumber, string firstName, string lastName);
    }

    // Implementation.
    public partial class SignUpService : ISignUpService
    {
        public async Task<Models.Database.User> byPhoneNumber(string phoneNumber, string firstName, string lastName)
        {
            var newUser = new Models.Database.User();

            newUser.FirstName = firstName;
            newUser.LastName = lastName;
            newUser.PhoneNumber = phoneNumber;

            await persistUser.Add(newUser);

            return newUser;
        }
    }

    public partial class SignUpService
    {
        private IUserFetcher findUser;
        private IUserPersistor persistUser;

        public SignUpService(IUserFetcher findUser, IUserPersistor persistUser)
        {
            this.findUser = findUser;
            this.persistUser = persistUser;
        }
    }
}
