﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using Dinero.Services.OrderItem;
using Dinero.Services.TableGuest;
using Dinero.Util.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dinero.Services.Order
{
    // Interface 
    public interface IOrderService
    {
        IOrderFetcher find { get; set; }

        Task addItems(ICollection<Models.Database.MenuItem> items, Models.Database.TableGuest orderer);
        Task<Models.Database.OrderItem.Status> placeOrderFor(Models.Database.TableGuest guest);
    }

    // Implementation.
    public class OrderService : IOrderService
    {
        // Instance Variables.
        public IOrderFetcher find { get; set; }
        private IOrderItemFetcher findOrderItem { get; set; }
        private IOrderItemPersistor persistOrderItem { get; set; }
        private ITableGuestFetcher findTableGuest { get; set; }
        
        // Constructor.
        public OrderService(IOrderItemPersistor persist, IOrderFetcher find, ITableGuestFetcher findTableGuest, IOrderItemFetcher findOrderItem)
        {
            this.persistOrderItem = persist;
            this.find = find;
            this.findTableGuest = findTableGuest;
            this.findOrderItem = findOrderItem;
        }

        // IOrderService.
        public async Task<Models.Database.OrderItem.Status> placeOrderFor(Models.Database.TableGuest guest)
        {
            // Get everyone sitting with guest.
            var tableGroup = findTableGuest.manyByTableSessionId(guest.TableSessionId);

            // If no one sitting with guest, not even guest...theres is a problem.
            if (tableGroup.Count == 0)
            {
                throw new DnException("Order cannot be placed from an empty table session. Something is wrong.", null);
            }

            // The items ordered by other people in guest's group.
            var groupOrder = new List<Models.Database.OrderItem>();
            // If true, the groupOrder is moved to the ORDERED state.
            var everyoneIsReady = true;

            // loop over group.
            foreach (var groupMember in tableGroup)
            {
                // Skip guest.
                if (groupMember.Id == guest.Id) continue;

                // Get the food they are waiting to order as a group.
                var groupMemberOrder = findOrderItem.byTableGuest(groupMember, Models.Database.OrderItem.Status.WAITING_FOR_GROUP);

                // If nothing, group is not ready to order.
                if (groupMemberOrder.Count == 0)
                {
                    everyoneIsReady = false;
                }

                // Concat group order.
                foreach (var groupMemberItem in groupMemberOrder)
                {
                    groupOrder.Add(groupMemberItem);
                }
            }

            // Move the guest order to waiting for group.
            var guestOrder = findOrderItem.byTableGuest(guest, Models.Database.OrderItem.Status.WAITING);
            guestOrder.setState(Models.Database.OrderItem.Status.WAITING_FOR_GROUP);

            // Make the guestorder part of the group order.
            foreach(var guestItem in guestOrder)
            {
                groupOrder.Add(guestItem);
            }

            // Order the group order if everyone is ready.
            if (everyoneIsReady)
            {
                groupOrder.setState(Models.Database.OrderItem.Status.ORDERED);
                await persistOrderItem.Save();
                return Models.Database.OrderItem.Status.ORDERED;
            }
            else
            {
                await persistOrderItem.Save();
                return Models.Database.OrderItem.Status.WAITING_FOR_GROUP;
            }
        }

        public async Task addItems(ICollection<Models.Database.MenuItem> items, Models.Database.TableGuest orderer)
        {
            var orderItems = items.Select(menuItem => new Models.Database.OrderItem(menuItem.Id, orderer.Id, menuItem.MenuItemOption.Select(option => new OrderItemOption(option)).ToList()));
            await persistOrderItem.Add(orderItems);
        }
    }
}
