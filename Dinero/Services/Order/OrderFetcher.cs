﻿using Dinero.Services.MenuItem;
using Dinero.Services.MenuItemOptions;
using Dinero.Services.OrderItem;
using Dinero.Services.OrderItemOptions;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.Order
{
    public interface IOrderFetcher
    {
        ICollection<Models.Database.OrderItem> byTableGuest(Models.Database.TableGuest tableGuest);
        ICollection<Models.Database.OrderItem> forTableGuests(IEnumerable<Models.Database.TableGuest> tableGuestList);
    }

    public class OrderFetcher : IOrderFetcher
    {
        // Instance Variables.
        private IOrderItemFetcher findOrderItem { get; set; }
        private IOrderItemOptionsFetcher findOrderItemOption { get; set; }
        private IMenuItemFetcher findMenuItem { get; set; }
        private IMenuItemOptionsFetcher findMenuItemOption { get; set; }

        // Constructor.
        public OrderFetcher(IOrderItemFetcher findOrderItem, IOrderItemOptionsFetcher findOrderItemOption, IMenuItemFetcher findMenuItem, IMenuItemOptionsFetcher findMenuItemOption)
        {
            this.findOrderItem = findOrderItem;
            this.findOrderItemOption = findOrderItemOption;
            this.findMenuItem = findMenuItem;
            this.findMenuItemOption = findMenuItemOption;
        }

        public ICollection<Models.Database.OrderItem> byTableGuest(Models.Database.TableGuest tableGuest)
        {
            // Get all order items for the table guest.
            var orderItems = findOrderItem.byTableGuest(tableGuest);
            var order = new List<Models.Database.OrderItem>();

            // Get all options tied to the order items.
            foreach( var orderItem in orderItems )
            {
                var orderItemOptions = findOrderItemOption.manyByOrderItem(orderItem);
                var menuItem = findMenuItem.byId(orderItem.MenuItemId);
                var menuItemOptions = orderItemOptions.Select(orderItemOption => findMenuItemOption.byId(orderItemOption.MenuItemOptionId));

                orderItem.OrderItemOption = orderItemOptions;
                orderItem.MenuItem = menuItem;
                orderItem.MenuItem.MenuItemOption = menuItemOptions.ToList();

                order.Add(orderItem);
            }

            return order;
        }

        public ICollection<Models.Database.OrderItem> forTableGuests(IEnumerable<Models.Database.TableGuest> tableGuestList)
        {
            var order = new List<Models.Database.OrderItem>();

            foreach (var guest in tableGuestList)
            {
                var guestOrder = byTableGuest(guest);

                foreach(var item in guestOrder)
                {
                    order.Add(item);
                }
            }

            return order;
        }
    }
}
