﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.BillGroup
{
    // Interface.
    public interface IBillGroupPersistor : IPersistor<Models.Database.BillGroup> { }

    // Implementation.
    public class BillGroupPersistor : Persistor<Models.Database.BillGroup>, IBillGroupPersistor
    {
        // Constructor.
        public BillGroupPersistor(DineroContext database) : base(database) { }

        // Methods
        public override async Task Add(Models.Database.BillGroup obj)
        {
            database.BillGroup.Add(obj);
            await Save();
        }
    }
}
