﻿using Dinero.Services.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dinero.Services.BillGroup
{
    // Interface.
    public interface IBillGroupService
    {
        IBillGroupFetcher find { get; set; }
        IBillGroupPersistor persist { get; set; }
        IBillGroupMutator mutate { get; set; }

        bool isGuestBeingPaidFor(Models.Database.TableGuest tableGuest);
        bool areAnyGuestsBeingPaidFor(IEnumerable<Models.Database.TableGuest> tableGuestList);

        Task create(Models.Database.TableGuest groupLeader, IEnumerable<Models.Database.TableGuest> groupMembers);
        Task deleteGroupLeadBy(Models.Database.TableGuest groupLeader);
    }

    // Implementation.
    public class BillGroupService : IBillGroupService
    {
        // Instance Variables.
        public IBillGroupFetcher find { get; set; }
        public IBillGroupPersistor persist { get; set; }
        public IBillGroupMutator mutate { get; set; }

        // Constructor.
        public BillGroupService(IBillGroupFetcher find, IBillGroupPersistor persist, IBillGroupMutator mutate)
        {
            this.find = find;
            this.persist = persist;
            this.mutate = mutate;
        }

        // Methods.
        /// <summary>
        /// Returns true if the tableGuest is already part of a BillGroup.
        /// </summary>
        /// <param name="tableGuest"></param>
        /// <returns></returns>
        public bool isGuestBeingPaidFor(Models.Database.TableGuest tableGuest )
        {
            try
            {
                return find.byPayee(tableGuest) != null;
            }
            catch (DnException e)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if any of the given table guests are being paid for.
        /// </summary>
        /// <param name="tableGuestList"></param>
        /// <returns></returns>
        public bool areAnyGuestsBeingPaidFor( IEnumerable<Models.Database.TableGuest> tableGuestList)
        {
            foreach(var tableGuest in tableGuestList)
            {
                if (isGuestBeingPaidFor(tableGuest))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Register the groupLeader as paying for the groupMembers.
        /// </summary>
        /// <param name="groupLeader">The payer.</param>
        /// <param name="groupMembers">The ones being payid for.</param>
        public async Task create(Models.Database.TableGuest groupLeader, IEnumerable<Models.Database.TableGuest> groupMembers)
        {
            foreach(var groupMember in groupMembers)
            {
                var billGrouping = new Models.Database.BillGroup(groupLeader.Id, groupMember.Id);
                await persist.Add(billGrouping);
            }
        }

        /// <summary>
        /// Delete a Bill Group lead by the given TableGuest.
        /// </summary>
        /// <param name="groupLeader">The one paying for the group.</param>
        public async Task deleteGroupLeadBy(Models.Database.TableGuest groupLeader)
        {
            // Get everyone who is being paid for by given group leader.
            var group = find.byPayer(groupLeader);

            // Delete records.
            foreach(var member in group)
            {
                mutate.delete(member);
            }

            // Save db.
            await persist.Save();
        }
    }
}
