﻿using Dinero.Models.Database;
using Dinero.Services.Core;

namespace Dinero.Services.BillGroup
{
    // Interface.
    public interface IBillGroupMutator : IMutator<Models.Database.BillGroup> { }

    // Implementation.
    public class BillGroupMutator : Mutator<Models.Database.BillGroup>, IBillGroupMutator
    {
        // Constructor.
        public BillGroupMutator(DineroContext database) : base(database) { }

        // Methods.
        public override void delete(Models.Database.BillGroup obj)
        {
            database.BillGroup.Remove(obj);
        }
    }
}
