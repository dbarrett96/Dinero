﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dinero.Services.BillGroup
{
    // Interface.
    public interface IBillGroupFetcher
    {
        Models.Database.BillGroup byPayee(Models.Database.TableGuest tableGuest);
        ICollection<Models.Database.BillGroup> byPayer(Models.Database.TableGuest tableGuest);
    }

    // Implementation.
    public class BillGroupFetcher : Fetcher, IBillGroupFetcher
    {
        // Constructor.
        public BillGroupFetcher(DineroContext database) : base(database) { }

        // IBillGroupFetcher.
        public Models.Database.BillGroup byPayee(Models.Database.TableGuest tableGuest)
        {
            try
            {
                return database.BillGroup.Single(group => group.Guest == tableGuest.Id);
            }
            catch (InvalidOperationException e)
            {
                throw new DnException("Could not find any record of table guest being paid for.", e);
            }
        }

        public ICollection<Models.Database.BillGroup> byPayer(Models.Database.TableGuest tableGuest)
        {
            return database.BillGroup.Where(group => group.GroupLeader == tableGuest.Id).ToList();
        }
    }
}
