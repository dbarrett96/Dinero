﻿using Dinero.Services.Core;
using Dinero.Services.SignIn;
using Dinero.Services.SignUp;

namespace Dinero.Services.User
{
    // Interface.
    public interface IUserService
    {
        IUserFetcher find { get; set; }
        ISignUpService signUp { get; set; }
        ISignInService signIn { get; set; }
        IIdCoder id { get; set; }
    }

    // Implementation.
    public class UserService: IUserService
    {
        public IUserFetcher find { get; set; }
        public ISignUpService signUp { get; set; }
        public ISignInService signIn { get; set; }
        public IIdCoder id { get; set; }

        // Constructor.
        public UserService(IUserFetcher find, ISignUpService signUp, ISignInService signIn, IIdCoder id)
        {
            this.find = find;
            this.signUp = signUp;
            this.signIn = signIn;
            this.id = id;
        }
    }
}
