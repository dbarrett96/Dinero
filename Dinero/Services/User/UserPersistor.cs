﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.User
{
    // Interface.
    public interface IUserPersistor : IPersistor<Models.Database.User> { }

    // Implementation.
    public class UserPersistor : Persistor<Models.Database.User>, IUserPersistor
    {
        public UserPersistor(DineroContext database) : base(database) { }

        public override async Task Add(Models.Database.User obj)
        {
            database.User.Add(obj);
            await database.SaveChangesAsync();
        }
    }
}
