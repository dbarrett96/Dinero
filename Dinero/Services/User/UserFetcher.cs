﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System;
using System.Linq;

namespace Dinero.Services.User
{
    // Interface.
    public interface IUserFetcher
    {
        Models.Database.User byPhoneNumber(string phoneNumber);
        Models.Database.User byAccessToken(string accessToken);
        Models.Database.User byId(long id);
    }

    // Implementation.
    public class UserFetcher: IUserFetcher
    {
        // Instance Variables.
        private readonly DineroContext database;

        // Constructor.
        public UserFetcher(DineroContext database)
        {
            this.database = database;
        }

        // IUserRetrievalService
        /// <summary>
        /// Retrieve a user profile using the phone number registered to the profile.
        /// </summary>
        /// <param name="phoneNumber">The users phone number.</param>
        /// <returns>The user registerd to the given phoneNumber, or null if no such user could be found.</returns>
        public Models.Database.User byPhoneNumber(string phoneNumber)
        {
            try
            {
                return database.User.Single(user => user.PhoneNumber == phoneNumber);
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchUserException();
            }
        }

        /// <summary>
        /// Retreive a user profile using the access token corresponding to the profile.
        /// </summary>
        /// <param name="accessToken">The access token provided as authorization for a request.</param>
        /// <returns>The user corresponding to the access token.</returns>
        public Models.Database.User byAccessToken(string accessToken)
        {
            try
            {
                return (
                    from token in database.Token
                    join user in database.User on token.UserId equals user.Id
                    where token.AccessToken == accessToken
                    select user
                ).Single();
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchUserException();
            }
        }

        /// <summary>
        /// Retreive a user profile using their user id.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <returns>The user corresponding to the access token.</returns>
        public Models.Database.User byId(long id)
        {
            try
            {
                return database.User.Single(u => u.Id == id);
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchUserException();
            }
        }
    }

    /// <summary>
    /// Thrown when a user matching a specified credential set cannot be found in the database.
    /// </summary>
    public class NoSuchUserException: DnException
    {
        public NoSuchUserException() : base("No such user.", null) { }
    }
}
