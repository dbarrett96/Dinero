﻿using Dinero.Models.Database;
using System;
using System.Linq;
using Dinero.Services.Token.Exceptions;

namespace Dinero.Services.Token
{
    public interface ITokenFetcher
    {
        Models.Database.Token byUser(Models.Database.User user);
        Models.Database.Token byRefreshToken(string refreshToken);
    }

    public class TokenFetcher : ITokenFetcher
    {
        // Instance Variables.
        private DineroContext database;

        // Constructor.
        public TokenFetcher(DineroContext database)
        {
            this.database = database;
        }

        // ITokenFetcher.
        public Models.Database.Token byUser(Models.Database.User user)
        {
            try
            {
                return database.Token.Single(token => token.UserId == user.Id);
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchTokenException(e);
            }
        }

        public Models.Database.Token byRefreshToken(string refreshToken)
        {
            try
            {
                return database.Token.Single(token => token.RefreshToken == refreshToken);
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchTokenException(e);
            }
        }
    }
}
