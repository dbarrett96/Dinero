﻿using Dinero.Services.Core;
using System;

namespace Dinero.Services.Token.Exceptions
{
    public class NoSuchTokenException: DnException
    {
        public NoSuchTokenException(Exception e) : base($"A token could not be found matching the search parameters.).", e) { }
    }
}
