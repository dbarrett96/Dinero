﻿namespace Dinero.Services.Token
{
    public interface ITokenService
    {
        ITokenFetcher find { get; set; }
        ITokenPersistor persist { get; set; }
    }

    public class TokenService: ITokenService
    {
        // Instance Variables.
        public ITokenFetcher find { get; set; }
        public ITokenPersistor persist { get; set; }

        // Constructor.
        public TokenService(ITokenFetcher find, ITokenPersistor persist)
        {
            this.find = find;
            this.persist = persist;
        }
    }
}
