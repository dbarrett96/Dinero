﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.Token
{
    // Interface.
    public interface ITokenPersistor: IPersistor<Models.Database.Token> {}

    // Implementation.
    public class TokenPersistor :  Persistor<Models.Database.Token>, ITokenPersistor
    {
        public TokenPersistor(DineroContext db) : base(db) { }

        public override async Task Add(Models.Database.Token obj)
        {
            database.Token.Add(obj);
            await Save();
        }
    }
}
