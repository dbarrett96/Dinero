﻿using Dinero.Services.Core;
using System;

namespace Dinero.Services.SignIn.Exceptions
{
    public class NoSuchSecretException: DnException
    {
        public NoSuchSecretException(Exception e) : base("A sign in secret matching the search parameters was not found.", e) { }
    }
}
