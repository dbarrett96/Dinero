﻿using Dinero.Models.Database;
using Dinero.Services.User;
using System.Threading.Tasks;

namespace Dinero.Services.SignIn
{
    // Interface.
    public interface ISignInService
    {
        ISignInSecretFetcher findSecret { get; set; }

        Task<SigninSecret> byUser(Models.Database.User user);
    }

    // Implementation.
    public class SignInService : ISignInService
    {
        // Private Instance Variables.
        private IUserFetcher findUser;
        private ISignInSecretPersistor persistSecret;

        // Constructor.
        public SignInService(IUserFetcher findUser, ISignInSecretFetcher findSecret, ISignInSecretPersistor persistSecret)
        {
            this.findUser = findUser;
            this.findSecret = findSecret;
            this.persistSecret = persistSecret;
        }

        // ISignInService.
        public ISignInSecretFetcher findSecret { get; set; }

        public async Task<SigninSecret> byUser(Models.Database.User user)
        {
            SigninSecret secret;

            try
            {
                secret = findSecret.byUser(user);
                secret.refresh();
                await persistSecret.Save();
            }
            catch
            {
                secret = new SigninSecret(user.PhoneNumber);
                await persistSecret.Add(secret);
            }
            
            return secret;
        }
    }
}
