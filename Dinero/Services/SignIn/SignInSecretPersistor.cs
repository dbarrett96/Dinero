﻿using Dinero.Models.Database;
using Dinero.Services.Core;
using System.Threading.Tasks;

namespace Dinero.Services.SignIn
{
    // Interface.
    public interface ISignInSecretPersistor : IPersistor<SigninSecret> { }

    // Implementation.
    public class SignInSecretPersistor : Persistor<SigninSecret>, ISignInSecretPersistor
    {
        // Constructor.
        public SignInSecretPersistor(DineroContext db) : base(db) { }

        // Public Methods.
        public override async Task Add(SigninSecret obj)
        {
            database.SigninSecret.Add(obj);
            await Save();
        }
    }
}
