﻿using Dinero.Models.Database;
using Dinero.Services.SignIn.Exceptions;
using System;
using System.Linq;

namespace Dinero.Services.SignIn
{
    // Interface.
    public interface ISignInSecretFetcher
    {
        SigninSecret byUser(Models.Database.User user);
        SigninSecret byPhoneNumber(string phoneNumber);
        SigninSecret bySecret(string secret);
    }

    // Implementation.
    public class SignInSecretFetcher: ISignInSecretFetcher
    {
        // Private Instance Variables.
        private DineroContext database;

        // Constructor.
        public SignInSecretFetcher(DineroContext database)
        {
            this.database = database;
        }

        // ISignInSecretFetcher
        /// <summary>
        /// Retrieve the SigninSecret for a specific user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>The sign in secret for the current user, or null if none exists.</returns>
        public SigninSecret byUser(Models.Database.User user)
        {
            return byPhoneNumber(user.PhoneNumber);
        }

        public SigninSecret byPhoneNumber(string phoneNumber)
        {
            try
            {
                return database.SigninSecret.Single(secret => secret.PhoneNumber == phoneNumber);
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchSecretException(e);
            }
        }

        public SigninSecret bySecret(string secret)
        {
            try
            {
                return database.SigninSecret.Single(s => s.Secret == secret);
            }
            catch (InvalidOperationException e)
            {
                throw new NoSuchSecretException(e);
            }
        }
    }
}
