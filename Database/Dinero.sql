USE [Dinero]
GO
/****** Object:  Table [dbo].[BillGroup]    Script Date: 2018-06-02 10:15:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillGroup](
	[GroupLeader] [bigint] NOT NULL,
	[Guest] [bigint] NOT NULL,
 CONSTRAINT [PK_BillGroup] PRIMARY KEY CLUSTERED 
(
	[GroupLeader] ASC,
	[Guest] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerSession]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerSession](
	[UserId] [bigint] NOT NULL,
	[TableSessionId] [bigint] NOT NULL,
	[PaymentId] [bigint] NOT NULL,
 CONSTRAINT [PK_CustomerSession] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[TableSessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fee]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FeeType] [tinyint] NOT NULL,
	[Description] [nvarchar](64) NOT NULL,
	[Amount] [bigint] NOT NULL,
	[Currency] [smallint] NULL,
 CONSTRAINT [PK_Fee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItem]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RestaurantId] [bigint] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](512) NOT NULL,
	[Price] [int] NOT NULL,
	[Currency] [int] NOT NULL,
	[Category] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_MenuItem4] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItemFee]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItemFee](
	[MenuItemId] [bigint] NOT NULL,
	[FeeId] [bigint] NOT NULL,
 CONSTRAINT [PK_MenuItemFee] PRIMARY KEY CLUSTERED 
(
	[MenuItemId] ASC,
	[FeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItemOption]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItemOption](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MenuItemId] [bigint] NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
	[Category] [nvarchar](256) NOT NULL,
	[Surcharge] [int] NULL,
	[Currency] [int] NULL,
 CONSTRAINT [PK_MenuItemOption] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItem]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MenuItemId] [bigint] NOT NULL,
	[TableGuestId] [bigint] NOT NULL,
	[State] [tinyint] NOT NULL,
 CONSTRAINT [PK_OrderItem2_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItemFee]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItemFee](
	[OrderItemId] [bigint] NOT NULL,
	[FeeId] [bigint] NOT NULL,
 CONSTRAINT [PK_OrderItemFee] PRIMARY KEY CLUSTERED 
(
	[OrderItemId] ASC,
	[FeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItemOption]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItemOption](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderItemId] [bigint] NOT NULL,
	[MenuItemOptionId] [bigint] NOT NULL,
 CONSTRAINT [PK_OrderItemOption2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[TableSessionId] [bigint] NOT NULL,
	[PaymentId] [varchar](256) NOT NULL,
	[PaymentConfirmation] [varchar](256) NULL,
	[Provider] [smallint] NOT NULL,
	[Amount] [bigint] NOT NULL,
	[Currency] [smallint] NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Restaurant]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Restaurant](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Address] [nvarchar](256) NOT NULL,
	[City] [nvarchar](32) NOT NULL,
	[State] [nvarchar](32) NOT NULL,
	[Country] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_Restaurant2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SigninSecret]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SigninSecret](
	[PhoneNumber] [varchar](32) NOT NULL,
	[Pin] [varchar](6) NOT NULL,
	[Secret] [varchar](32) NOT NULL,
 CONSTRAINT [PK_SigninSecret] PRIMARY KEY CLUSTERED 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ResturantId] [bigint] NOT NULL,
	[Number] [int] NOT NULL,
	[Seats] [int] NOT NULL,
 CONSTRAINT [PK_Table] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableAlias]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableAlias](
	[TableId] [bigint] NOT NULL,
	[Alias] [varchar](128) NOT NULL,
 CONSTRAINT [PK_TableAlias2] PRIMARY KEY CLUSTERED 
(
	[TableId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableGuest]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableGuest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableSessionId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[IsGroupLeader] [bit] NOT NULL,
 CONSTRAINT [PK_TableGuest2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableGuestRequest]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableGuestRequest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[TableId] [bigint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ExpiresAt] [datetime] NOT NULL,
 CONSTRAINT [PK_TableGuestRequest2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableSession]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableSession](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableId] [bigint] NOT NULL,
	[StartedAt] [datetime] NOT NULL,
	[EndedAt] [datetime] NULL,
 CONSTRAINT [PK_TableSession2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Token]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token](
	[UserId] [bigint] NOT NULL,
	[AccessToken] [varchar](1024) NOT NULL,
	[RefreshToken] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_Token] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 2018-06-02 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](128) NOT NULL,
	[LastName] [varchar](128) NOT NULL,
	[PhoneNumber] [varchar](32) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BillGroup]  WITH CHECK ADD  CONSTRAINT [FK_BillGroup_TableGuest] FOREIGN KEY([GroupLeader])
REFERENCES [dbo].[TableGuest] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BillGroup] CHECK CONSTRAINT [FK_BillGroup_TableGuest]
GO
ALTER TABLE [dbo].[BillGroup]  WITH CHECK ADD  CONSTRAINT [FK_BillGroup_TableGuest1] FOREIGN KEY([Guest])
REFERENCES [dbo].[TableGuest] ([Id])
GO
ALTER TABLE [dbo].[BillGroup] CHECK CONSTRAINT [FK_BillGroup_TableGuest1]
GO
ALTER TABLE [dbo].[CustomerSession]  WITH CHECK ADD  CONSTRAINT [FK_CustomerSession_Payment] FOREIGN KEY([PaymentId])
REFERENCES [dbo].[Payment] ([Id])
GO
ALTER TABLE [dbo].[CustomerSession] CHECK CONSTRAINT [FK_CustomerSession_Payment]
GO
ALTER TABLE [dbo].[CustomerSession]  WITH CHECK ADD  CONSTRAINT [FK_CustomerSession_TableSession] FOREIGN KEY([TableSessionId])
REFERENCES [dbo].[TableSession] ([Id])
GO
ALTER TABLE [dbo].[CustomerSession] CHECK CONSTRAINT [FK_CustomerSession_TableSession]
GO
ALTER TABLE [dbo].[CustomerSession]  WITH CHECK ADD  CONSTRAINT [FK_CustomerSession_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[CustomerSession] CHECK CONSTRAINT [FK_CustomerSession_User]
GO
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD  CONSTRAINT [FK_MenuItem_Restaurant] FOREIGN KEY([RestaurantId])
REFERENCES [dbo].[Restaurant] ([Id])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_Restaurant]
GO
ALTER TABLE [dbo].[MenuItemFee]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemFee_Fee] FOREIGN KEY([FeeId])
REFERENCES [dbo].[Fee] ([Id])
GO
ALTER TABLE [dbo].[MenuItemFee] CHECK CONSTRAINT [FK_MenuItemFee_Fee]
GO
ALTER TABLE [dbo].[MenuItemFee]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemFee_MenuItem] FOREIGN KEY([MenuItemId])
REFERENCES [dbo].[MenuItem] ([Id])
GO
ALTER TABLE [dbo].[MenuItemFee] CHECK CONSTRAINT [FK_MenuItemFee_MenuItem]
GO
ALTER TABLE [dbo].[MenuItemOption]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemOption_MenuItem] FOREIGN KEY([MenuItemId])
REFERENCES [dbo].[MenuItem] ([Id])
GO
ALTER TABLE [dbo].[MenuItemOption] CHECK CONSTRAINT [FK_MenuItemOption_MenuItem]
GO
ALTER TABLE [dbo].[OrderItem]  WITH CHECK ADD  CONSTRAINT [FK_OrderItem_TableGuest] FOREIGN KEY([TableGuestId])
REFERENCES [dbo].[TableGuest] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderItem] CHECK CONSTRAINT [FK_OrderItem_TableGuest]
GO
ALTER TABLE [dbo].[OrderItem]  WITH CHECK ADD  CONSTRAINT [FK_OrderItem2_MenuItem1] FOREIGN KEY([MenuItemId])
REFERENCES [dbo].[MenuItem] ([Id])
GO
ALTER TABLE [dbo].[OrderItem] CHECK CONSTRAINT [FK_OrderItem2_MenuItem1]
GO
ALTER TABLE [dbo].[OrderItemFee]  WITH CHECK ADD  CONSTRAINT [FK_OrderItemFee_Fee] FOREIGN KEY([FeeId])
REFERENCES [dbo].[Fee] ([Id])
GO
ALTER TABLE [dbo].[OrderItemFee] CHECK CONSTRAINT [FK_OrderItemFee_Fee]
GO
ALTER TABLE [dbo].[OrderItemFee]  WITH CHECK ADD  CONSTRAINT [FK_OrderItemFee_OrderItem] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[OrderItem] ([Id])
GO
ALTER TABLE [dbo].[OrderItemFee] CHECK CONSTRAINT [FK_OrderItemFee_OrderItem]
GO
ALTER TABLE [dbo].[OrderItemOption]  WITH CHECK ADD  CONSTRAINT [FK_OrderItemOption2_MenuItemOption] FOREIGN KEY([MenuItemOptionId])
REFERENCES [dbo].[MenuItemOption] ([Id])
GO
ALTER TABLE [dbo].[OrderItemOption] CHECK CONSTRAINT [FK_OrderItemOption2_MenuItemOption]
GO
ALTER TABLE [dbo].[OrderItemOption]  WITH CHECK ADD  CONSTRAINT [FK_OrderItemOption2_OrderItem] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[OrderItem] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderItemOption] CHECK CONSTRAINT [FK_OrderItemOption2_OrderItem]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_TableSession] FOREIGN KEY([TableSessionId])
REFERENCES [dbo].[TableSession] ([Id])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_TableSession]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_User]
GO
ALTER TABLE [dbo].[SigninSecret]  WITH CHECK ADD  CONSTRAINT [FK_SigninSecret_User] FOREIGN KEY([PhoneNumber])
REFERENCES [dbo].[User] ([PhoneNumber])
GO
ALTER TABLE [dbo].[SigninSecret] CHECK CONSTRAINT [FK_SigninSecret_User]
GO
ALTER TABLE [dbo].[Table]  WITH CHECK ADD  CONSTRAINT [FK_Table_Restaurant] FOREIGN KEY([ResturantId])
REFERENCES [dbo].[Restaurant] ([Id])
GO
ALTER TABLE [dbo].[Table] CHECK CONSTRAINT [FK_Table_Restaurant]
GO
ALTER TABLE [dbo].[TableAlias]  WITH CHECK ADD  CONSTRAINT [FK_TableAlias2_Table] FOREIGN KEY([TableId])
REFERENCES [dbo].[Table] ([Id])
GO
ALTER TABLE [dbo].[TableAlias] CHECK CONSTRAINT [FK_TableAlias2_Table]
GO
ALTER TABLE [dbo].[TableGuest]  WITH CHECK ADD  CONSTRAINT [FK_TableGuest2_TableSession1] FOREIGN KEY([TableSessionId])
REFERENCES [dbo].[TableSession] ([Id])
GO
ALTER TABLE [dbo].[TableGuest] CHECK CONSTRAINT [FK_TableGuest2_TableSession1]
GO
ALTER TABLE [dbo].[TableGuest]  WITH CHECK ADD  CONSTRAINT [FK_TableGuest2_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[TableGuest] CHECK CONSTRAINT [FK_TableGuest2_User]
GO
ALTER TABLE [dbo].[TableGuestRequest]  WITH CHECK ADD  CONSTRAINT [FK_TableGuestRequest2_Table] FOREIGN KEY([TableId])
REFERENCES [dbo].[Table] ([Id])
GO
ALTER TABLE [dbo].[TableGuestRequest] CHECK CONSTRAINT [FK_TableGuestRequest2_Table]
GO
ALTER TABLE [dbo].[TableGuestRequest]  WITH CHECK ADD  CONSTRAINT [FK_TableGuestRequest2_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[TableGuestRequest] CHECK CONSTRAINT [FK_TableGuestRequest2_User]
GO
ALTER TABLE [dbo].[TableSession]  WITH CHECK ADD  CONSTRAINT [FK_TableSession_Table2] FOREIGN KEY([TableId])
REFERENCES [dbo].[Table] ([Id])
GO
ALTER TABLE [dbo].[TableSession] CHECK CONSTRAINT [FK_TableSession_Table2]
GO
ALTER TABLE [dbo].[Token]  WITH CHECK ADD  CONSTRAINT [FK_Token_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Token] CHECK CONSTRAINT [FK_Token_User]
GO
