USE [Dinero]
GO
SET IDENTITY_INSERT [dbo].[Restaurant] ON 
GO
INSERT [dbo].[Restaurant] ([Id], [Name], [Address], [City], [State], [Country]) VALUES (1, N'The Hideaway', N'4240 Highway 620', N'Coe Hill', N'Ontario', N'Canada')
GO
SET IDENTITY_INSERT [dbo].[Restaurant] OFF
GO
SET IDENTITY_INSERT [dbo].[MenuItem] ON 
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (1, 1, N'Cool Slaw', N'peace, love and cabbage', 495, 0, N'Sides')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (2, 1, N'Goat ''N Weeds', N'mixed leaves, tomato, candied sunflower seeds, crumbled goat cheese and topped with choice of balsamic dijon or peppery buttermilk dressing', 495, 0, N'Sides')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (4, 1, N'Rice ''N Beans', N'basmati rice topped with pinto beans in a toasted pumpkin seed mole ‘sauce', 495, 0, N'Sides')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (5, 1, N'Hot Buff', N'bacon, blue cheese, hot peppers, hot sauce, arugula', 1095, 0, N'Burgers')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (6, 1, N'Easy Eh', N'avocado relish, tomato, arugula, cheddar', 1095, 0, N'Burgers')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (7, 1, N'Zesty Pork-Pineapple', N'perfect balance of sweet and spicy: grilled pork tenderloin, onion, cilantro, pineapple topped with adobo honey aioli', 995, 0, N'Dos Tacos De')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (8, 1, N'Baja Fish', N'hand held favourite: crispy battered haddock, romaine, cheddar, pickled pico de gallo and zippy sauce', 995, 0, N'Dos Tacos De')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (13, 1, N'Ultimate Grilled Cheese', N'gooey medley of cheddar and asiago cheeses, bacon, tomato, grilled onions all on toasted bread', 1095, 0, N'Sandwiches')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (14, 1, N'No Frills Fish', N'no-frills, just a damn good fish sandwich: crusted haddock topped
with tartar, cheddar served on toasted bread', 1295, 0, N'Sandwiches')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (15, 1, N'Pasta', N'changes every day – check the board', 1695, 0, N'After Five')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (17, 1, N'Shrimp Diablo', N'really big shrimp poached in the devils sauce served over rice and peas', 2295, 0, N'After Five')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (18, 1, N'Grilled Steak and Onions', N'an almost holy combination of sliced flank steak and onions: this synthesis is delicious served with blue cheese mashed potato and a side of leftover wine sauce', 2395, 0, N'After Five')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (19, 1, N'Curry Dip and Fry Bread', N'great bread with a great dip', 595, 0, N'Other Stuff')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (22, 1, N'Crisp Flour Tortilla with Guac', N'it’s all about fresh made to order', 695, 0, N'Other Stuff')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (23, 1, N'PEI Mussels', N'1 pound steamed in white wine, garlic and salsa', 1395, 0, N'Other Stuff')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (24, 1, N'BBQ Sundae Poutine', N'first off, it’s important to point out that there is no ice cream: layered cheesy beans, slaw and BBQ pork tenderloin served with a side of fries', 995, 0, N'Other Stuff')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (25, 1, N'Waffles and Ice Cream', N'', 595, 0, N'Desserts')
GO
INSERT [dbo].[MenuItem] ([Id], [RestaurantId], [Name], [Description], [Price], [Currency], [Category]) VALUES (26, 1, N'Frozen Cups', N'', 695, 0, N'Desserts')
GO
SET IDENTITY_INSERT [dbo].[MenuItem] OFF
GO
SET IDENTITY_INSERT [dbo].[MenuItemOption] ON 
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (1, 2, N'Balsamic Dijon', N'Dressing', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (2, 2, N'Peppery Ranch', N'Dressing', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (3, 4, N'Goat Cheese', N'Toppings', 250, N'0         ')
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (4, 5, N'Beef (6oz)', N'Patty', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (5, 5, N'Veggie (6oz)', N'Patty', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (6, 5, N'Chicken Breast', N'Patty', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (8, 6, N'Beef (6oz)', N'Patty', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (9, 6, N'Veggie (6oz)', N'Patty', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (10, 6, N'Chicken Breast', N'Patty', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (11, 7, N'Rice', N'On...', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (12, 7, N'Corn Tortillas', N'On...', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (13, 8, N'Rice ', N'On...', NULL, NULL)
GO
INSERT [dbo].[MenuItemOption] ([Id], [MenuItemId], [Description], [Category], [Surcharge], [Currency]) VALUES (14, 8, N'Corn Tortillas', N'On...', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[MenuItemOption] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([Id], [FirstName], [LastName], [PhoneNumber]) VALUES (1, N'Damon', N'Bar', N'416-555-0001')
GO
INSERT [dbo].[User] ([Id], [FirstName], [LastName], [PhoneNumber]) VALUES (2, N'Cooper', N'Bar', N'416-555-0002')
GO
INSERT [dbo].[User] ([Id], [FirstName], [LastName], [PhoneNumber]) VALUES (3, N'Glen', N'Bar', N'416-555-0003')
GO
INSERT [dbo].[User] ([Id], [FirstName], [LastName], [PhoneNumber]) VALUES (4, N'Cynthia', N'Bar', N'416-555-0004')
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[Table] ON 
GO
INSERT [dbo].[Table] ([Id], [ResturantId], [Number], [Seats]) VALUES (1, 1, 1, 4)
GO
SET IDENTITY_INSERT [dbo].[Table] OFF
GO
INSERT [dbo].[TableAlias] ([TableId], [Alias]) VALUES (1, N'wx4ulrvnwzq62ccdt1ib4k')
GO
INSERT [dbo].[Token] ([UserId], [AccessToken], [RefreshToken]) VALUES (1, N'5DgD36K3d1hYxH4mOyGSAwSGYk13h8hTDAA1BupWsqGafPjXke8dhMOCVAm7sAmYgwAK4SLfmBSl8DgpymC70Sa6oa8NaLqplnmAprQEAQoqMOsSK4nlLHfbfRKNDEIqho6e5rYJdJ9zJIhC4eClW4325wA7Ge46I1XqLW6Zft8fHINBZ1fNHzXMrUFwdyiVK4nlFZTJ34qvaIejP1E00HXQgT6pPav6WLlBAl8dP9zbfazKjLeCwn1bYNsZHsHW', N'mRxt7Hjmf6bQeDN8XbkJ5GWBxo2iV4S0vL6h24dw03PLL7Y3iBAqNKrigbgi6q4fN1aZn1zPkhWcI8GljUBd40qz91jDylSJ4Rri3UJE2UdpPhrbnWnEF3b6AnSKfVIoS0zCWjj2H4wjR9eZajaFjJDE2EBq3ah5MCfJ14rB5fWGxm6PIC0eGYGuC9Hb1iUb6EHha2tHj0IRESMjIBtqARRJgCSjh9IUhSyKvf5Z4nw4Dac2DQfhg9kZ1JhroLYV')
GO
INSERT [dbo].[Token] ([UserId], [AccessToken], [RefreshToken]) VALUES (2, N'tp1QlzrtgnnimxW3o05DzJpyvcMQAa9eB8NpdGODu5y3n4lpK2aRynFEM3thIfyeQ8rRH2QLzgi7qooCWitq5wR9nVItaRjFLuIEoTShhpDZG9SsnYuhze3hTOMnVNif90JG1PbLvm3gpJ6DpuBKzdInqHXOzqk5roxYxsg7xQxwZfuyJs8PUDwJVuddTeRXDqg5FNJWp18H6krOs8mjG17A0HknlNQAGzDntT8c311nXGl3RC3BbEOgOppSrpKE', N'FoVz6WV0pPIOOlLlMym7E66joK0HMYWBD94yOfqAVlEXSEX2IzxCZsPrAenknY3qdi4e8tXTBxN3xBz8nxCCbNQPuJW3QbwAFQgmwkGPvyyHDR0J74iQroDXhtdLh9igqX1Gc9zxe1A8FStVc2zz3c8w2ohzMhyIR1TUhVLafDbo32uEB6vnwB2KPX2Ga72ysYyh58UVt9lTZRED51WZumBcvGjFS58GcU7wt6hAmC8LPSaA3d1blj7ELjcKehKH')
GO
INSERT [dbo].[Token] ([UserId], [AccessToken], [RefreshToken]) VALUES (3, N'aTXEyOCs3qzbdiX8b21gkKVBN5q5WS3oVXw1WhWA9UflagU0Qa5pZYpq9KblhVBfIfGTMcoeYiTrUt2ZGOVVxOLl8NM33cKsz1z6cDw50lJa7TD4kjKrNYLNUv1GbZwgrQRPZZ45g2hwZ7Y4kCQ5EhYSeNFPpbhWWWUeuyT3JnH3G2BmZ8saBJ8FqBsougk5XkWQVQUqk9rRf1GifdYOoMgSZodZbewfUKho5MFIMDgpVA2rOaeC9fMnOKBIZmDp', N'1Z8XaRMgLJjgYob1OLGGfW4SngoW6opRpH8YrOHTSAXlRD1cbXBf5Uy5ayBdtoRwJvn1n7oT3YtSdKg6FiHxY68TQOXj5cJ3g7Wdz8ck26Uf6IEmWewtP4D0Iqt071VZGelhwAbEQMiAMw7f8Ry0uNX96Wa473UNFgFtsjJXwNLeta34xw3J3bwXbxHXElH5pVAilljshYgX3NTKIzSeyswUX9msDD1OI20Nj9HzJEPUJ1cjRXqH3RymwC9a4vCD')
GO
INSERT [dbo].[Token] ([UserId], [AccessToken], [RefreshToken]) VALUES (4, N'c65uFazwzojmX7WzP1eiXqx3BHCoV5FfEzLlHiNDY75nDuEcUUwDsue03SG0g9KedihtjVH3b8iRWyHJD6Y97pW63FbFc7kmYLjdPY2CXbsZywBzf4mm5wVhp6KHCHdTXXbSS8MUrSFaToAkcOp79ODrbT7yVvG0YY66SOLSk4eCi4HviAgxuAWTpG4mrZJn9XNci7DcazofC6XCZyz3GUom9D8cNzSj6aW2VnsSCM51oj24uIUCPZ6D830lscgw', N'hONyChMql9P5Xg1Glecrk9LkD383GL3M18gayyiO44dQP6OYIUW2BF7TVG3xyzhvL8R0zhj4iMxskejF6bCx9IfpyS4WbQIeYgMYSFuYcQRS6ut3yB8UmM6ppHN4M4blTK7hhsDKkKGWYHlStLtMNTvnHnnylKUwhEgXxLK8pwOL0unp5HobObtQH8plBC2fY1Eoz2SWrtFtWpvPRmQMq3FK552mVyHl9haR1IAIubU86z5Qeo9pF26iAq3BMj6I')
GO
INSERT [dbo].[SigninSecret] ([PhoneNumber], [Pin], [Secret]) VALUES (N'416-555-0001', N'984344', N'qTSx3ULRsvLllEsZFazk6zmN6QAZUzMQ')
GO
INSERT [dbo].[SigninSecret] ([PhoneNumber], [Pin], [Secret]) VALUES (N'416-555-0002', N'448978', N'fGr9MpBcZyHQS0qq3Gz8cBwoFEFnd4Tw')
GO
INSERT [dbo].[SigninSecret] ([PhoneNumber], [Pin], [Secret]) VALUES (N'416-555-0003', N'107695', N'XBcDs61g9JfV6rwyfZ6FwYUeD1Km7LIl')
GO
INSERT [dbo].[SigninSecret] ([PhoneNumber], [Pin], [Secret]) VALUES (N'416-555-0004', N'558128', N'aQ67zqKopWJUhHg7ux1OKakaQ6Yi4mgU')
GO
