//
//  LaunchView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-10.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

class LaunchView: View<LaunchViewDelegate> {
    override func createView() {
        self.backgroundColor = .red
    }
}

extension LaunchView: LaunchViewModel {
    
}
