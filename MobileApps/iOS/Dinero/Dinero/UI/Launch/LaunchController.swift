//
//  LaunchController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-10.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class LaunchController: Controller<LaunchViewModel, LaunchViewDelegate, LaunchView> {
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Session.current.cache.accessToken == nil {
            Session.current.state = .unauthenticated
            self.segueToLanding()
        }
        else {
            self.checkForActiveDiningSession()
        }
    }
    
    func checkForActiveDiningSession() {
        CurrentSessionRequest().send().then { response -> Void in
            Session.current.cache.sessionId = response.sessionId
            Session.current.cache.tableId = response.tableId
            Session.current.cache.restaurantId = response.restaurantId
            self.segueToDiningSession()
        }.catch { error in
            if let apiError = error as? ApiError {
                switch apiError {
                case .invalidResponse(_, _, let statusCode):
                    if statusCode == 400 {
                        self.segueToHome()
                        break
                    }
                    else {
                        fallthrough
                    }
                default: self.segueToLanding()
                }
            }
            else {
                self.segueToLanding()
            }
        }
    }
    
    func segueToLanding() {
        UIApplication.setRoot( LandingController().withNavigation() )
    }
    
    func segueToHome() {
         UIApplication.setRoot( HomeController().withNavigation() )
    }
    
    func segueToDiningSession() {
        UIApplication.setRoot( MenuController().withNavigation() )
    }
}

extension LaunchController: LaunchViewDelegate {}
