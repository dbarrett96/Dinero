//
//  MenuController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-09.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class MenuController: Controller<MenuViewModel, MenuViewDelegate, MenuView> {
    override var navbarTitle: String? {
        return "The Hideaway"
    }
    
    override var navbarColor: UIColor? {
        return .gray8
    }
    
    override var rightBarButton: UIBarButtonItem? {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "IconListWhite"), style: .plain, target: nil, action: nil)
    }
    
    override var leftBarButton: UIBarButtonItem? {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "IconGroupWhite"), style: .plain, target: nil, action: nil)
    }
}

extension MenuController: MenuViewDelegate {
    
}
