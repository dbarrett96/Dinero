//
//  SignUpViewModel.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

protocol SignUpViewModel {
    // MARK: - Properties
    var phoneNumber: PhoneNumber? { get }
    var firstName: String? { get }
    var lastName: String? { get }
    
    // MARK: - Instance Variables
    var isSignUpButtonEnabled: Bool { get set }
    
    // MARK: - Methods
    func showKeyboard()
}
