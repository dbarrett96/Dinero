//
//  SignUpViewDelegate.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

protocol SignUpViewDelegate {
    func signUp()
    func formContentChanged()
}
