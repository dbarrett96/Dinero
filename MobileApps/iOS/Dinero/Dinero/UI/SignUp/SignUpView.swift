//
//  SignUpView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class SignUpView: View<SignUpViewDelegate>, KeyboardNotifiable, UITextFieldDelegate {
    private let style = Style()
    
    // MARK: - Subviews
    private lazy var phoneNumberField = PhoneNumberField()
    private lazy var firstNameField = UITextField(self.style.fields.textStyle)
    private lazy var lastNameField = UITextField(self.style.fields.textStyle)
    private lazy var fieldSeparator = UIView()
    private lazy var nameFieldsBackgroundView = UIView()
    private lazy var signUpButton = StatefulTextButton(self.style.signUpButton.styles[.disabled]!.text)
    
    // MARK: - Methods
    override func createView() {
        self.addGradientBackground(colors: AppStyle.Colors.landingGradient,
                                   isFullscreen: true)
        
        self.phoneNumberField.delegate = self
        
        self.firstNameField.placeholder = "First Name"
        self.lastNameField.placeholder = "Last Name"
        self.firstNameField.autocorrectionType = .no
        self.lastNameField.autocorrectionType = .no
        self.firstNameField.delegate = self
        self.lastNameField.delegate = self
        self.firstNameField.returnKeyType = .next
        self.lastNameField.returnKeyType = .done
        
        self.fieldSeparator.backgroundColor = self.style.fields.separatorColor
        
        self.nameFieldsBackgroundView.backgroundColor = self.style.fields.backgroundColor
        self.nameFieldsBackgroundView.layer.cornerRadius = self.style.fields.backgroundCornerRadius
        self.nameFieldsBackgroundView.addShadow()
        
        self.signUpButton.styles = self.style.signUpButton.styles
        self.signUpButton.status = .disabled
        
        self.addSubview( self.phoneNumberField )
        self.addSubview( self.signUpButton )
        self.addSubview( self.nameFieldsBackgroundView )
        self.addSubview( self.firstNameField )
        self.addSubview( self.lastNameField )
        self.addSubview( self.fieldSeparator )
        
        self.setGestureRecognizers()
        self.setConstraints()
    }
    
    private func setConstraints() {
        self.phoneNumberField.snp.makeConstraints { (make) in
            make.top.equalTo( Device.topBarHeight + self.style.phoneNumber.marginTop )
            make.left.right.equalTo( self ).inset( self.style.phoneNumber.marginX )
            make.height.equalTo( self.style.phoneNumber.height )
        }
        
        self.nameFieldsBackgroundView.snp.makeConstraints { (make) in
            make.left.right.equalTo( self ).inset( self.style.fields.backgroundMarginX )
            make.top.equalTo( self.phoneNumberField.snp.bottom ).offset( self.style.fields.backgroundMarginTop )
            make.bottom.equalTo( self.lastNameField.snp.bottom ).offset( self.style.fields.marginY )
        }
        
        self.firstNameField.snp.makeConstraints { (make) in
            make.left.right.equalTo( self.nameFieldsBackgroundView ).inset( self.style.fields.marginX )
            make.top.equalTo( self.nameFieldsBackgroundView ).inset( self.style.fields.marginY )
            make.height.equalTo( self.style.fields.height )
        }
        
        self.lastNameField.snp.makeConstraints { (make) in
            make.left.right.equalTo( self.nameFieldsBackgroundView ).inset( self.style.fields.marginX )
            make.top.equalTo( self.firstNameField.snp.bottom ).offset( self.style.fields.marginY )
            make.height.equalTo( self.style.fields.height )
        }
        
        self.fieldSeparator.snp.makeConstraints { (make) in
            make.left.right.equalTo( self.lastNameField )
            make.centerY.equalTo( self.nameFieldsBackgroundView )
            make.height.equalTo( self.style.fields.separatorHeight )
        }
    }
    
    private func setConstraintsForSignUpButton(keyboardHeight: CGFloat) {
        self.signUpButton.snp.makeConstraints { (make) in
            make.bottom.equalTo( self ).inset( keyboardHeight )
            make.left.right.equalTo( self )
            make.height.equalTo( self.style.signUpButton.height )
        }
    }
    
    private func setGestureRecognizers() {
        self.firstNameField.addTarget(self,
                                      action: #selector(self.fieldValueChangedAction),
                                      for: .editingChanged)
        
        self.lastNameField.addTarget(self,
                                     action: #selector(self.fieldValueChangedAction),
                                     for: .editingChanged)
        
        self.registerForKeyboardNotifications()
        
        self.signUpButton.addTarget(self,
                                    action: #selector(self.signUpAction),
                                    for: .touchUpInside)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        self.signUpButton.show()
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.setConstraintsForSignUpButton(keyboardHeight: keyboardRectangle.height)
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        var animationDuration = CGFloat(0)
        
        if let keyboardHideAnimationLength = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? CGFloat {
            animationDuration = keyboardHideAnimationLength
        }
        
        UIView.animate(withDuration: Double(animationDuration)) {
            self.signUpButton.hide()
        }
    }
    
    @objc private func fieldValueChangedAction() {
        self.viewDelegate?.formContentChanged()
    }
    
    @objc private func signUpAction() {
        self.viewDelegate?.signUp()
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.firstNameField) {
            self.lastNameField.becomeFirstResponder()
        }
        else if (textField == self.lastNameField) {
            self.viewDelegate?.signUp()
        }
        
        return false
    }
}

extension SignUpView: SignUpViewModel {
    var isSignUpButtonEnabled: Bool {
        get { return self.signUpButton.status == .enabled }
        set { self.signUpButton.status = StatefulTextButton.Status(rawValue: Int(newValue))! }
    }
    
    var phoneNumber: PhoneNumber? {
        return self.phoneNumberField.phoneNumber
    }
    
    var firstName: String? {
        return self.firstNameField.text
    }
    
    var lastName: String? {
        return self.lastNameField.text
    }
    
    func showKeyboard() {
        self.phoneNumberField.becomeFirstResponder()
    }
}

extension SignUpView: NumberSequenceFieldDelegate {
    func numberSequenceFieldContentChanged() {
        self.fieldValueChangedAction()
    }
}

extension SignUpView {
    struct Style {
        let signUpButton = SignUpButton()
        let phoneNumber = PhoneNumber()
        let fields = Fields()
        
        struct PhoneNumber {
            let marginTop = CGFloat(14)
            let marginX = 10
            
            let height = 100
        }
        
        struct SignUpButton {
            static let baseText = StyledString("SignUp.signUpButtonText".localized, .regular, 20, .center, .white)
            
            let height = 50
            
            let styles: [StatefulTextButton.Status: StatefulTextButton.Style] = [
                .disabled: .style(baseText, .lightGray),
                .enabled: .style(baseText, UIColor(hexString: "#3ccca0")!)
            ]
        }
        
        struct Fields {
            let separatorHeight = 1
            let separatorColor = UIColor.lightGray
            
            let backgroundColor = UIColor.white
            let backgroundCornerRadius = CGFloat(12)
            let backgroundMarginX = 10
            let backgroundMarginTop = 14
            
            let marginY = 10
            let marginX = 10
            let spacing = 10
            let height = 44
            
            let textStyle = StringStyle(.medium, 18, .left, .black)
        }
    }
}
