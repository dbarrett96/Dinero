//
//  SignUpController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

class SignUpController: Controller<SignUpViewModel, SignUpViewDelegate, SignUpView> {
    override var navbarTitle: String? {
        return "SignUp.navbarTitle".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.showKeyboard()
    }
}

extension SignUpController {
    func segueToSignInConfirmation(secret: String) {
        self.navigationController?.pushViewController(SignInPinController(secret: secret), animated: true)
    }
}

extension SignUpController: SignUpViewDelegate {
    var isFormReadyForSubmission: Bool {
        guard let _ = self.viewModel.phoneNumber,
              let firstName = self.viewModel.firstName,
              let lastName = self.viewModel.lastName
        else { return false }
        
        return !firstName.isEmpty && !lastName.isEmpty
    }
    
    func formContentChanged() {
        self.viewModel.isSignUpButtonEnabled = self.isFormReadyForSubmission
    }
    
    func signUp() {
        guard self.isFormReadyForSubmission else { return }
        
        let request = SignUpRequest(firstName: self.viewModel.firstName!,
                                    lastName: self.viewModel.lastName!,
                                    phoneNumber: self.viewModel.phoneNumber!)
        
        self.showLoading()
        
        request.send().then { secret -> Void in
            self.dismiss(animated: true) {
                self.segueToSignInConfirmation(secret: secret.secret)
            }
        }.catch { error in
            self.dismiss(animated: true) {
                self.present(alert: error.alert)
            }
        }
    }
}
