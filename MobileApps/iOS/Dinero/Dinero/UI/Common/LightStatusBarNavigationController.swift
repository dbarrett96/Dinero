//
//  LightStatusBarNavigationController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

/// Navigation controller with a light status bar.
class LightStatusBarNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
