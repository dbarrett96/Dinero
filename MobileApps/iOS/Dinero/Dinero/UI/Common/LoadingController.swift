//
//  LoadingController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit
import WebKit

class ModalController : UIViewController{
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LoadingController : ModalController {
    class View: UIView {
        init() {
            super.init(frame: .zero)
            
            self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            
            let loadingGifUrl = Bundle.main.url(forResource: "loading", withExtension: "gif")!
            let webView = WKWebView()
            
            webView.isOpaque = false
            webView.backgroundColor = .clear
            webView.scrollView.backgroundColor = .clear
            webView.load( URLRequest(url: loadingGifUrl) )
            
            self.addSubview( webView )
            
            webView.snp.makeConstraints { (make) in
                make.center.equalTo(self)
                make.size.equalTo(UIScreen.main.bounds.height * 0.10)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = View()
    }
}


