//
//  LocalPhoneNumberField.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-29.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

class LocalPhoneNumberField: NumberSequenceField {
    // MARK: - Properties
    var localPhoneNumber: LocalPhoneNumber? {
        return LocalPhoneNumber(self.sequence ?? [],
                                from: self.pattern)
    }
}
