//
//  NumberSequenceField.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//
import UIKit

protocol NumberSequenceFieldDelegate: AnyObject {
    func numberSequenceFieldContentChanged()
}

class NumberSequenceField: UIView {
    private let style = Style()
    
    // MARK: - Instance Variables
    weak var delegate: NumberSequenceFieldDelegate?
    let pattern: [Int]
    private var currentIndex = 0
    
    // MARK: - Properties
    var sequence: [Int]? {
        let seq = self.captureField.text?.compactMap { Int(String($0)) }
        
        guard seq != nil, seq!.count == pattern.reduce(0, +) else { return nil }
        
        return seq
    }
    
    // MARK: - Subviews
    let captureField = UITextField()
    let digits: [UILabel]
    
    // MARK: - init
    init(pattern: [Int]) {
        let totalDigits = pattern.reduce(0, +)
        
        self.pattern = pattern
        self.digits = (0..<totalDigits).map { _ in UILabel() }
        
        super.init(frame: .zero)
        
        self.captureField.keyboardType = .numberPad
        
        for digit in digits {
            digit.load(stringStyle: self.style.digits.textStyle)
            digit.addBorder(on: [.bottom],
                            withColor: self.style.digits.borderColor,
                            andWidth: self.style.digits.borderWidth)
            
            self.addSubview( digit )
        }
        
        self.addSubview( self.captureField )
        
        self.setConstraints()
        self.setGestureRecognizers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    func setDigitStyle(_ stringStyle: StringStyle) {
        self.digits.forEach { $0.load(stringStyle: stringStyle) }
    }
    
    private func setConstraints() {
        var previousDigit: UILabel!
        var overallIndex = 0
        
        for groupingSize in self.pattern {
            for groupIndex in 0..<groupingSize {
                let digit = digits[overallIndex]
                
                digit.snp.makeConstraints { (make) in
                    if overallIndex == 0 {
                        make.left.equalTo( self )
                    }
                    else if groupIndex == 0 {
                        make.left.equalTo(previousDigit.snp.right).offset(self.style.digits.largeSpace)
                    }
                    else {
                        make.left.equalTo(previousDigit.snp.right).offset(self.style.digits.smallSpace)
                    }
                    
                    if overallIndex == self.digits.indices.last {
                        make.right.equalTo( self )
                    }
                    
                    make.top.bottom.equalTo( self )
                    
                    if previousDigit != nil {
                        make.width.equalTo( previousDigit )
                    }
                }
                
                previousDigit = digit
                overallIndex += 1
            }
        }
    }
    
    // MARK: - Methods
    private func setGestureRecognizers() {
        self.captureField.addTarget(self,
                                    action: #selector(self.captureFieldChangeAction),
                                    for: .editingChanged)
        
        let selfTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.selfTapAction))
        self.addGestureRecognizer(selfTapGesture)
        self.isUserInteractionEnabled = true
    }
    
    @objc private func captureFieldChangeAction() {
        guard let captureFieldText = self.captureField.text else { return }
        
        let phoneNumberLength = pattern.reduce(0, +)
        
        if captureFieldText.count > phoneNumberLength {
            self.captureField.text = String(captureFieldText.dropLast(captureFieldText.count - phoneNumberLength))
        }
        
        for digit in self.digits {
            digit.text = ""
        }
        
        for (i, text) in self.captureField.text!.enumerated() {
            self.digits[i].text = String(text)
        }
        
        self.delegate?.numberSequenceFieldContentChanged()
    }
    
    @objc private func selfTapAction() {
        self.becomeFirstResponder()
    }
    
    @discardableResult
    override func becomeFirstResponder() -> Bool {
        DispatchQueue.main.async {
            self.captureField.becomeFirstResponder()
        }
        
        return true
    }
}

extension NumberSequenceField {
    struct Style {
        let digits = Digits()
        
        struct Digits {
            let textStyle = StringStyle(.medium, 22, .center, .black)
            
            let smallSpace = 4
            let largeSpace = 14
            
            let borderColor = UIColor.lightGray
            let borderWidth = CGFloat(1)
        }
    }
}
