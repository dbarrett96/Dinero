//
//  PhoneNumberField.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class PhoneNumberField: UIView {
    private let style = Style()
    
    // MARK: - Properties
    var phoneNumber: PhoneNumber? {
        guard let phoneNumber = self.phoneNumberField.localPhoneNumber else {
            return nil
        }
        
        return PhoneNumber(countryCode: .canada, phoneNumber)
    }
    
    // MARK: - Instance Variables
    var delegate: NumberSequenceFieldDelegate? {
        get { return self.phoneNumberField.delegate }
        set { self.phoneNumberField.delegate = newValue }
    }
    
    // MARK: - Subviews
    private lazy var countrySelector = UIImageView(image: #imageLiteral(resourceName: "IconFlagCanada"))
    private lazy var phoneNumberField = LocalPhoneNumberField(pattern: [3,3,4])
    
    // MARK: - init
    init() {
        super.init(frame: .zero)
        
        self.isUserInteractionEnabled = true
        
        self.backgroundColor = self.style.backgroundColor
        self.layer.cornerRadius = self.style.cornerRadius
        self.addShadow()

        self.countrySelector.layer.cornerRadius = self.style.countrySelector.cornerRadius
        self.countrySelector.addShadow()
        
        self.addSubview( self.countrySelector )
        self.addSubview( self.phoneNumberField )
        
        self.setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    private func setConstraints() {
        self.countrySelector.snp.makeConstraints { (make) in
            make.left.equalTo( self ).inset( self.style.countrySelector.inset )
            make.size.equalTo( self.style.countrySelector.size )
            make.centerY.equalTo( self )
        }
        
        self.phoneNumberField.snp.makeConstraints { (make) in
            make.left.equalTo( self.countrySelector.snp.right ).offset(self.style.phoneNumber.marginX)
            make.right.equalTo( self ).inset( self.style.phoneNumber.marginX )
            make.top.bottom.equalTo( self.countrySelector )
        }
    }
    
    @discardableResult
    override func becomeFirstResponder() -> Bool {
        return self.phoneNumberField.becomeFirstResponder()
    }
}

extension PhoneNumberField {
    struct Style {
        let backgroundColor = UIColor.white
        let cornerRadius = CGFloat(12)
        
        let countrySelector = CountrySelector()
        let phoneNumber = PhoneNumber()
        
        struct CountrySelector {
            let inset = 16
            let size = 44
            
            let cornerRadius = CGFloat(4)
        }
        
        struct PhoneNumber {
            let marginX = 14
        }
    }
}
