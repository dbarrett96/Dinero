//
//  PinField.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class PinField: UIView {
    // MARK: - Properties
    var pin: [Int]? {
        return self.field.sequence
    }
    
    // MARK: - Instance Variables
    var delegate: NumberSequenceFieldDelegate? {
        get { return self.field.delegate }
        set { self.field.delegate = newValue }
    }
    
    // MARK: - Subviews
    private var field: NumberSequenceField
    
    // MARK: - init
    init(length: Int) {
        self.field = NumberSequenceField(pattern: [length])
        
        super.init(frame: .zero)
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 4
        self.addShadow()
        
        self.field.setDigitStyle(StringStyle(.medium, 36, .center, .black))
        
        self.addSubview( field )
        self.setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
    @discardableResult
    override func becomeFirstResponder() -> Bool {
        return self.field.becomeFirstResponder()
    }
    
    private func setConstraints() {
        self.field.snp.makeConstraints { (make) in
            make.left.right.equalTo( self ).inset( 10 )
            make.height.equalTo( self ).multipliedBy( 0.6 )
            make.centerY.equalTo( self )
        }
    }
}
