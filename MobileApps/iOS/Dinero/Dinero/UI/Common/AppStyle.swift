//
//  UI.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

struct AppStyle {
    struct Colors {
        static let landingGradient = [
            UIColor(hexString: "#266fc0")!,
            UIColor(hexString: "#084181")!,
        ]
    }
}
