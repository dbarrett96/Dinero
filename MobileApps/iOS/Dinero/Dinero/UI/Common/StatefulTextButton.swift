//
//  StatefulButton.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class StatefulTextButton: UIButton {
    // MARK: - Inner Types
    struct Style {
        var text: StyledString
        var backgroundColor: UIColor
        
        static func style(_ text: StyledString, _ backgroundColor: UIColor) -> Style {
            return Style(text: text, backgroundColor: backgroundColor)
        }
    }
    
    enum Status: Int {
        case disabled
        case enabled
    }
    
    // MARK: - Properties
    var status: Status {
        get { return self.isUserInteractionEnabled ? .enabled : .disabled }
        set {
            let stateStyle = styles[newValue]!
            
            self.backgroundColor = stateStyle.backgroundColor
            self.load(styledString: stateStyle.text)
            self.isUserInteractionEnabled = Bool(newValue.rawValue)
        }
    }
    
    // MARK: - Instance Variables
    var styles: [Status: Style] = [:]
}


