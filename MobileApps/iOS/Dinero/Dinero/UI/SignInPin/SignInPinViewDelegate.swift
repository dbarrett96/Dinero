//
//  SignInPinViewDelegate.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

protocol SignInPinViewDelegate: NumberSequenceFieldDelegate {
    func signIn()
}
