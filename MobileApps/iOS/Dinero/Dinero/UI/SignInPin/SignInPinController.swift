//
//  SignInPinController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class SignInPinController: Controller<SignInPinViewModel, SignInPinViewDelegate, SignInPinView> {
    // MARK: - Properties
    override var navbarTitle: String? {
        return "SignInPin.navbarTitle".localized
    }
    
    // MARK: - Instance Variables
    private let signInSecret: String
    
    // MARK: - init
    init(secret: String) {
        self.signInSecret = secret
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.showKeyboard()
    }
}

extension SignInPinController {
    func segueToAppHome() {
        UIApplication.setRoot( HomeController() )
    }
}

extension SignInPinController: SignInPinViewDelegate {
    func numberSequenceFieldContentChanged() {
        self.viewModel.isSignInButtonEnabled = self.viewModel.pin != nil
    }
    
    func signIn() {
        self.showLoading()
        
        TokenRequest(secret: self.signInSecret,
                     pin: self.viewModel.pin!).send().then
        { token -> Void in
            Session.current.cache.accessToken = token.access_token
            Session.current.cache.refreshToken = token.refresh_token
            Session.current.state = .authenticated
            
            self.dismiss(animated: true) {
                self.segueToAppHome()
            }
        }.catch { error in
            self.dismiss(animated: false) {
                self.present(alert: error.alert)
            }
        }
    }
}
