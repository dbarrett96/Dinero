//
//  SignInPinView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation
import UIKit

class SignInPinView: View<SignInPinViewDelegate> {
    private let style = Style()
    
    // MARK: - Instance Variables
    override var viewDelegate: SignInPinViewDelegate? {
        get { return super.viewDelegate }
        set {
            super.viewDelegate = newValue
            self.pinField.delegate = newValue
        }
    }
    
    // MARK: - Subviews
    private lazy var headlineLabel = UILabel(self.style.headline.text)
    private lazy var pinField = PinField(length: 6)
    private lazy var signInButton = StatefulTextButton(self.style.signInButton.styles[.disabled]!.text)
    
    // MARK: - Methods
    override func createView() {
        self.addGradientBackground(colors: AppStyle.Colors.landingGradient,
                                   isFullscreen: true)
    
        
        self.signInButton.styles = self.style.signInButton.styles
        self.signInButton.status = .disabled
        
        self.headlineLabel.adjustsFontSizeToFitWidth = true
        
        self.addSubview( self.pinField )
        self.addSubview( self.signInButton )
        self.addSubview( self.headlineLabel )
        
        self.setConstraints()
        self.setGestureRecognizers()
    }
    
    // MARK: - Methods
    private func setConstraints() {
        self.pinField.snp.makeConstraints { (make) in
            make.left.right.equalTo( self ).inset( 10 )
            make.top.equalTo( Device.topBarHeight + 14 )
            make.height.equalTo( 100 )
        }
        
        self.headlineLabel.snp.makeConstraints { (make) in
            make.top.equalTo( self.pinField.snp.bottom ).offset(self.style.headline.marginTop)
            make.left.right.equalTo( self.pinField ).inset( self.style.headline.marginX )
        }
    }
    
    private func setConstraintsForSignInButton(keyboardHeight: CGFloat) {
        self.signInButton.snp.makeConstraints { (make) in
            make.bottom.equalTo( self ).inset( keyboardHeight )
            make.left.right.equalTo( self )
            make.height.equalTo( self.style.signInButton.height )
        }
    }
    
    private func setGestureRecognizers() {
        // Register for keyboard show notification.
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
        
        self.signInButton.addTarget(self,
                                    action: #selector(self.signInAction),
                                    for: .touchUpInside)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        self.signInButton.show()
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.setConstraintsForSignInButton(keyboardHeight: keyboardRectangle.height)
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        var animationDuration = CGFloat(0)
        
        if let keyboardHideAnimationLength = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? CGFloat {
            animationDuration = keyboardHideAnimationLength
        }
        
        UIView.animate(withDuration: Double(animationDuration)) {
            self.signInButton.hide()
        }
    }
    
    @objc private func signInAction() {
        self.viewDelegate?.signIn()
    }
}

extension SignInPinView : SignInPinViewModel{
    var isSignInButtonEnabled: Bool {
        get { return self.signInButton.status == .enabled }
        set { self.signInButton.status = StatefulTextButton.Status(rawValue: Int(newValue))! }
    }
    
    var pin: [Int]? {
        return self.pinField.pin
    }
    
    func showKeyboard() {
        self.pinField.becomeFirstResponder()
    }
}

extension SignInPinView {
    private struct Style {
        let headline = Headline()
        let pin = Pin()
        let signInButton = SignInButton()
        
        struct Headline {
            let text = StyledString("SignInPin.mainText".localized, .medium, 12, .left, UIColor.white.withAlphaComponent(0.7))
            
            let marginTop = 7
            let marginX = 4
        }
        
        struct Pin {
            let marginTop = 100
            let marginX = 10
            
            let height = 100
        }
        
        struct SignInButton {
            static let baseText = StyledString("SignInPin.signInButtonText".localized, .regular, 20, .center, .white)
            
            let height = 50
            
            let styles: [StatefulTextButton.Status: StatefulTextButton.Style] = [
                .disabled: .style(baseText, .lightGray),
                .enabled: .style(baseText, UIColor(hexString: "#3ccca0")!)
            ]
        }
    }
}
