//
//  SignInPinViewModel.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

protocol SignInPinViewModel {
    // MARK: - Instance Variables
    var isSignInButtonEnabled: Bool { get set }
    
    // MARK: - Properties
    var pin: [Int]? { get }
    
    // MARK: - Methods
    func showKeyboard()
}
