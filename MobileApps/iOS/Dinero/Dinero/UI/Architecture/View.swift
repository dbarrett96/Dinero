//
//  View.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import SnapKit
import UIKit

class View<Delegate>: UIView {
    private weak var _viewDelegate: AnyObject?
    var viewDelegate: Delegate? {
        get { return self._viewDelegate as? Delegate }
        set { self._viewDelegate = newValue as AnyObject}
    }
    
    func createView() {
        // HOOK
    }
}
