//
//  Controller.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class Controller<ViewModel, ViewDelegate, ViewT: View<ViewDelegate>>: UIViewController {
    // MARK: - Properties
    var isNavigationBarHidden: Bool {
        return false
    }
    
    var navbarTitle: String? {
        return nil
    }
    
    var navbarTitleColor: UIColor {
        return .white
    }
    
    var navbarColor: UIColor? {
        return nil
    }
    
    var rightBarButton: UIBarButtonItem? {
        return nil
    }
    
    var leftBarButton: UIBarButtonItem? {
        return nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var viewModel: ViewModel {
        get { return self.view as! ViewModel }
        set { return }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let theView = ViewT()
        theView.viewDelegate = self as? ViewDelegate
        theView.createView()
        
        self.view = theView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.layoutNavigationBar()
    }
    
    // MARK: - Methods
    private func layoutNavigationBar() {
        self.navigationController?.isNavigationBarHidden = self.isNavigationBarHidden
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = .white
        
        self.navigationItem.rightBarButtonItem = self.rightBarButton
        self.navigationItem.leftBarButtonItem = self.leftBarButton
        
        // Make navbar colored or transparent.
        if self.navbarColor != nil {
            self.navigationController?.navigationBar.barTintColor = self.navbarColor
        }
        else {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
        }
        
        // Set navbar title.
        self.navigationItem.title = self.navbarTitle
        
        // Set navbar title color and font size.
        self.navigationController?.navigationBar.titleTextAttributes = [
            .foregroundColor: self.navbarTitleColor,
            .font: Font.regular.with(size: 18)
        ]
    }
}
