//
//  SignInViewModel.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

protocol SignInViewModel {
    // MARK: - Properties
    var phoneNumber: PhoneNumber? { get }
    
    // MARK: - Instance Variables
    var viewDelegate: SignInViewDelegate? { get set }
    var isSignInButtonEnabled: Bool { get set }
    
    // MARK: - Methods
    func showKeyboard()
}
