//
//  SignInView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class SignInView: View<SignInViewDelegate> {
    private let style = Style()
    
    // MARK : - Instance Variables
    override var viewDelegate: SignInViewDelegate? {
        get { return super.viewDelegate }
        set {
            super.viewDelegate = newValue
            self.phoneNumberField.delegate = newValue
        }
    }
    
    // MARK: - Subviews
    private lazy var headlineLabel = UILabel(self.style.headline.text)
    private lazy var phoneNumberField = PhoneNumberField()
    private lazy var signInButton = StatefulTextButton(self.style.signInButton.styles[.disabled]!.text)
    
    // MARK: - Methods
    override func createView() {
        self.addGradientBackground(colors: AppStyle.Colors.landingGradient,
                                   isFullscreen: true)
        
        self.headlineLabel.adjustsFontSizeToFitWidth = true
        
        self.signInButton.styles = self.style.signInButton.styles
        self.signInButton.status = .disabled
        
        self.addSubview( self.phoneNumberField )
        self.addSubview( self.headlineLabel )
        self.addSubview( self.signInButton )
        
        self.setConstraints()
        self.setGestureRecognizers()
    }
    
    private func setConstraints() {
        self.phoneNumberField.snp.makeConstraints { (make) in
            make.top.equalTo( Device.topBarHeight + self.style.phoneNumber.marginTop )
            make.left.right.equalTo( self ).inset( self.style.phoneNumber.marginX )
            make.height.equalTo( self.style.phoneNumber.height )
        }
        
        self.headlineLabel.snp.makeConstraints { (make) in
            make.top.equalTo( self.phoneNumberField.snp.bottom ).offset(self.style.headline.marginTop)
            make.left.right.equalTo( self.phoneNumberField ).inset( self.style.headline.marginX )
        }
    }
    
    private func setConstraintsForSignInButton(keyboardHeight: CGFloat) {
        self.signInButton.snp.makeConstraints { (make) in
            make.bottom.equalTo( self ).inset( keyboardHeight )
            make.left.right.equalTo( self )
            make.height.equalTo( self.style.signInButton.height )
        }
    }
    
    private func setGestureRecognizers() {
        // Register for keyboard show notification.
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillHide),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
        
        self.signInButton.addTarget(self,
                                    action: #selector(self.signInAction),
                                    for: .touchUpInside)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        self.signInButton.show()
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.setConstraintsForSignInButton(keyboardHeight: keyboardRectangle.height)
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        var animationDuration = CGFloat(0)
        
        if let keyboardHideAnimationLength = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? CGFloat {
            animationDuration = keyboardHideAnimationLength
        }
        
        UIView.animate(withDuration: Double(animationDuration)) {
            self.signInButton.hide()
        }
    }
    
    @objc private func signInAction() {
        self.viewDelegate?.signIn()
    }
}

extension SignInView: SignInViewModel {
    var isSignInButtonEnabled: Bool {
        get { return self.signInButton.status == .enabled }
        set { self.signInButton.status = StatefulTextButton.Status(rawValue: Int(newValue))! }
    }
    
    var phoneNumber: PhoneNumber? {
        return self.phoneNumberField.phoneNumber
    }
    
    func showKeyboard() {
        self.phoneNumberField.becomeFirstResponder()
    }
}

extension SignInView {
    private struct Style {
        let headline = Headline()
        let phoneNumber = PhoneNumber()
        let signInButton = SignInButton()
        
        struct Headline {
            let text = StyledString("SignIn.mainText".localized, .medium, 12, .left, UIColor.white.withAlphaComponent(0.7))
            
            let marginTop = 7
            let marginX = 4
        }
        
        struct PhoneNumber {
            let marginTop = CGFloat(14)
            let marginX = 10
        
            let height = 100
        }
        
        struct SignInButton {
            static let baseText = StyledString("SignIn.signInButtonText".localized, .regular, 20, .center, .white)

            let height = 50
            
            let styles: [StatefulTextButton.Status: StatefulTextButton.Style] = [
                .disabled: .style(baseText, .lightGray),
                .enabled: .style(baseText, UIColor(hexString: "#3ccca0")!)
            ]
        }
    }
}
