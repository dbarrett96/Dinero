//
//  SignInController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

class SignInController: Controller<SignInViewModel, SignInViewDelegate, SignInView> {
    override var navbarTitle: String? {
        return "SignIn.navbarTitle".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.showKeyboard()
    }
}

extension SignInController {
    func segueToSignInConfirmation(secret: String) {
        self.navigationController?.pushViewController(SignInPinController(secret: secret), animated: true)
    }
}

extension SignInController: SignInViewDelegate {
    func numberSequenceFieldContentChanged() {
        self.viewModel.isSignInButtonEnabled = self.viewModel.phoneNumber != nil
    }
    
    func signIn() {
        self.showLoading()
        
        SignInRequest(self.viewModel.phoneNumber!).send().then { secret -> Void in
            self.dismiss(animated: true) {
                self.segueToSignInConfirmation(secret: secret.secret)
            }
        }.catch { error in
            self.dismiss(animated: true) {
                self.present(alert: error.alert)
            }
        }
    }
}
