//
//  SignInViewDelegate.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

protocol SignInViewDelegate: NumberSequenceFieldDelegate {
    func signIn()
}
