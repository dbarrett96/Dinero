//
//  DiningSessionController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-10.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

class DiningSessionController: LightStatusBarNavigationController {
    convenience init() {
        self.init(rootViewController: MenuController())
    }
}
