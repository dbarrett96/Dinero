//
//  QrScanBorderView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-06.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

/// Frames the corners of a QR Code.
class QrScanBorderView: UIView {
    static var segmentSizeRatio = 0.1
    
    // MARK: - Subviews.
    private let verticalSegments = [
        UIView(),
        UIView(),
        UIView(),
        UIView()
    ]
    
    private let horizontalSegments = [
        UIView(),
        UIView(),
        UIView(),
        UIView()
    ]
    
    private var segments: [UIView] {
        return self.verticalSegments + self.horizontalSegments
    }
    
    // MARK: - Instance Variables.
    var width: CGFloat {
        get { return self.horizontalSegments[0].frame.size.height }
        set { self.horizontalSegments[0].snp.updateConstraints { (make) in
            make.height.equalTo( newValue )
            }
        }
    }
    
    var color: UIColor {
        get { return self.horizontalSegments[0].backgroundColor! }
        set { self.segments.forEach{ $0.backgroundColor = newValue } }
    }
    
    var cornerRadius: CGFloat {
        get { return self.horizontalSegments[0].layer.cornerRadius }
        set { self.segments.forEach { $0.layer.cornerRadius = newValue } }
    }

    // MARK: - Init.
    init(color: UIColor, width: CGFloat) {
        super.init(frame: .zero)
        
        self.color = color
        self.cornerRadius = 2
        
        segments.forEach { self.addSubview( $0 ) }
        self.setConstraints(with: width)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods.
    private func setConstraints(with thickness: CGFloat) {
        self.horizontalSegments[0].snp.makeConstraints { (make) in
            make.top.left.equalTo( self )
            make.width.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
            make.height.equalTo( thickness )
        }
        
        self.horizontalSegments[1].snp.makeConstraints { (make) in
            make.top.right.equalTo( self )
            make.width.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
            make.height.equalTo( self.horizontalSegments[0] )
        }
        
        self.horizontalSegments[2].snp.makeConstraints { (make) in
            make.bottom.right.equalTo( self )
            make.width.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
            make.height.equalTo( self.horizontalSegments[0] )
        }
        
        self.horizontalSegments[3].snp.makeConstraints { (make) in
            make.bottom.left.equalTo( self )
            make.width.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
            make.height.equalTo( self.horizontalSegments[0] )
        }
        
        self.verticalSegments[0].snp.makeConstraints { (make) in
            make.top.left.equalTo( self )
            make.width.equalTo( self.horizontalSegments[0].snp.height )
            make.height.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
        }
        
        self.verticalSegments[1].snp.makeConstraints { (make) in
            make.top.right.equalTo( self )
            make.width.equalTo( self.horizontalSegments[0].snp.height )
            make.height.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
        }
        
        self.verticalSegments[2].snp.makeConstraints { (make) in
            make.bottom.right.equalTo( self )
            make.width.equalTo( self.horizontalSegments[0].snp.height )
            make.height.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
        }
        
        self.verticalSegments[3].snp.makeConstraints { (make) in
            make.bottom.left.equalTo( self )
            make.width.equalTo( self.horizontalSegments[0].snp.height )
            make.height.equalTo( self ).multipliedBy( QrScanBorderView.segmentSizeRatio )
        }
    }
}
