//
//  HomeController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class HomeController: Controller<HomeViewModel, HomeViewDelegate, HomeView> {
    // MARK: - Instance Variables
    private var seenInvalidQrCodes = [String]()
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.startQrScan()
    }
    
    func segueToDiningSession() {
        let menu = UIViewController()
        
        // TEMP
        menu.view.backgroundColor = .red
        
        let diningSessionRoot = LightStatusBarNavigationController(rootViewController: menu)
        UIApplication.shared.keyWindow?.rootViewController = diningSessionRoot
    }
}

extension HomeController: HomeViewDelegate {
    func didScanQrCode(string qrString: String) {
        guard !self.seenInvalidQrCodes.contains(qrString) else { return }
        
        self.viewModel.endQrScan()
        self.showLoading()
        
        RequestSeatRequest(tableId: qrString).send().then { response -> Void in
            if response.state == .accepted {
                Session.current.state = .dining
                
                self.dismiss(animated: true) {
                    self.segueToDiningSession()
                }
            }
        }.catch { error in
            self.dismiss(animated: false) {
                self.seenInvalidQrCodes.append(qrString)
                
                var alert = error.alert
                alert.buttons[0].action = { _ in self.viewModel.startQrScan() }
                
                self.present( alert: alert )
            }
        }
    }
}
