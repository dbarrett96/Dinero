//
//  HomeView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import AVKit
import Foundation

class HomeView: View<HomeViewDelegate>, AVCaptureMetadataOutputObjectsDelegate {
    private let style = Style()
    
    // MARK: - Instance Variables.
    private lazy var captureSession = AVCaptureSession()
    private var previewLayer: AVCaptureVideoPreviewLayer!
    
    // MARK: - Subviews.
    private lazy var menuButton = UIButton()
    private lazy var qrBorderView = QrScanBorderView(color: self.style.qrBorder.color,
                                                     width: self.style.qrBorder.width)
    
    // MARK: - Methods.
    override func createView() {
        self.menuButton.setImage(#imageLiteral(resourceName: "IconHamburgerMenu"), for: .normal)
        
        self.setupQrCaptureSession()
        
        self.addSubview(self.menuButton)
        self.addSubview(self.qrBorderView)
        
        self.setConstraints()
        self.setGestureRecognizers()
    }
    
    private func setConstraints() {
        self.qrBorderView.snp.makeConstraints { (make) in
            make.center.equalTo( self )
            make.size.equalTo( self.style.qrBorder.size )
        }
        
        self.menuButton.snp.makeConstraints { (make) in
            make.bottom.equalTo( self ).inset( self.style.menuButton.marginBottom )
            make.right.equalTo( self ).inset( self.style.menuButton.marginRight )
            make.size.equalTo( self.style.menuButton.size)
        }
    }
    
    private func setGestureRecognizers() {
        self.menuButton.addTarget(self,
                                  action: #selector(self.menuAction),
                                  for: .touchUpInside)
    }
    
    private func setupQrCaptureSession() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video),
            let videoInput = try? AVCaptureDeviceInput(device: videoCaptureDevice),
            self.captureSession.canAddInput(videoInput)
        else { return }
        
        self.captureSession.addInput(videoInput)
        
        var captureFrame = CGRect()
        captureFrame.origin = CGPoint(x: Device.screen.size.width/2 - self.style.qrBorder.size/2,
                                      y: Device.screen.size.height/2 - (self.style.qrBorder.size/2))
        captureFrame.size = .square(self.style.qrBorder.size)
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        self.captureSession.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [.qr]
//        captureMetadataOutput.rectOfInterest = captureFrame
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer.frame = Device.screen
        self.previewLayer.videoGravity = .resizeAspectFill
        self.layer.addSublayer(previewLayer)
    }
    
    @objc private func menuAction() {
        print("Menu")
    }
    
    // AVCaptureMetadataOutputObjectsDelegate
    func metadataOutput(
        _ output: AVCaptureMetadataOutput,
        didOutput metadataObjects: [AVMetadataObject],
        from connection: AVCaptureConnection)
    {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr,
            let stringValue = metadataObj.stringValue
        {
            self.viewDelegate?.didScanQrCode(string: stringValue)
        }
    }
}

extension HomeView: HomeViewModel {
    func startQrScan() {
        self.captureSession.startRunning()
    }
    
    func endQrScan() {
        self.captureSession.stopRunning()
    }
}

extension HomeView {
    private struct Style {
        let qrBorder = QrBorder()
        let menuButton = MenuButton()
        
        struct QrBorder {
            let size = CGFloat(220)
            let width = CGFloat(4)
            let color = UIColor.white.withAlphaComponent(1)
        }
        
        struct MenuButton {
            let size = 44
            let marginRight = 28
            let marginBottom = 28
        }
    }
}
