//
//  LandingController.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

class LandingController: Controller<LandingViewModel, LandingViewDelegate, LandingView> {
    override var isNavigationBarHidden: Bool {
        return true
    }
}

extension LandingController: LandingViewDelegate {
    func signIn() {
        self.navigationController?.pushViewController(SignInController(),
                                                      animated: true)
    }
    
    func signUp() {
        self.navigationController?.pushViewController(SignUpController(),
                                                      animated: true)
    }
}
