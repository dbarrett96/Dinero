//
//  LandingViewDelegate.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

protocol LandingViewDelegate {
    func signIn()
    func signUp()
}
