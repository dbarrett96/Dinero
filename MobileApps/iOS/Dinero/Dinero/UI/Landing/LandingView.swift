//
//  LandingView.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//


import UIKit

class LandingView: View<LandingViewDelegate> {
    private let style = Style()
    
    // MARK: - Subviews
    private lazy var companyLogoImage = UIImageView(image: #imageLiteral(resourceName: "Logo"))
    private lazy var signInButton = UIButton(self.style.signInButton.text)
    private lazy var signUpButton = UIButton(self.style.signUpButton.text)
    private lazy var signUpArrowButton = UIButton(type: .custom)
    
    // MARK: - Constructor
    override func createView() {
        self.addGradientBackground(colors: AppStyle.Colors.landingGradient,
                                   isFullscreen: true)
        
        self.signInButton.backgroundColor = self.style.signInButton.backgroundColor
        self.signInButton.layer.cornerRadius = self.style.signInButton.cornerRadius
        self.signInButton.addShadow()
        
        self.signUpArrowButton.setImage(#imageLiteral(resourceName: "IconArrowRightWhite").withRenderingMode(.alwaysTemplate), for: .normal)
        self.signUpArrowButton.tintColor = UIColor.white.withAlphaComponent(0.6)
        
        self.addSubview( self.companyLogoImage )
        self.addSubview( self.signInButton )
        self.addSubview( self.signUpButton )
        self.addSubview( self.signUpArrowButton )
        
        self.setConstraints()
        self.setGestureRecognizers()
    }
    
    private func setConstraints() {
        self.companyLogoImage.snp.makeConstraints { (make) in
            make.top.equalTo( self ).inset( self.style.logo.marginTop )
            make.centerX.equalTo( self )
            make.size.equalTo( self.style.logo.size )
        }
        
        self.signInButton.snp.makeConstraints { (make) in
            make.bottom.equalTo( self.signUpButton.snp.top ).offset(-self.style.signInButton.marginBottom)
            make.centerX.equalTo( self )
            make.size.equalTo( self.style.signInButton.size )
        }
        
        self.signUpButton.snp.makeConstraints { (make) in
            make.bottom.equalTo( self ).inset( self.style.signUpButton.marginBottom )
            make.centerX.equalTo( self ).inset(
                -(self.style.signUpArrow.size.width/2.0 +
                self.style.signUpArrow.marginLeft/2.0))
        }
        
        self.signUpArrowButton.snp.makeConstraints { (make) in
            make.centerY.equalTo( self.signUpButton )
            make.left.equalTo( self.signUpButton.snp.right ).offset( self.style.signUpArrow.marginLeft )
            make.size.equalTo( self.style.signUpArrow.size )
        }
    }

    private func setGestureRecognizers() {
        self.signUpButton.addTarget(self,
                                    action: #selector(self.signUpAction),
                                    for: .touchUpInside)
        
        self.signUpArrowButton.addTarget(self,
                                         action: #selector(self.signUpAction),
                                         for: .touchUpInside)
        
        self.signInButton.addTarget(self,
                                    action: #selector(self.signInAction),
                                    for: .touchUpInside)
    }
    
    @objc private func signUpAction() {
        self.viewDelegate?.signUp()
    }
    
    @objc private func signInAction() {
        self.viewDelegate?.signIn()
    }
}

extension LandingView: LandingViewModel {
    
}

extension LandingView {
    private struct Style {
        let logo = Logo()
        let signInButton = SignInButton()
        let signUpButton = SignUpButton()
        let signUpArrow = SignUpArrow()
        
        struct Logo {
            let marginTop = 100
            let size = 250
        }
        
        struct SignInButton {
            let text = StyledString("Sign In", .bold, 22, .center, .white)
            let backgroundColor = UIColor(hexString: "#266fc0")!

            
            let size = CGSize(width: 250, height: 46)
            let cornerRadius = CGFloat(4)
            
            let marginBottom = 7
        }
        
        struct SignUpButton {
            let text = StyledString("I don't have an account", .medium, 14, .center, UIColor.white.withAlphaComponent(0.6))
            let marginBottom = 28
        }
        
        struct SignUpArrow {
            let size = CGSize(width: 14, height: 14)
            let marginLeft = CGFloat(4)
        }
    }
}
