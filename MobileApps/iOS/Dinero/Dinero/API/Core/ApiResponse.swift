//
//  ApiResponse.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-31.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation
import ObjectMapper

class ApiResponse: Mappable {
    required init?(map: Map) {
        let mirror = Mirror(reflecting: self)
        
        for child in mirror.children where child.label != nil {
            let childType = String(describing: type(of: child.value))
            
            if childType.starts(with: "ImplicitlyUnwrappedOptional<"),
               map.JSON[child.label!] == nil
            {
                return nil
            }
        }
    }
    
    func mapping(map: Map) {
        
    }
}
