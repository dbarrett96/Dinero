//
//  ApiError.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

enum ApiError: Error, LocalizedError {
    case invalidResponse(String, Int, Int)
    case undecodableResponse
    case noResponse
    
    var errorDescription: String? {
        switch self {
        case .invalidResponse(let message, let errorCode, let statusCode):
            return "[HTTP \(statusCode): \(errorCode)] \(message)"
        case .noResponse:
            return "There was no response to the request."
        case .undecodableResponse:
            return "The response could not be decoded."
        }
    }
}
