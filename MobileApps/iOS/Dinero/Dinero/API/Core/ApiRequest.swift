//
//  ApiRequest.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-31.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Alamofire
import Foundation
import ObjectMapper
import PromiseKit

class ApiRequest<Response: ApiResponse> {
    // MARK: - Properties
    var baseUrl: URL {
        return URL(string: "https://dinero-dev-api.azurewebsites.net/api")!
    }
    
    var path: String! {
        return ""
    }
    
    var completeUrl: URL {
        var url = self.baseUrl
        url.appendPathComponent(String(format: self.path, pathParameters))
        return url
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var encoding: ParameterEncoding {
        return self.method == .put || self.method == .post ?
            JSONEncoding.default : URLEncoding.default
    }
    
    private var request: DataRequest {
        return Alamofire.request(self.completeUrl,
                                 method: self.method,
                                 parameters: self.body,
                                 encoding: self.encoding,
                                 headers: self.headers).validate()
    }
    
    // MARK: - Instance Variables
    var pathParameters: [String]
    var headers: [String: String]
    var body: [String: Any]
    
    // MARK: - Init
    init() {
        self.pathParameters = []
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": Session.current.cache.accessToken == nil ? "" : Session.current.cache.accessToken!
        ]
        self.body = [:]
    }
    
    // MARK: - Methods
    func send() -> Promise<Response> {
        return Promise<Response>() { fulfill, reject in
            self.request.responseJSON { response in
                self.debugPrint( response )

                switch response.result {
                case .failure(let e):
                    if let resp = response.response
                    {
                        let error = ApiError.invalidResponse(
                            e.localizedDescription,
                            -1,
                            resp.statusCode)
                        
                        reject( error )
                    }
                    else {
                        reject(ApiError.noResponse)
                    }
                case .success(_):
                    if let dataResponse = response.data,
                        let stringResponse = String(data: dataResponse, encoding: .ascii),
                        let responseObject = Mapper<Response>().map(JSONString: stringResponse)
                    {
                        fulfill(responseObject)
                    }
                    else {
                        reject(ApiError.undecodableResponse)
                    }
                }
            }
        }
    }
    
    private func debugPrint( _ response: DataResponse<Any> ) {
        print("[\(self.method.rawValue)] \(self.completeUrl)")
        print(headers)
        print(body)
        
        if let responseData = response.data,
           let responseString = String( data: responseData, encoding: .utf8 )
        {
            print(responseString)
        }
        else {
            print("no response")
        }
        
        print()
    }
}
