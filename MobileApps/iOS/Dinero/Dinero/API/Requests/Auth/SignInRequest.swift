//
//  SignInRequest.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-31.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Alamofire
import ObjectMapper

class SignInRequest: ApiRequest<SignInRequest.Response> {
    // MARK: - Properties
    override var path: String! {
        return "signIn"
    }
    
    override var method: HTTPMethod {
        return .post
    }
    
    // MARK: - init
    init(_ phoneNumber: PhoneNumber) {
        super.init()
        self.body = [
            "phoneNumber": phoneNumber.localDescription
        ]
    }
}

extension SignInRequest {
    // MARK: - Response Type
    class Response: ApiResponse {
        var secret: String!
        
        override func mapping(map: Map) {
            secret <- map["secret"]
        }
    }
}
