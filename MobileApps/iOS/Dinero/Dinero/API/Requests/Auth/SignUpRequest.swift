//
//  SignUpRequest.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Alamofire
import ObjectMapper

class SignUpRequest: ApiRequest<SignInRequest.Response> {
    // MARK: - Properties
    override var path: String! {
        return "signUp"
    }
    
    override var method: HTTPMethod {
        return .post
    }
    
    // MARK: - init
    init(firstName: String, lastName: String, phoneNumber: PhoneNumber) {
        super.init()
        self.body = [
            "firstName": firstName,
            "lastName": lastName,
            "phoneNumber": phoneNumber.localDescription
        ]
    }
}
