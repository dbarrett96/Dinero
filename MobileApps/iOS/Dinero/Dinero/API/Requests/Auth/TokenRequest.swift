//
//  TokenRequest.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Alamofire
import ObjectMapper

class TokenRequest: ApiRequest<TokenRequest.Response> {
    // MARK: - Properties
    override var path: String! {
        return "token"
    }
    
    override var method: HTTPMethod {
        return .post
    }

    // MARK: - init
    init(secret: String, pin: [Int]) {
        super.init()
        self.body = [
            "signInSecret": secret,
            "pin": pin.map { String($0) }.reduce("", +)
        ]
    }
}

extension TokenRequest {
    class Response: ApiResponse {
        var access_token: String!
        var refresh_token: String!
        
        override func mapping(map: Map) {
            access_token <- map["access_token"]
            refresh_token <- map["refresh_token"]
        }
    }
}
