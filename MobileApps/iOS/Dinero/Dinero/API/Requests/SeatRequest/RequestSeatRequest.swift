//
//  CreateSeatRequest.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-07.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Alamofire
import ObjectMapper

class RequestSeatRequest: ApiRequest<RequestSeatRequest.Response> {
    // MARK: - Properties
    override var path: String! {
        return "seatRequest"
    }
    
    override var method: HTTPMethod {
        return .post
    }
    
    // MARK: - Init
    init(tableId: String) {
        super.init()
        
        self.body = [
            "table": tableId
        ]
    }
}

extension RequestSeatRequest {
    class Response: ApiResponse {
        var id: String!
        var state: SeatRequestState!
        
        override func mapping(map: Map) {
            id <- map["id"]
            state <- map["state"]
        }
    }
}
