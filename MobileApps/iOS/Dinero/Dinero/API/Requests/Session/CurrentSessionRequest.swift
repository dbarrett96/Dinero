//
//  CurrentSessionRequest.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-09.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import ObjectMapper

class CurrentSessionRequest: ApiRequest<CurrentSessionRequest.Response> {
    // MARK: - Properties
    override var path: String! {
        return "session"
    }
}

extension CurrentSessionRequest {
    class Response: ApiResponse {
        var sessionId: String!
        var restaurantId: String!
        var tableId: String!
        var isPaidFor: Bool!
        var isComplete: Bool!
        
        override func mapping(map: Map) {
            sessionId <- map["sessionId"]
            restaurantId <- map["restaurantId"]
            tableId <- map["tableId"]
            isPaidFor <- map["isPaidFor"]
            isComplete <- map["isComplete"]
        }
    }
}
