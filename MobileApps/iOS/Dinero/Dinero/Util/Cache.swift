//
//  Cache.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

class Cache {
    // MARK: - Inner Types
    private struct Key {
        static let accessToken = "DINERO_ACCESS_TOKEN"
        static let refreshToken = "DINERO_REFRESH_TOKEN"
        static let sessionId = "DINERO_SESSION_ID"
        static let tableId = "DINERO_TABLE_ID"
        static let restaurantId = "DINERO_RESTAURANT_ID"
    }
    
    // MARK: - Instance Variables.
    private var userDefaults = UserDefaults()
    
    // MARK: - Cache Members.
    var accessToken: String? {
        get { return userDefaults.string(forKey: Key.accessToken) }
        set { userDefaults.set(newValue, forKey: Key.accessToken)}
    }
    
    var refreshToken: String? {
        get { return userDefaults.string(forKey: Key.refreshToken) }
        set { userDefaults.set(newValue, forKey: Key.refreshToken)}
    }
    
    var sessionId: String? {
        get { return userDefaults.string(forKey: Key.sessionId) }
        set { userDefaults.set(newValue, forKey: Key.sessionId)}
    }
    
    var tableId: String? {
        get { return userDefaults.string(forKey: Key.tableId) }
        set { userDefaults.set(newValue, forKey: Key.tableId)}
    }
    
    var restaurantId: String? {
        get { return userDefaults.string(forKey: Key.restaurantId) }
        set { userDefaults.set(newValue, forKey: Key.restaurantId)}
    }
}
