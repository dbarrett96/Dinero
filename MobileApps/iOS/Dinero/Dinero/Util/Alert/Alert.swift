//
//  Alert.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

struct Alert {
    var title: String?
    var body: String?
    var buttons: [AlertButton]
}


