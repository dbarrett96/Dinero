//
//  AlertButton.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

struct AlertButton {
    var text: String
    var action: ((UIAlertAction) -> Void)?
    
    init(_ text: String, action: ((UIAlertAction) -> Void)?) {
        self.text = text
        self.action = action
    }
}

extension AlertButton {
    static var ok: AlertButton {
        return AlertButton(.ok, action: nil)
    }
}
