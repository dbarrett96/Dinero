//
//  Session.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-09.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

class Session {
    static let current = Session()
    
    enum State {
        case unauthenticated
        case authenticated
        case dining
    }
    
    var state = State.unauthenticated
    let cache = Cache()
}
