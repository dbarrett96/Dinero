//
//  Device.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-31.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

struct Device {
    static var statusBarHeight: CGFloat {
        return min(UIApplication.shared.statusBarFrame.size.height,
                   UIApplication.shared.statusBarFrame.size.width)
    }
    
    static var navbarHeight: CGFloat {
        return 48
    }
    
    static var topBarHeight: CGFloat {
        return statusBarHeight + navbarHeight
    }
    
    static var screen: CGRect {
        return UIScreen.main.bounds
    }
}
