//
//  CGSize+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-06.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

extension CGSize {
    static func square(_ size: CGFloat) -> CGSize {
        return CGSize(width: size, height: size)
    }
}
