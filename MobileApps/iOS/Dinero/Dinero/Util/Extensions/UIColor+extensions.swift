//
//  UIColor+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

extension UIColor {
    /// Create a UIColor from a rgba hex color string.
    ///
    /// - Parameter hexString: The color as a hex string.
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? String(hexString.dropFirst()) : hexString)
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
    
    convenience init?(hex: String) {
        self.init(hexString: "#" + hex)
    }
}

extension UIColor {
    static let gray0 = UIColor(hex: "f8f9f9")!
    static let gray1 = UIColor(hex: "ebedee")!
    static let gray2 = UIColor(hex: "dee1e3")!
    static let gray3 = UIColor(hex: "cfd3d6")!
    static let gray4 = UIColor(hex: "bec4c8")!
    static let gray5 = UIColor(hex: "acb4b9")!
    static let gray6 = UIColor(hex: "97a1a7")!
    static let gray7 = UIColor(hex: "7f8a93")!
    static let gray8 = UIColor(hex: "5f6e78")!
    static let gray9 = UIColor(hex: "374047")!
}
