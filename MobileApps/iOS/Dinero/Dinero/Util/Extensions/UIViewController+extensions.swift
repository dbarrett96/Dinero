//
//  UIViewController+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

extension UIViewController {
    func present(alert: Alert) {
        let alertController = UIAlertController(title: alert.title, message: alert.body, preferredStyle: .alert)
        
        alert.buttons.map {
            UIAlertAction(title: $0.text, style: .default, handler: $0.action)
        }.forEach {
            alertController.addAction( $0 )
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showLoading() {
        self.present(LoadingController(), animated: false, completion: nil)
    }
    
    func withNavigation() -> UIViewController {
        return LightStatusBarNavigationController(rootViewController: self)
    }
}
