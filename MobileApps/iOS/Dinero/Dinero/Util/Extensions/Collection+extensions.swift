//
//  Collection+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-29.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
