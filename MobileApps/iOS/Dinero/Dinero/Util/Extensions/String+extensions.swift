//
//  String+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

extension String {
    // MARK: - Static Properties
    static var ok: String {
        return "SimpleStrings.ok".localized
    }
    
    // MARK: - Properties
    var localized: String {
        return Bundle.main.localizedString(forKey: self, value: nil, table: nil)
    }
}
