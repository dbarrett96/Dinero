//
//  UIView+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-27.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

extension UIView{
    /// Add a gradient background to a UIView.
    ///
    /// - Parameter colors: The colors
    func addGradientBackground(colors: [UIColor], isFullscreen: Bool = false){
        clipsToBounds = true
        
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.frame = isFullscreen ? UIScreen.main.bounds : self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    /// Add a shadow to a UIView.
    func addShadow() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 2
        self.layer.masksToBounds = false
    }
}

extension UIView {
    func show() {
        self.alpha = 1
        self.isHidden = false
    }
    
    func hide() {
        self.alpha = 0
        self.isHidden = true
    }
}

extension UIView {
    enum Edge {
        case top, left, bottom, right
    }
    
    @discardableResult
    func addBorder(on edges: [Edge], withColor color: UIColor, andWidth width: CGFloat) -> [UIView] {
        guard !edges.isEmpty else { return [] }
        
        let makeEdge: () -> UIView = {
            let edgeView = UIView()
            edgeView.backgroundColor = color
            return edgeView
        }
        
        var addedEdges = [UIView]()
        
        if edges.contains(.top) {
            let edge = makeEdge()
            self.addSubview(edge)
            edge.snp.makeConstraints { (make) in
                make.top.left.right.equalTo( self )
                make.height.equalTo( width )
            }
            addedEdges.append(edge)
        }
        
        if edges.contains(.left) {
            let edge = makeEdge()
            self.addSubview(edge)
            edge.snp.makeConstraints { (make) in
                make.top.left.bottom.equalTo( self )
                make.width.equalTo( width )
            }
            addedEdges.append(edge)
        }
        
        if edges.contains(.bottom) {
            let edge = makeEdge()
            self.addSubview(edge)
            edge.snp.makeConstraints { (make) in
                make.bottom.left.right.equalTo( self )
                make.height.equalTo( width )
            }
            addedEdges.append(edge)
        }
        
        if edges.contains(.right) {
            let edge = makeEdge()
            self.addSubview(edge)
            edge.snp.makeConstraints { (make) in
                make.top.right.bottom.equalTo( self )
                make.width.equalTo( width )
            }
            addedEdges.append(edge)
        }
        
        return addedEdges
    }
}
