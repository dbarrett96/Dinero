//
//  UIImageView+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

extension UIImageView {
    /// Set the color of a UIImageView.
    ///
    /// - Parameter color: The color for the image view.
    func color(with color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}
