//
//  Error+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-03.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

extension Error {
    var alert: Alert {
        return Alert(title: "ErrorAlert.title".localized,
                     body: self.localizedDescription,
                     buttons: [.ok])
    }
}
