//
//  Bool+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

extension Bool {
    /// Create a boolean from an integer.
    ///
    /// - Parameter int: The integer
    init(_ int: Int) {
        self = int == 0 ? false : true
    }
}
