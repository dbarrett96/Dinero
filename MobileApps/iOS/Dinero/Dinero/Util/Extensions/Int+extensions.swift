//
//  Int+extensions.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import Foundation

extension Int {
    /// Create an integer value from a boolean value.
    ///
    /// - Parameter bool: The boolean value.
    init(_ bool: Bool) {
        self = bool ? 1 : 0
    }
}
