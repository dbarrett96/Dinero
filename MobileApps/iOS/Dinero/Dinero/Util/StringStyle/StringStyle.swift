//
//  StringStyle.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class StringStyle {
    // MARK: - Instance Variables
    var font: UIFont
    var color: UIColor
    var alignment: NSTextAlignment
    
    // MARK: - Constructor
    init(_ font: Font,
         _ fontSize: CGFloat,
         _ alignment: NSTextAlignment,
         _ color: UIColor)
    {
        self.font = UIFont(name: font.rawValue, size: fontSize)!
        self.alignment = alignment
        self.color = color
    }
    
    init(copy: StringStyle) {
        self.font = copy.font
        self.color = copy.color
        self.alignment = copy.alignment
    }
    
    // MARK: - Methods
    func with( color: UIColor ) -> StringStyle {
        let copy = StringStyle(copy: self)
        copy.color = color
        return copy
    }
}


