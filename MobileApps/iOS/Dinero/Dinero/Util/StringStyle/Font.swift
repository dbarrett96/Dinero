//
//  Font.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

enum Font: String {
    case black = "Roboto-Black"
    case blackItalic = "Roboto-BlackItalic"
    case bold = "Roboto-Bold"
    case boldItalic = "Roboto-BoldItalic"
    case italic = "Roboto-Italic"
    case light = "Roboto-Light"
    case lightItalic = "Roboto-LightItalic"
    case medium = "Roboto-Medium"
    case mediumItalic = "Roboto-MediumItalic"
    case regular = "Roboto-Regular"
    case thin = "Roboto-Thin"
    case thinItalic = "Roboto-ThinItalic"
    
    func with(size: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: size)!
    }
}
