//
//  UIViews+StringStyle.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

protocol StringStyleable {
    func load(stringStyle: StringStyle)
    func load(styledString: StyledString)
}

extension StringStyleable where Self: UIView {
    init(_ stringStyle: StringStyle) {
        self.init()
        self.load(stringStyle: stringStyle)
    }
    
    init(_ styledString: StyledString) {
        self.init()
        self.load(styledString: styledString)
    }
}

extension UIButton: StringStyleable {
    func load(stringStyle: StringStyle) {
        self.titleLabel?.font = stringStyle.font
        self.setTitleColor(stringStyle.color, for: .normal)
        self.titleLabel?.textAlignment = stringStyle.alignment
    }
    
    func load(styledString: StyledString) {
        self.load(stringStyle: styledString)
        self.setTitle(styledString.string, for: .normal)
    }
}

extension UILabel: StringStyleable {
    func load(stringStyle: StringStyle) {
        self.font = stringStyle.font
        self.textColor = stringStyle.color
        self.textAlignment = stringStyle.alignment
    }
    
    func load(styledString: StyledString) {
        self.load(stringStyle: styledString)
        self.text = styledString.string
    }
}

extension UITextField: StringStyleable {
    func load(stringStyle: StringStyle) {
        self.font = stringStyle.font
        self.textColor = stringStyle.color
        self.textAlignment = stringStyle.alignment
    }
    
    func load(styledString: StyledString) {
        self.load(stringStyle: styledString)
        self.text = styledString.string
    }
}
