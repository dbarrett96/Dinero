//
//  StyledString.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-28.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

import UIKit

class StyledString: StringStyle {
    // MARK: - Instance Variables
    var string: String
    
    // MARK: - Constructor
    init(_ string: String,
         _ font: Font,
         _ fontSize: CGFloat,
         _ alignment: NSTextAlignment,
         _ color: UIColor) {
        self.string = string
        super.init(font, fontSize, alignment, color)
    }
    
    init(copy: StyledString) {
        self.string = copy.string
        super.init(copy: copy)
    }
    
    // MARK: - Methods
    func with( text: String ) -> StyledString {
        let copy = StyledString(copy: self)
        copy.string = text
        return copy
    }
    
    override func with( color: UIColor ) -> StyledString {
        let copy = StyledString(copy: self)
        copy.color = color
        return copy
    }
}
