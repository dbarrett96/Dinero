//
//  PhoneNumber.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

class LocalPhoneNumber {
    // MARK: - Properties
    var description: String {
        var desc = ""
        var overallIndex = 0
        
        for groupingLength in pattern {
            for _ in 0..<groupingLength {
                desc += String(self.digits[overallIndex])
                overallIndex += 1
            }
            
            desc += "-"
        }
        
        return String(desc.dropLast())
    }
    
    // MARK: - Instance Variables
    let pattern: [Int]
    let digits: [Int]
    
    // MARK: - Constructor
    init?(_ digits: [Int], from pattern: [Int]) {
        guard digits.count == pattern.reduce(0, +) else {
            return nil
        }
        
        self.pattern = pattern
        self.digits = digits
    }
}
