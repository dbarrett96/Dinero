//
//  PhoneNumber.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-05-30.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

class PhoneNumber: LocalPhoneNumber, CustomStringConvertible {
    // MARK: - Inner Type
    enum CountryCode: CustomStringConvertible {
        case canada
        
        var value: [Int] {
            switch self {
            case .canada: return [1]
            }
        }
        
        var description: String {
            return self.value.map { String($0) }.reduce("", +)
        }
    }
    
    // MARK: - Properties
    override var description: String {
        return "+\(self.countryCode.description) \(super.description)"
    }
    
    var localDescription: String {
        return super.description
    }

    // MARK: - Instance Variables
    let countryCode: CountryCode
    
    // MARK: - Init
    init(countryCode: CountryCode,_ localPhoneNumber: LocalPhoneNumber) {
        self.countryCode = countryCode
        super.init(localPhoneNumber.digits, from: localPhoneNumber.pattern)!
    }
}
