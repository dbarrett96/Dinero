//
//  SeatRequestState.swift
//  Dinero
//
//  Created by Damon Barrett on 2018-06-07.
//  Copyright © 2018 Damon Barrett. All rights reserved.
//

enum SeatRequestState: String {
    case canceled = "CANCELED"
    case pending = "PENDING"
    case declined = "DECLINED"
    case rejected = "REJECTED"
    case expired = "EXPIRED"
    case accepted = "ACCEPTED"
}
